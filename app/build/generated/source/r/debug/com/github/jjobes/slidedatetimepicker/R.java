/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.github.jjobes.slidedatetimepicker;

public final class R {
	public static final class color {
		public static final int gray_holo_dark = 0x7f0d006e;
		public static final int gray_holo_light = 0x7f0d006f;
	}
	public static final class drawable {
		public static final int selection_divider = 0x7f0200b6;
	}
	public static final class id {
		public static final int buttonHorizontalDivider = 0x7f08015d;
		public static final int buttonVerticalDivider = 0x7f08015f;
		public static final int cancelButton = 0x7f08015e;
		public static final int customTab = 0x7f080101;
		public static final int datePicker = 0x7f080118;
		public static final int okButton = 0x7f080160;
		public static final int slidingTabLayout = 0x7f08015b;
		public static final int tabText = 0x7f080102;
		public static final int timePicker = 0x7f08012e;
		public static final int viewPager = 0x7f08015c;
	}
	public static final class layout {
		public static final int custom_tab = 0x7f040032;
		public static final int fragment_date = 0x7f040041;
		public static final int fragment_time = 0x7f040047;
		public static final int slide_date_time_picker = 0x7f040061;
	}
	public static final class string {
		public static final int picker_btn_confirm = 0x7f070113;
		public static final int picker_btn_now = 0x7f070114;
		public static final int picker_error = 0x7f070115;
	}
	public static final class style {
		public static final int AppBaseTheme = 0x7f0a000c;
		public static final int AppTheme = 0x7f0a0093;
	}
}
