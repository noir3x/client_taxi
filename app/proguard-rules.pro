# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/colors3/Documents/code/android/android-sdk-linux/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-keepattributes Exceptions
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes Deprecated
-keepattributes SourceFile
-keepattributes LineNumberTable
-keepattributes *Annotation*
-keepattributes EnclosingMethod

# For SearchView
-keep class android.support.v7.widget.SearchView { *; }

# For RoboSpice
-keep,includedescriptorclasses class com.octo.android.robospice.** { *; }
-keepclassmembers class com.gootax.client.requests.** { *; }
-dontwarn com.octo.android.robospice.SpiceService
-dontwarn android.support.**
-dontwarn com.sun.xml.internal.**
-dontwarn com.sun.istack.internal.**
-dontwarn org.codehaus.jackson.**
-dontwarn org.springframework.**
-dontwarn java.awt.**
-dontwarn javax.security.**
-dontwarn java.beans.**
-dontwarn javax.xml.**
-dontwarn java.util.**
-dontwarn org.w3c.dom.**
-dontwarn com.google.common.**
-dontwarn com.octo.android.robospice.persistence.**
-dontwarn com.octo.android.robospice.request.**

# For Retrofit
-keep class com.octo.android.robospice.retrofit.** { *; }
-keep class retrofit.** { *; }
-keep interface retrofit.** { *;}
-keepclasseswithmembers class * {
@retrofit.** *;
}
-keepclassmembers class * {
@retrofit.** *;
}
-dontwarn retrofit.appengine.UrlFetchClient
-dontwarn retrofit.converter.JacksonConverter
-dontwarn com.fasterxml.jackson.databind.ObjectMapper
-keep class com.google.gson.** { *; }
-keep class com.google.inject.** { *; }
-keep class org.apache.james.mime4j.* { *; }
-keep class javax.inject.** { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-dontwarn com.google.appengine.api.urlfetch.**
-dontwarn rx.**

# For OkHttp
-keep class com.squareup.okhttp.** { *; }
-keep interface com.squareup.okhttp.** { *; }
-dontwarn com.squareup.okhttp.**

# For GSON
#-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }

# For Active Android
-keep class com.activeandroid.** { *; }
-keep class com.activeandroid.** { *** mId; }
-keep class com.gootax.client.models.** { *; }

# For EventBus
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
-keep class com.gootax.client.events.** { *; }

# For SwipeLayout lib
-keep,includedescriptorclasses class com.daimajia.swipe.** { *; }
-keep class com.daimajia.swipe.** { int gravity; }

# For SlideDateTimePicker
-keep class com.github.jjobes.slidedatetimepicker.** { int year; }
-keep class com.github.jjobes.slidedatetimepicker.** { int month; }
-keep class com.github.jjobes.slidedatetimepicker.** { int day; }
-keep class com.github.jjobes.slidedatetimepicker.** { int hour; }
-keep class com.github.jjobes.slidedatetimepicker.** { int minute; }
-keep class com.github.jjobes.slidedatetimepicker.** { int amPm; }

-keep class com.github.jjobes.slidedatetimepicker.** { *** mSelectionDivider; }

# For Google GMS
-keep,includedescriptorclasses class com.google.android.gms.** { *; }
-keep class com.google.android.gms.** { java.lang.String MODULE_ID; }
-keep class com.google.android.gms.** { int MODULE_VERSION; }
-keep class com.google.android.gms.** { *** theUnsafe; }

# For jars
-dontnote android.net.http.**
-dontnote org.apache.http.**