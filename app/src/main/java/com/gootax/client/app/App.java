package com.gootax.client.app;


import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.gootax.client.utils.AnalyticsHelper;
import com.gootax.client.utils.LocaleHelper;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        Fresco.initialize(this);
        LocaleHelper.onCreate(this);
        AnalyticsHelper.init(this);
    }

}
