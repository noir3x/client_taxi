package com.gootax.client.app;

import com.gootax.client.R;

public class AppParams {

    // DEMO OR NOT
    public static final boolean IS_DEMO = true;

    // URL for requests
    //

    // URL for geo requests
    //

    // For request header
    public static final String TENANT_ID = "18"; //"68";
    //

    // Notification parameter
    public static final int NOTIFICATION_ID = 42;

    // Google cloud messaging info
    public static final String PUSH_SENDER_ID = "1042866605155"; //407168236755
    public static final String PUSH_ORDERINFO = "com.gootax.client.order";

    // Google key for static maps
    public static final String GOOGLE_API_KEY = "AIzaSyDlPtgZnp__IrjdBN1roVF6My5i4Z50Zdw";

    // Radius of the zone with cars
    public static final String RADIUS = "3000";

    // When there is no polygon
    public static final int ORDERS_RADIUS = 40;

    //
    public static final long TIME_UPDATE_TARIFFS = 21600000;

    // Order states
    public static final String STATUS_ID_COMPLETE_PAID = "37";
    public static final String STATUS_ID_COMPLETE_NOPAID = "38";

    /*
     * 0 - OSM
     * 1 - Google
     * 2 - Google Hybrid
     */
    public static final int DEFAULT_MAP = 1;

    // Can map be chosen
    public static final boolean WITH_OSM_MAP = true;
    public static final boolean WITH_GOOGLE_MAP_NORMAL = true;
    public static final boolean WITH_GOOGLE_MAP_HYBRID = true;

    // Use driver or car photo
    public static final boolean USE_PHOTO = true;
    // Use driver photo otherwise
    public static final boolean USE_CAR_PHOTO = false;

    // Can user call the driver or dispatcher
    public static final boolean USE_CALLS = true;

    // Geocoding params
    public static final boolean USE_SEARCH = true; // in B, C, D, E points

    // SMS RESENDING
    public static final int SMS_TIMEOUT = 60;

    // Can user define flat
    public static final boolean WITH_FLAT = true;

    // Can user pay by bank card
    public static final boolean PAYMENT_WITH_CARDS = true;

    // Can user pay by cash
    public static final boolean PAYMENT_WITH_CASH = true;

    // Can user create order with only 1 address
    public static final boolean ORDER_WITH_ONE_ADDRESS = true;

    public static final int DEFAULT_THEME = R.style.AppTheme1;
    public static final int[] THEMES = {R.style.AppTheme1, R.style.AppTheme2, R.style.AppTheme3,
            R.style.AppTheme4, R.style.AppTheme5, R.style.AppTheme6};

    public static final int[] MAPS = {1, 2};

    // Yandex AppMetrica
    public static final boolean WITH_METRICA = true;
    public static final String METRICA_KEY = "7fa2611c-cefc-4760-954d-49c05a7dbc41";

}
