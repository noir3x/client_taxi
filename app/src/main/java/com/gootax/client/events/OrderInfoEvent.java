package com.gootax.client.events;

public class OrderInfoEvent {

    private String status;
    private String statusLabel;
    private String carDesc = "";
    private String driverName = "";
    private String photo = "";
    private String cost;
    private double lat;
    private double lon;
    private Float degree;
    private long raiting;
    private String driverPhone;
    private String time;
    private String statusId;

    public OrderInfoEvent() {

    }

    public OrderInfoEvent(String status, String statusLabel, String carDesc, String driverName, String photo, String cost, double lat, double lon, Float degree, long raiting, String driverPhone, String time, String statusId) {
        this.status = status;
        this.statusLabel = statusLabel;
        this.carDesc = carDesc;
        this.driverName = driverName;
        this.photo = photo;
        this.cost = cost;
        this.lat = lat;
        this.lon = lon;
        this.degree = degree;
        this.raiting = raiting;
        this.driverPhone = driverPhone;
        this.time = time;
        this.statusId = statusId;
    }

    public String getTime() {
        return time;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public String getCarDesc() {
        return carDesc;
    }

    public String getDriverName() {
        return driverName;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getPhoto() {
        return photo;
    }

    public String getCost() {
        return cost;
    }

    public Float getDegree() {
        return degree;
    }

    public long getRaiting() {
        return raiting;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public void setCarDesc(String carDesc) {
        this.carDesc = carDesc;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setDegree(Float degree) {
        this.degree = degree;
    }

    public void setRaiting(long raiting) {
        this.raiting = raiting;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
}
