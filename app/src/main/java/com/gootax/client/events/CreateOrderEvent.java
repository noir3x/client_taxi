package com.gootax.client.events;


public class CreateOrderEvent {

    private String orderId;
    private String orderNumber;
    private String statusLabel;
    private String type;

    public CreateOrderEvent(String orderId, String orderNumber, String statusLabel, String type) {
        this.orderId = orderId;
        this.orderNumber = orderNumber;
        this.statusLabel = statusLabel;
        this.type = type;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public String getType() {
        return type;
    }
}
