package com.gootax.client.events;

public class CurrentLocationEvent {

    private double lat;
    private double lon;
    private boolean withReverse;

    public CurrentLocationEvent(double lat, double lon, boolean withReverse) {
        this.lat = lat;
        this.lon = lon;
        this.withReverse = withReverse;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public boolean withReverse() {
        return withReverse;
    }

}
