package com.gootax.client.events;


import com.gootax.client.models.Option;
import com.gootax.client.models.Tariff;

import java.util.List;

public class SaveTariffsEvent {

    private List<Tariff> tariffList;
    private List<Option> optionList;

    public SaveTariffsEvent(List<Tariff> tariffList, List<Option> optionList) {
        this.tariffList = tariffList;
        this.optionList = optionList;
    }

    public List<Tariff> getTariffList() {
        return tariffList;
    }

    public List<Option> getOptionList() {
        return optionList;
    }
}
