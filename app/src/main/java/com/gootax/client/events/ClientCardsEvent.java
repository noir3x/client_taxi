package com.gootax.client.events;


import java.util.ArrayList;

public class ClientCardsEvent {

    private ArrayList<String> clientCards;

    public ClientCardsEvent(ArrayList<String> clientCards) {
        this.clientCards = clientCards;
    }

    public ArrayList<String> getClientCards() {
        return clientCards;
    }

}
