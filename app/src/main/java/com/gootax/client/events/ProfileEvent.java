package com.gootax.client.events;

import com.gootax.client.models.Company;
import com.gootax.client.models.Profile;

import java.util.List;

public class ProfileEvent {

    private Profile profile;
    private List<Company> companies;
    private int success;

    public ProfileEvent(Profile profile, List<Company> companies, int success) {
        this.profile = profile;
        this.companies = companies;
        this.success = success;
    }

    public Profile getProfile() {
        return profile;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public int getSuccess() {
        return success;
    }

}
