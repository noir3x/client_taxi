package com.gootax.client.events;


public class CompanyPageEvent {

    private String webPageString;

    public CompanyPageEvent(String webPageString) {
        this.webPageString = webPageString;
    }

    public String getWebPageString() {
        return webPageString;
    }

}
