package com.gootax.client.events;


public class DeleteItemListEvent {

    int position;

    public DeleteItemListEvent(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

}
