package com.gootax.client.events;

import com.gootax.client.models.Car;

import java.util.List;

public class CarFreeEvent {

    private List<Car> carList;

    public CarFreeEvent(List<Car> carList) {
        this.carList = carList;
    }

    public List<Car> getCarList() {
        return carList;
    }
}
