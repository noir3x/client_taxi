package com.gootax.client.events;


import com.gootax.client.models.City;

public class PolygonDetectEvent {

    public City city;

    public PolygonDetectEvent(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }

}
