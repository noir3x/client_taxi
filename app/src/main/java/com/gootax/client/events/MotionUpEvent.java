package com.gootax.client.events;

public class MotionUpEvent {

    private boolean up;

    public MotionUpEvent(boolean up) {
        this.up = up;
    }

    public boolean isUp() {
        return up;
    }
}
