package com.gootax.client.events.geo_events;

import com.gootax.client.models.Address;

public class ReverseEvent {

    private Address address;
    private boolean isGpsAddress;

    public ReverseEvent(Address address, boolean isGpsAddress) {
        this.address = address;
        this.isGpsAddress = isGpsAddress;
    }

    public Address getAddress() {
        return address;
    }

    public boolean isGpsAddress() {
        return isGpsAddress;
    }

}
