package com.gootax.client.events.geo_events;


public class ChangeTariff {

    private String tariffId;

    public ChangeTariff(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffId() {
        return tariffId;
    }
}
