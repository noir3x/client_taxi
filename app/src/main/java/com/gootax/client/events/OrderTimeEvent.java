package com.gootax.client.events;


public class OrderTimeEvent {

    private final String orderTime;

    public OrderTimeEvent(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderTime() {
        return orderTime;
    }

}
