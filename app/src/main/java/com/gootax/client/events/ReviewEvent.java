package com.gootax.client.events;


public class ReviewEvent {

    private String status;

    public ReviewEvent(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
