package com.gootax.client.events;

public class SignUpEvent {

    private int success;

    public SignUpEvent(int success) {
        this.success = success;
    }

    public int getSuccess() {
        return success;
    }

}