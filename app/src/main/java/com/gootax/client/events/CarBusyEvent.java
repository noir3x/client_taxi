package com.gootax.client.events;

import com.gootax.client.models.Car;

public class CarBusyEvent {

    private Car car;

    public CarBusyEvent(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }
}
