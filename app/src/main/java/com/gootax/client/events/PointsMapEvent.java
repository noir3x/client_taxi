package com.gootax.client.events;

import com.gootax.client.models.Address;

import java.util.List;

public class PointsMapEvent {

    List<Address> addressList;
    boolean activeOrder;
    String statusOrder;
    String time;

    public PointsMapEvent(List<Address> addressList, boolean activeOrder, String statusOrder, String time) {
        this.addressList = addressList;
        this.activeOrder = activeOrder;
        this.statusOrder = statusOrder;
        this.time = time;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public boolean isActiveOrder() {
        return activeOrder;
    }

    public String getStatusOrder() {
        return statusOrder;
    }

    public String getTime() {
        return time;
    }
}
