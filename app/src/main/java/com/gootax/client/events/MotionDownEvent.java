package com.gootax.client.events;

public class MotionDownEvent {

    private boolean down;

    public MotionDownEvent(boolean down) {
        this.down = down;
    }

    public boolean isDown() {
        return down;
    }
}
