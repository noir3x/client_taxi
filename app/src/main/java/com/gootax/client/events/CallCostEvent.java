package com.gootax.client.events;

public class CallCostEvent {

    private String cost;
    private String dis;
    private String time;

    public CallCostEvent(String cost, String dis, String time) {
        this.cost = cost;
        this.dis = dis;
        this.time = time;
    }

    public String getCost() {
        return cost;
    }

    public String getDis() {
        return dis;
    }

    public String getTime() {
        return time;
    }
}
