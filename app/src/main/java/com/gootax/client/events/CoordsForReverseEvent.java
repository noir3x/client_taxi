package com.gootax.client.events;

public class CoordsForReverseEvent {

    private String lat;
    private String lon;

    public CoordsForReverseEvent(String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

}
