package com.gootax.client.events.geo_events;

import com.gootax.client.models.Address;

import java.util.ArrayList;

public class GeoEvent {

    private ArrayList<Address> addresses;

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

}
