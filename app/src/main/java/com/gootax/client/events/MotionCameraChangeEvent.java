package com.gootax.client.events;

public class MotionCameraChangeEvent {

    private boolean change;

    public MotionCameraChangeEvent(boolean change) {
        this.change = change;
    }

    public boolean isChange() {
        return change;
    }
}
