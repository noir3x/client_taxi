package com.gootax.client.maps.google;

import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.gootax.client.R;
import com.gootax.client.activities.MainActivity;
import com.gootax.client.activities.MapActivity;
import com.gootax.client.events.CarBusyEvent;
import com.gootax.client.events.CarFreeEvent;
import com.gootax.client.events.CoordsForReverseEvent;
import com.gootax.client.events.CurrentLocationEvent;
import com.gootax.client.events.GetCurrentLocationEvent;
import com.gootax.client.events.MotionBlockMapEvent;
import com.gootax.client.events.MotionCameraChangeEvent;
import com.gootax.client.events.MotionUnblockMapEvent;
import com.gootax.client.events.OrderRepeatEvent;
import com.gootax.client.events.PointsMapEvent;
import com.gootax.client.events.PolygonDetectEvent;
import com.gootax.client.maps.FragmentAbstractMap;
import com.gootax.client.models.Address;
import com.gootax.client.models.Car;
import com.gootax.client.models.City;
import com.gootax.client.models.Profile;
import com.gootax.client.models.RoutePoint;
import com.gootax.client.network.listeners.geo_listeners.ReverseListener;
import com.gootax.client.network.requests.GetReverseRequest;
import com.gootax.client.utils.DetectCity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * Google map fragment
 */
public class FragmentGMap extends FragmentAbstractMap implements
        OnMapReadyCallback, TouchableWrapper.UpdateMapAfterUserInteraction {

    public static FragmentGMap newInstance() {
        return new FragmentGMap();
    }

    private GoogleMap googleMap;
    private View mOriginalContentView;
    private DetectCity detectCity;
    private Profile profile;
    private Marker carBusy;
    private List<Marker> points;
    private double lat, lon;
    private int[] icons = {R.drawable.pin_a,
            R.drawable.pin_b, R.drawable.pin_c,
            R.drawable.pin_d,R.drawable.pin_e};
    private ValueAnimator valueAnimator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<City> cityList = City.getCities();
        detectCity = new DetectCity(cityList);
        profile = Profile.getProfile();
        lat = 0;
        lon = 0;
        points = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getMapAsync(this);
        mOriginalContentView = super.onCreateView(inflater, container, savedInstanceState);
        TouchableWrapper mTouchView = new TouchableWrapper(
                getActivity().getApplicationContext(), this);
        mTouchView.addView(mOriginalContentView);
        EventBus.getDefault().register(this);
        return mTouchView;
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.googleMap = map;

        if (profile.getMap() == 1) {
            this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        } else {
            this.googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }

        if (lat == 0) {
            City city;
            if (profile.isAuthorized()) {
                city = City.getCity(profile.getCityId());
            } else {
                city = City.getFirstCity();
            }
            city = City.getFirstCity();
            lat = city.getLat();
            lon = city.getLon();
        }

        setCameraAndTitle();

        googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                EventBus.getDefault().post(new MotionCameraChangeEvent(true));
                if (getAddrNum() == 1) {
                    City city = detectCity.getCity(
                            cameraPosition.target.latitude,
                            cameraPosition.target.longitude);
                    if (city != null) {
                        profile.setCityId(city.getCityId());
                        profile.save();
                    }
                    EventBus.getDefault().post(new PolygonDetectEvent(city));
                }
            }
        });

        try {
            if (getTag().equals("main") && ((MainActivity) getActivity()).getAddressList().size() > 0
                    && ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0) {
                City city = detectCity.getCity(
                        Double.valueOf(((MainActivity) getActivity()).getAddressList().get(0).getLat()),
                        Double.valueOf(((MainActivity) getActivity()).getAddressList().get(0).getLon()));
                if (city != null) {
                    profile.setCityId(city.getCityId());
                    profile.save();
                }
                EventBus.getDefault().post(new PolygonDetectEvent(city));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                EventBus.getDefault().post(new MotionCameraChangeEvent(true));
            }
        });

        if (getTag().equals("main")) {
            if (!((MainActivity) getActivity()).getOrder().getStatus().equals("empty")) {
                googleMap.clear();
                List<Address> addressList = RoutePoint.getRoute(((MainActivity) getActivity()).getOrder());
                points.clear();
                int count = 0;
                for (Address address : addressList) {
                    try {
                        points.add(googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .icon(BitmapDescriptorFactory.fromResource(icons[count]))));
                        count++;
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                ((MainActivity) getActivity()).updatePoints(false);
            }
        }
    }

    private void setCameraAndTitle() {
        OrderRepeatEvent orderRepeat = EventBus.getDefault()
                .removeStickyEvent(OrderRepeatEvent.class);
        if (orderRepeat != null && getTag().equals("main")) {
            setCameraAndTitleToFirstPoint();
        } else {
           setCameraAndTitleToCurrentLocation();
        }
    }

    private void setCameraAndTitleToFirstPoint() {
        Address firstAddress = null;
        MainActivity activity = (MainActivity) getActivity();
        if (activity != null) firstAddress = activity.getAddressList().get(0);
        if (firstAddress != null) {
            EventBus.getDefault().postSticky(
                    new CoordsForReverseEvent(String.valueOf(firstAddress.getLat()),
                            String.valueOf(firstAddress.getLon())));
            getSpiceManager().execute(
                    new GetReverseRequest(
                            profile.getCityId(),
                            String.valueOf(firstAddress.getLat()),
                            String.valueOf(firstAddress.getLon()),
                            "0.1", "1",
                            profile.getTenantId()),
                    new ReverseListener());
            googleMap.animateCamera(CameraUpdateFactory
                    .newLatLngZoom(new LatLng(
                            Double.valueOf(firstAddress.getLat()),
                            Double.valueOf(firstAddress.getLon())), 15));
        }
    }

    private void setCameraAndTitleToCurrentLocation() {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 15));
        EventBus.getDefault().postSticky(
                new CoordsForReverseEvent(String.valueOf(lat), String.valueOf(lon)));
        getSpiceManager().execute(
                new GetReverseRequest(
                        profile.getCityId(),
                        String.valueOf(lat),
                        String.valueOf(lon),
                        "0.1", "1",
                        profile.getTenantId()),
                new ReverseListener());
        EventBus.getDefault().post(new GetCurrentLocationEvent());
    }

    private int getAddrNum() {
        int addrNum = 1;
        if (getTag().equals("main")) {
            MainActivity activity = (MainActivity) getActivity();
            if (activity != null) addrNum = activity.getAddressList().size();
        } else {
            MapActivity activity = (MapActivity) getActivity();
            if (activity != null) addrNum = activity.getPointNum() + 1;
        }
        return addrNum;
    }

    @Override
    public void onUpdateMapAfterUserInteraction() {
        try {
            CameraPosition cameraPosition = googleMap.getCameraPosition();
            EventBus.getDefault().postSticky(new CoordsForReverseEvent(
                    String.valueOf(cameraPosition.target.latitude),
                    String.valueOf(cameraPosition.target.longitude)));
            getSpiceManager().execute(
                    new GetReverseRequest(
                            profile.getCityId(),
                            String.valueOf(cameraPosition.target.latitude),
                            String.valueOf(cameraPosition.target.longitude),
                            "0.1", "1",
                            profile.getTenantId()),
                    new ReverseListener());
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onMessage(MotionBlockMapEvent motionBlockMapEvent) {
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    @Subscribe
    public void onMessage(MotionUnblockMapEvent motionUnblockMapEvent) {
        googleMap.getUiSettings().setScrollGesturesEnabled(true);
    }

    @Subscribe
    public void onMessage(CarFreeEvent carFreeEvent) {
        if (getTag().equals("main") && !((MainActivity) getActivity())
                .getOrder().getStatus().equals("car_assigned")) {
            googleMap.clear();
            if (carFreeEvent.getCarList().size() > 0) {
                for (Car car : carFreeEvent.getCarList()) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(car.getLat(), car.getLon()))
                            .anchor(0.5f, 0.5f)
                            .rotation(car.getBearing())
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                }
            }
            int count = 0;
            for (Marker marker : points) {
                googleMap.addMarker(new MarkerOptions()
                        .position(marker.getPosition())
                        .icon(BitmapDescriptorFactory.fromResource(icons[count])));
                count++;
            }
        }
    }

    public Bitmap drawTextToBitmap(Context gContext,
                                   int gResId,
                                   String gText) {
        Resources resources = gContext.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap =
                BitmapFactory.decodeResource(resources, gResId);

        android.graphics.Bitmap.Config bitmapConfig =
                bitmap.getConfig();
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(getResources().getColor(R.color.textColorWhite));
        paint.setTextSize((int) (18 * scale));
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        float x = (bitmap.getWidth() - bounds.width())/(float)2.2;
        float y = (bitmap.getHeight() + bounds.height())/(float)2.9;

        canvas.drawText(gText, x, y, paint);

        Paint paintTime = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintTime.setColor(getResources().getColor(R.color.textColorWhite));
        paintTime.setTextSize((int) (8 * scale));
        Rect bounds2 = new Rect();
        paintTime.getTextBounds(getString(R.string.fragments_FragmentAbstractMap_time_minute_long), 0, getString(R.string.fragments_FragmentAbstractMap_time_minute_long).length(), bounds2);
        float x2 = (bitmap.getWidth() - bounds2.width())/(float)2.2;
        float y2 = (bitmap.getHeight() + bounds2.height())/(float)2.0;

        canvas.drawText(getString(R.string.fragments_FragmentAbstractMap_time_minute_long), x2, y2, paintTime);

        return bitmap;
    }

    @Subscribe
    public void onMessage(CarBusyEvent carBusyEvent) {
        if (carBusy == null)
            carBusy = googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()))
                    .anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        else
            carBusy.setPosition(new LatLng(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()));
        carBusy.setRotation(Float.valueOf(carBusyEvent.getCar().getBearing()));

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(carBusyEvent.getCar().getLat(), carBusyEvent.getCar().getLon()), 15));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(CurrentLocationEvent currentLocationEvent) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(currentLocationEvent.getLat(), currentLocationEvent.getLon()), 15));
        if (currentLocationEvent.withReverse()) {
            EventBus.getDefault().postSticky(new CoordsForReverseEvent(
                    String.valueOf(currentLocationEvent.getLat()),
                    String.valueOf(currentLocationEvent.getLon())));
            getSpiceManager().execute(
                    new GetReverseRequest(
                            profile.getCityId(),
                            String.valueOf(currentLocationEvent.getLat()),
                            String.valueOf(currentLocationEvent.getLon()),
                            "0.1", "1",
                            profile.getTenantId()),
                    new ReverseListener());
        }
    }

    @Subscribe
    public void onMessage(PointsMapEvent pointsMapEvent) {
        googleMap.clear();
        if (pointsMapEvent.isActiveOrder() || (pointsMapEvent.getAddressList().size() > 1 && !pointsMapEvent.isActiveOrder())) {
            points.clear();
            int count = 0;
            for (Address address : pointsMapEvent.getAddressList()) {
                try {
                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("car_assigned")) {
                        googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromBitmap(drawTextToBitmap(getActivity(),
                                        R.drawable.pin_empty, pointsMapEvent.getTime()))));
                    } else {
                        points.add(googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .icon(BitmapDescriptorFactory.fromResource(icons[count]))));
                    }

                    if (count == 0 && valueAnimator != null && valueAnimator.isStarted())
                        valueAnimator.cancel();
                    if (count == 0 && pointsMapEvent.getStatusOrder().equals("new")) {
                        TypedValue typedValue = new TypedValue();
                        getActivity().getTheme().resolveAttribute(R.attr.accent_bg_tariff, typedValue, true);

                        final Circle circle = googleMap.addCircle(new CircleOptions().center(new LatLng(Double.valueOf(address.getLat()), Double.valueOf(address.getLon())))
                                .radius(300)
                                .strokeColor(typedValue.data));
                        //.fillColor(android.graphics.ColorApp.parseColor("#AA0000FF")));
                        valueAnimator = new ValueAnimator();
                        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
                        valueAnimator.setRepeatMode(ValueAnimator.RESTART);
                        valueAnimator.setIntValues(0, 300);
                        valueAnimator.setDuration(2000);
                        valueAnimator.setEvaluator(new IntEvaluator());
                        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
                        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                            @Override
                            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                                float animatedFraction = valueAnimator.getAnimatedFraction();
                                // Log.e("", "" + animatedFraction);
                                circle.setRadius(animatedFraction * 300);
                            }
                        });
                        valueAnimator.start();
                    }

                    count++;
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        } else {
            points.clear();
        }
        if (pointsMapEvent.getAddressList().size() == 1 && !pointsMapEvent.isActiveOrder()) {
            Address firstAddress = pointsMapEvent.getAddressList().get(0);
            googleMap.animateCamera(CameraUpdateFactory
                    .newLatLngZoom(new LatLng(
                            Double.valueOf(firstAddress.getLat()),
                            Double.valueOf(firstAddress.getLon())), 15));
        }
        if (carBusy != null) {
            googleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(carBusy.getPosition().latitude, carBusy.getPosition().longitude))
                    .anchor(0.5f, 0.5f)
                    .rotation(carBusy.getRotation())
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        }
    }

    public String[] getMarkerCoords() {
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        String[] coords = new String[2];
        coords[0] = String.valueOf(cameraPosition.target.latitude);
        coords[1] = String.valueOf(cameraPosition.target.longitude);
        return coords;
    }

}
