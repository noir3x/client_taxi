package com.gootax.client.maps;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.gootax.client.maps.google.FragmentGMap;
import com.gootax.client.utils.AppPreferences;

public class MapFactory {

    public static final String MAP_TYPE_GOOGLE = "google";
    public static final String MAP_TYPE_YANDEX = "yandex";
    public static final String MAP_TYPE_OSM = "osm";

    public static FragmentAbstractMap getFragmentMap(Context context) {
        AppPreferences appPrefs = new AppPreferences(context);
        String mapType = appPrefs.getText("mapType");
        FragmentAbstractMap fragmentMap = FragmentGMap.newInstance();

        /*
        switch (mapType) {
            case MAP_TYPE_YANDEX:
            default:
                appPrefs.saveText("mapType", MAP_TYPE_GOOGLE);
                fragmentMap = FragmentGMap.newInstance();
                break;
            case MAP_TYPE_GOOGLE:

                appPrefs.saveText("mapType", MAP_TYPE_YANDEX);
                fragmentMap = FragmentYaMap.newInstance();
                break;
            case MAP_TYPE_OSM:
                appPrefs.saveText("mapType", MAP_TYPE_OSM);
                fragmentMap = FragmentOSMMap.newInstance();
                break;
        }
        */
        return fragmentMap;
    }

}
