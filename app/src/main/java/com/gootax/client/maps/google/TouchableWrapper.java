package com.gootax.client.maps.google;


import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.widget.FrameLayout;

import com.gootax.client.events.MotionDownEvent;
import com.gootax.client.events.MotionUnblockMapEvent;
import com.gootax.client.events.MotionUpEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Class for getting touch events on the map
 */
public class TouchableWrapper extends FrameLayout {

    private long lastTouched = 0;
    private static final long SCROLL_TIME = 200L; // 200 Milliseconds, but you can adjust that to your liking
    private UpdateMapAfterUserInteraction updateMapAfterUserInteraction;

    public TouchableWrapper(Context context) {
        super(context);
    }

    public TouchableWrapper(Context context, UpdateMapAfterUserInteraction userInteraction) {
        super(context);
        // Force the host activity to implement the UpdateMapAfterUserInteraction Interface
        try {
            this.updateMapAfterUserInteraction = userInteraction;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UpdateMapAfterUserInteraction");
        }
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                lastTouched = SystemClock.uptimeMillis();
                EventBus.getDefault().post(new MotionDownEvent(true));
                break;
            case MotionEvent.ACTION_UP:
                final long now = SystemClock.uptimeMillis();
                if (now - lastTouched > SCROLL_TIME) {
                    // Update the map
                    updateMapAfterUserInteraction.onUpdateMapAfterUserInteraction();
                }
                EventBus.getDefault().post(new MotionUpEvent(true));
                EventBus.getDefault().post(new MotionUnblockMapEvent());
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    // Map Activity must implement this interface
    public interface UpdateMapAfterUserInteraction {
        void onUpdateMapAfterUserInteraction();
    }
}
