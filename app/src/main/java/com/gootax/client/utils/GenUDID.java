package com.gootax.client.utils;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import java.util.UUID;


public class GenUDID {

    /** Return  unique identifier device*/
    public static String getUDID(Context context) {
        String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        UUID deviceUUID = new UUID(androidId.hashCode(), ((long) Build.VERSION.RELEASE.hashCode()) | Build.MODEL.hashCode());
        return deviceUUID.toString();
    }

}
