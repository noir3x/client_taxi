package com.gootax.client.utils;


import android.util.Log;

import com.gootax.client.app.AppParams;
import com.gootax.client.models.City;
import com.gootax.client.utils.polygon.Point;
import com.gootax.client.utils.polygon.Polygon;

import org.json.JSONArray;

import java.util.List;

public class DetectCity {

    private List<City> cities;

    public DetectCity(List<City> cities) {
        this.cities = cities;


        Log.d("COUNT_CIT1Y", cities.size() + "");
        for (City city : cities) {
            try {
                Log.d("NAME_CIT1Y", city.getCityName());
                Log.d("POLYGON_CIT1Y", city.getPolygon());
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    public City getCity(double lat, double lon) {
        Point curPoint = new Point((float) lat, (float) lon);
        for (City city : cities) {
            try {
                if (city.getPolygon().length() > 0) {
                    JSONArray jsonArrayCoords = new JSONArray(city.getPolygon());
                    Polygon.Builder polygon = Polygon.Builder();
                    for (int i = 0; i < jsonArrayCoords.length(); i++) {
                        JSONArray jsonArrayPoint = jsonArrayCoords.getJSONArray(i);
                        polygon.addVertex(new Point((float) jsonArrayPoint.getDouble(1), (float) jsonArrayPoint.getDouble(0)));
                    }

                    if (polygon.build().contains(curPoint)) {
                        return city;
                    }
                } else {
                    if (DistanceGPS.getDistance(Double.valueOf(city.getLat()), Double.valueOf(city.getLon()), lat, lon) < AppParams.ORDERS_RADIUS) {
                        return city;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

}
