package com.gootax.client.utils;


import com.gootax.client.app.AppParams;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedHashMap;


public class HashMD5 {

    public static String getHash(String str) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(str.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);
            while (hashtext.length() < 32)
                hashtext = "0" + hashtext;
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getSignature(LinkedHashMap<String,String> params) {
        String rawSignature = params.toString();
        String signature = rawSignature.substring(1, rawSignature.length() - 1)
                .replaceAll(", ", "&")
                .replaceAll("%5C%2F", "%2F%2F")
                .replaceAll(":", "%3A")
                .replaceAll("\\+", "%20")
                + AppParams.API_KEY;
        return HashMD5.getHash(signature);
    }

}