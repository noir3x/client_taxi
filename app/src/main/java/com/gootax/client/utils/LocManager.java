package com.gootax.client.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

/**
 * Additional class for encapsulating location manager calls
 *
 * Requires permission
 */
public class LocManager implements LocationListener {

    public static final int REQUEST_CODE_PERMISSION_LOC = 123;

    private LocationManager locMan;
    private Location location;
    private boolean hasGPS;

    public LocManager(Context context) {
        locMan = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        hasGPS = hasGpsSensor(context);
    }

    public void startUpdatesRequesting() throws SecurityException {
        if (hasGPS) locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000, 0, this);
        locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 30000, 500, this);
    }

    public void stopUpdatesRequesting() throws SecurityException {
        locMan.removeUpdates(this);
    }

    public Location getLastLocation() throws SecurityException {
        if (hasGPS && locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
            location = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
            location = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        return location;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //empty
    }

    @Override
    public void onProviderEnabled(String provider) {
        //empty
    }

    @Override
    public void onProviderDisabled(String provider) {
        //empty
    }

    public Location getLocation() {
        return location;
    }

    private boolean hasGpsSensor(Context context) {
        PackageManager packMan = context.getPackageManager();
        return packMan.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
    }

}
