package com.gootax.client.utils;

import android.text.Editable;

import java.util.Map;
import java.util.TreeMap;

public class InputMask {

    private String mask, firstSymbol;
    private String[] maskArr;
    private String phone;

    public InputMask(String mask, String phone) {
        this.mask = mask;
        this.phone = phone;
        firstSymbol = mask.replaceAll("\\D+", "");
        maskArr = mask.split("");
    }

    public String getNewText() {
        String newText;
        if (phone.length() == 0) {
            newText = mask;
        } else {
            newText = reload(phone.replaceAll("\\D+", "").substring(firstSymbol.length()));
        }
        return newText;
    }

    public int getSelection(String currentText) {
        int firstIndexForInput = currentText.indexOf('_');
        //if '_' is missing
        int selection;
        if (firstIndexForInput == -1) {
            try {
                selection = reload(phone.replaceAll("\\D+", "").substring(firstSymbol.length())).length();
            } catch (StringIndexOutOfBoundsException e) {
                selection = currentText.length();
            }
        } else {
            selection = firstIndexForInput;
        }
        return selection;
    }

    public String afterTextChangedSetText(String phoneStr, Editable s) {
        String result;
        if (!(s.toString().replaceAll("\\D+", "").length() < firstSymbol.length())) {
            result = reload(s.toString().replaceAll("\\D+", "").substring(firstSymbol.length()));
        } else {
            result = reload(phoneStr.replaceAll("\\D+", "").substring(firstSymbol.length()));
        }
        return result;
    }

    public int afterTextChangedSetSelection(String result) {
        int countSelection = 0;
        int selection = 0;
        for(int j=0; j < result.length(); j++) {
            if (result.substring(j, j+1).matches("[0-9]*")) {
                selection = countSelection + 1;
            }
            countSelection++;
        }
        return selection;
    }

    private String reload(String temp) {
        Map<Integer, String> maskMap = new TreeMap<>();
        Map <Integer, String> maskMapKey = new TreeMap<>();
        int count_symbol = 0;
        for (String symbol : maskArr) {
            if (symbol.equals(""))
                continue;
            maskMap.put(count_symbol, symbol);
            if (symbol.equals("_")) {
                maskMapKey.put(count_symbol, symbol);
            }
            count_symbol++;
        }
        StringBuilder stringBuilder = new StringBuilder();
        String [] tempArr = temp.split("");
        int count = 1;
        if (temp.length() > 0) {
            for (Map.Entry<Integer, String> entry : maskMapKey.entrySet()) {
                maskMap.put(entry.getKey(), tempArr[count]);
                count++;
                if (count == tempArr.length)
                    break;
            }
        }
        for (Map.Entry<Integer, String> entry : maskMap.entrySet()) {
            stringBuilder.append(entry.getValue());
        }
        return stringBuilder.toString();
    }

    public static String getProfilePhone(String phoneMask, String phone) {
        char[] phoneMaskArr = phoneMask.toCharArray();
        char[] phoneArr = phone.toCharArray();
        int phoneArrIndex = phoneMask.trim().replaceAll("\\D+", "").length();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < phoneMaskArr.length; i++) {
            if (phoneMaskArr[i] == '_') {
                result.append(phoneArr[phoneArrIndex]);
                ++phoneArrIndex;
            } else {
                result.append(phoneMaskArr[i]);
            }
        }
        return result.toString();
    }

}