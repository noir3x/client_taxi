package com.gootax.client.utils;

import android.app.Application;

import com.gootax.client.app.AppParams;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

/**
 * Class for using analytics in app
 */
public class AnalyticsHelper {

    public static final int METRICA_SESSION_TIMEOUT = 60; //sec

    public static final String METRICA_EVENT_REG = "User registration";
    public static final String METRICA_EVENT_CREATE_ORDER = "Creating order";
    public static final String METRICA_EVENT_REJECT_ORDER = "Rejecting order";

    public static void init(Application application) {
        if (AppParams.WITH_METRICA) {
            YandexMetricaConfig.Builder configBuilder =
                    YandexMetricaConfig.newConfigBuilder(AppParams.METRICA_KEY);
            configBuilder.setSessionTimeout(METRICA_SESSION_TIMEOUT);
            YandexMetricaConfig extendedConfig = configBuilder.build();
            YandexMetrica.activate(application, extendedConfig);
            YandexMetrica.enableActivityAutoTracking(application);
        }
    }

    public static void sendEvent(String event) {
        if (AppParams.WITH_METRICA) YandexMetrica.reportEvent(event);
    }


}
