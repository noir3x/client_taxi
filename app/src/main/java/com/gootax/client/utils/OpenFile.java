package com.gootax.client.utils;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class OpenFile {

    private Context context;
    private String fileName;

    public OpenFile (Context context, String fileName) {
        this.context = context;
        this.fileName = fileName;
    }

    public String getJson() {
        return readFile();
    }

    private String readFile() {
        StringBuilder sbr = new StringBuilder();
        try {
            AssetManager assetManager = context.getAssets();
            BufferedReader br = new BufferedReader(
                    new InputStreamReader(assetManager.open(fileName)));
            String str = "";
            while ((str = br.readLine()) != null) {
                sbr.append(str);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sbr.toString();
    }

}