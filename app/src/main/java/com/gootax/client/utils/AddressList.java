package com.gootax.client.utils;

import com.gootax.client.models.Address;

import java.util.ArrayList;

/**
 * Custom implementation of array list for addresses
 */
public class AddressList extends ArrayList<Address> {

    public static final int SECOND_POINT = 1;

    @Override
    public int size() {
        Address address = null;
        try {
            address = get(SECOND_POINT);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        if (address != null && address.isEmpty()) {
            // only first point in the list
            return 1;
        } else {
            return super.size();
        }
    }

    public int realSize() {
        return super.size();
    }

}
