package com.gootax.client.network.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.HashMD5;

import java.util.LinkedHashMap;

import retrofit.client.Response;


public class PostCreateClientCard extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String tenantid;
    private String clientId;
    private String currentTime;
    private String phone;

    public PostCreateClientCard(String clientId,
                                String currentTime, String phone, String tenantid) {
        super(Response.class, IGootaxApi.class);
        this.tenantid = tenantid;
        this.clientId = clientId;
        this.currentTime = currentTime;
        this.phone = phone;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("client_id", clientId);
        requestParams.put("current_time", currentTime);
        requestParams.put("phone", phone);
        this.signature = HashMD5.getSignature(requestParams);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().postCreateClientCard(signature, tenantid, clientId, currentTime, phone);
    }

}
