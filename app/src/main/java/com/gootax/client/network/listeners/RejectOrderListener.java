package com.gootax.client.network.listeners;


import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.OrderInfoEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class RejectOrderListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            JSONObject jsonOrder = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            EventBus.getDefault().post(new OrderInfoEvent("rejected", "rejected", "", "", "", "", 0, 0, (float) 0, 0, "", "0", "0"));

        } catch (JSONException e) {

        }
    }
}
