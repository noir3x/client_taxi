package com.gootax.client.network.listeners;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.CheckCardEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CheckCardListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        int resultCode = 1;
        try {
            resultCode = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getInt("code");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new CheckCardEvent(resultCode));
    }

}
