package com.gootax.client.network.listeners;

import com.gootax.client.R;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.SaveCitiesEvent;
import com.gootax.client.models.City;
import com.gootax.client.models.Option;
import com.gootax.client.models.Tariff;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;


public class CitiesRequestListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        EventBus.getDefault().post(new SaveCitiesEvent(false,
                R.string.activities_OptionsActivity_progress_conn_error));
    }

    @Override
    public void onRequestSuccess(Response response) {
        boolean isSaved = false;
        try {
            JSONArray jsonArray = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getJSONObject("result").getJSONArray("city_list");

            if (jsonArray.length() > 0) {
                Option.deleteAllOptions();
                Tariff.deleteAllTariffs();
                City.deleteAllCities();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                City city = new City(jsonArray.getJSONObject(i).getString("city_id"),
                        jsonArray.getJSONObject(i).getString("city_name"),
                        Double.valueOf(jsonArray.getJSONObject(i).getString("city_lat")),
                        Double.valueOf(jsonArray.getJSONObject(i).getString("city_lon")),
                        jsonArray.getJSONObject(i).getString("currency"),
                        jsonArray.getJSONObject(i).getString("phone"),
                        jsonArray.getJSONObject(i).getString("city_reseption_area"));
                city.save();
            }
            isSaved = true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int infoText = isSaved ?
                R.string.activities_OptionsActivity_progress_success :
                R.string.activities_OptionsActivity_progress_acc_error;
        EventBus.getDefault().post(new SaveCitiesEvent(isSaved, infoText));
    }

}
