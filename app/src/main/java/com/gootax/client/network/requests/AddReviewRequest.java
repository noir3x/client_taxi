package com.gootax.client.network.requests;

import android.content.Context;

import com.gootax.client.app.AppParams;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class AddReviewRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String current_time;
    private String order_id;
    private String grade;
    private String text = "";

    public AddReviewRequest(Context context, String order_id,
                            String grade, String text, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.current_time = String.valueOf(new Date().getTime());
        this.order_id = order_id;
        this.grade = grade;
        try {
            this.text = URLEncoder.encode(text, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        LinkedHashMap<String,String> params = new LinkedHashMap<>();
        params.put("current_time", current_time);
        params.put("grade", this.grade);
        params.put("order_id",this.order_id);
        params.put("text", this.text);
        this.signature = HashMD5.getSignature(params);

        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);

    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().addReview(signature, typeclient,
                tenantid, lang, deviceid, current_time, grade, order_id, text);
    }
}
