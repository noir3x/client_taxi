package com.gootax.client.network.listeners;


import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.CallCostEvent;

import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CostListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("CALLCOST", new String(((TypedByteArray)
                    response.getBody()).getBytes()));
            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            if (jsonObject.getString("info").equals("OK")) {
                EventBus.getDefault().post(new CallCostEvent(jsonObject.getJSONObject("result").getJSONObject("cost_result").getString("summary_cost"),
                        jsonObject.getJSONObject("result").getJSONObject("cost_result").getString("summary_distance"),
                        jsonObject.getJSONObject("result").getJSONObject("cost_result").getString("summary_time")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
