package com.gootax.client.network.requests;


import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class PostAcceptPassRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String phone;
    private String password;
    private int city_id;
    private String time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;

    public PostAcceptPassRequest(Context context, String phone,
                                 String password, int city_id, String time, String tenantid) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", String.valueOf(city_id));
        requestParams.put("current_time", time);
        requestParams.put("password", password);
        requestParams.put("phone", phone);

        this.phone = phone;
        this.password = password;
        this.city_id = city_id;
        this.time = time;
        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().acceptPassword(signature,
                typeclient, tenantid, lang, deviceid, city_id, time, password, phone);
    }

}
