package com.gootax.client.network.listeners;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.CreateCardEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CreateCardListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        String url = "";
        String orderId = "";
        boolean hasError = false;
        try {
            JSONObject json = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");
            url = json.getString("url");
            orderId = json.getString("orderId");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        url = url.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
        if (url.isEmpty() && orderId.isEmpty()) hasError = true;
        EventBus.getDefault().post(new CreateCardEvent(url, orderId, hasError));
    }

}
