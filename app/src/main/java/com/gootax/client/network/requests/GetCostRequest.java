package com.gootax.client.network.requests;


import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.app.AppParams;
import com.gootax.client.models.Address;
import com.gootax.client.models.Option;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.models.Tariff;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.AppPreferences;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit.client.Response;

public class GetCostRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;

    private String additional_options = "";
    private String address = "";
    private String city_id = "";
    private String client_id = "";
    private String client_phone = "";
    private String comment = "";
    private String company_id = "";
    private String device_token = "";
    private String order_time = "";
    private String pan = "";
    private String pay_type = "";
    private String tariff_id = "";
    private String time;
    private String type_request = "";
    private int bonus_payment = 0;

    public GetCostRequest(Context context, String time, 
                          Order order, Profile profile, List<Address> addressList) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        JSONObject jsonObjectSource = new JSONObject();
        JSONArray jsonArrayAddress = new JSONArray();


        int count = 0;
        for (Address address : addressList) {
            if (!address.isEmpty()) {
                try {
                    JSONObject jsonObjectAddressFrom = new JSONObject();
                    jsonObjectAddressFrom.put("city_id", profile.getCityId());
                    jsonObjectAddressFrom.put("city", address.getCity());
                    if (address.getType().equals("house"))
                        jsonObjectAddressFrom.put("street", address.getStreet());
                    else
                        jsonObjectAddressFrom.put("street", address.getLabel());
                    jsonObjectAddressFrom.put("house", address.getHouse());
                    jsonObjectAddressFrom.put("housing", address.getHousing());
                    jsonObjectAddressFrom.put("porch", address.getPorch());
                    jsonObjectAddressFrom.put("apt", address.getApt());
                    jsonObjectAddressFrom.put("lat", address.getLat());
                    jsonObjectAddressFrom.put("lon", address.getLon());
                    jsonArrayAddress.put(jsonObjectAddressFrom);
                    count++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        this.bonus_payment = profile.getPaymentBonus();

        try {
            jsonObjectSource.put("address", jsonArrayAddress);
            this.address = URLEncoder.encode(jsonObjectSource.toString(), "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException | JSONException e) {
            e.printStackTrace();
        }

        this.client_phone = profile.getPhone();
        this.client_id = profile.getClientId();
        this.tariff_id = order.getTariff();
        this.time = time;
        this.type_request = "2";
        this.city_id = profile.getCityId();
        this.device_token = new AppPreferences(context).getText("device_token");
        this.order_time = order.getOrderTime();
        this.pay_type = profile.getPaymentType();
        if (pay_type.equals("CARD")) pan = profile.getPan();
        if (pay_type.equals("CORP_BALANCE")) company_id = profile.getCompany();

        try {
            StringBuilder sbOptions = new StringBuilder();
            List<Option> options = Option.getOptionsChecked(Tariff.getTariffById(order.getTariff()));
            for (Option option : options) {
                sbOptions.append(option.getOptionIdSecondary());
                sbOptions.append(",");
            }

            if (options.size() > 0) {
                try {
                    this.additional_options = URLEncoder.encode(sbOptions.toString().substring(0, sbOptions.toString().length() - 1), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        requestParams.put("additional_options", this.additional_options);
        requestParams.put("address", this.address);
        requestParams.put("city_id", this.city_id);
        requestParams.put("client_id", this.client_id);
        requestParams.put("client_phone", this.client_phone);
        requestParams.put("comment", "");
        requestParams.put("company_id", company_id);
        requestParams.put("current_time", this.time);
        requestParams.put("device_token", this.device_token);

        try {
            requestParams.put("order_time", URLEncoder.encode(this.order_time, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        requestParams.put("pan", this.pan);
        requestParams.put("pay_type", this.pay_type);
        requestParams.put("tariff_id", this.tariff_id);
        requestParams.put("type_request", this.type_request);
        requestParams.put("bonus_payment", String.valueOf(this.bonus_payment));

        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = profile.getTenantId();
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);

    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().createOrder(signature, typeclient, tenantid, lang, deviceid,
                additional_options, address, city_id, client_id, client_phone, comment,
                company_id, time, device_token, order_time, pan, pay_type, tariff_id, type_request, bonus_payment);
    }
}
