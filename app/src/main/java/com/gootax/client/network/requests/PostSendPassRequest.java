package com.gootax.client.network.requests;


import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class PostSendPassRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String phone;
    private String current_time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;

    public PostSendPassRequest(Context context, String phone,
                               String current_time, String tenantid) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", current_time);
        requestParams.put("phone", phone);

        this.phone = phone;
        this.current_time = current_time;
        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().sendPassword(signature,
                typeclient, tenantid, lang, deviceid, current_time, phone);
    }

}
