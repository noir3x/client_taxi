package com.gootax.client.network.listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.CarFreeEvent;
import com.gootax.client.models.Car;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CarsListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {

    }

    @Override
    public void onRequestSuccess(Response response) {
        //{"result":{"cars":[{"car_lat":"56.844150","car_info":"Alfa Romeo Giulietta Бежевый","degree":"-1.000","car_lon":"53.242359","car_id":"1558","car_number":"","car_is_free":1,"driver_callsign":"246"}]},
        // "info":"OK","code":0}

        try {
            Log.d("CARS", new String(((TypedByteArray) response.getBody()).getBytes()));

            JSONObject jsonObject = new JSONObject(new String(((TypedByteArray) response.getBody()).getBytes()));
            if (jsonObject.getString("info").equals("OK")) {
                JSONArray jsonArrayCar = jsonObject.getJSONObject("result").getJSONArray("cars");
                List<Car> carList = new ArrayList<>();
                for (int i = 0; i < jsonArrayCar.length(); i++) {
                    Car car = new Car(Double.valueOf(jsonArrayCar.getJSONObject(i).getString("car_lat")),
                            Double.valueOf(jsonArrayCar.getJSONObject(i).getString("car_lon")),
                            Float.valueOf(jsonArrayCar.getJSONObject(i).getString("degree")));
                    carList.add(car);
                }
                EventBus.getDefault().post(new CarFreeEvent(carList));
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
