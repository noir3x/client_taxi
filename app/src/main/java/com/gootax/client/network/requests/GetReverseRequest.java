package com.gootax.client.network.requests;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.network.interfaces.IGeoGootaxApi;
import com.gootax.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetReverseRequest extends RetrofitSpiceRequest<Response, IGeoGootaxApi> {

    private String cityId;
    private String lat;
    private String lon;
    private String typeApp;
    private String tenantId;
    private String format;
    private String lang;
    private String rad;
    private String size;
    private String hash;

    public GetReverseRequest(String cityId, String lat, String lon,
                             String rad, String size, String tenantId) {
        super(Response.class, IGeoGootaxApi.class);
        this.cityId = cityId;
        this.lat = lat;
        this.lon = lon;
        this.typeApp = "client";
        this.tenantId = tenantId;
        this.format = "gootax";
        this.lang = Locale.getDefault().getLanguage();
        this.rad = rad;
        this.size = size;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", cityId);
        requestParams.put("format", format);
        requestParams.put("lang", lang);
        requestParams.put("point.lat", lat);
        requestParams.put("point.lon", lon);
        requestParams.put("radius", rad);
        requestParams.put("size", size);
        requestParams.put("tenant_id", tenantId);
        requestParams.put("type_app", typeApp);
        this.hash = HashMD5.getSignature(requestParams);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getReverseResult(cityId, format, hash, lang, lat, lon,
                rad, size, tenantId, typeApp);
    }

}
