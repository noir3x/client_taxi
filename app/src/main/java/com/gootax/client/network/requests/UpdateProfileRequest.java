package com.gootax.client.network.requests;

import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class UpdateProfileRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String clientId;
    private String oldPhone;
    private String newPhone;
    private String surname;
    private String name;
    private String email;
    private String current_time;
    private TypedFile photo;

    public UpdateProfileRequest(Context context, String clientId, String oldPhone,
                                String newPhone, String surname, String name,
                                String email, String time, TypedFile photo, String tenantid) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("client_id", clientId);
        requestParams.put("current_time", time);
        try {
            requestParams.put("email", URLEncoder.encode(email, "UTF-8"));
            requestParams.put("name", URLEncoder.encode(name, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        requestParams.put("new_phone", newPhone);
        requestParams.put("old_phone", oldPhone);
        try {
            requestParams.put("surname", URLEncoder.encode(surname, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
        this.clientId = clientId;
        this.oldPhone = oldPhone;
        this.newPhone = newPhone;
        this.surname = surname;
        this.name = name;
        this.email = email;
        this.current_time = time;
        this.photo = photo;
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().updateProfile(signature, typeclient, tenantid, lang, deviceid,
                clientId, current_time, email, name, newPhone, oldPhone, photo, surname);
    }

}
