package com.gootax.client.network.listeners.geo_listeners;

import com.gootax.client.events.geo_events.SearchEvent;

public class SearchListener extends GeoListener {

    public SearchListener() {
        super(new SearchEvent());
    }

}
