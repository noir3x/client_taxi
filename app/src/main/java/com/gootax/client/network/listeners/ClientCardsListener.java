package com.gootax.client.network.listeners;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.ClientCardsEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class ClientCardsListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        ArrayList<String> cards = new ArrayList<>();
        try {
            JSONArray jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONArray("result");
            for (int i = 0; i < jsonResult.length(); i++) {
                String card = jsonResult.getString(i);
                cards.add(card);
            }

        } catch (JSONException | NullPointerException | IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new ClientCardsEvent(cards));
    }

}
