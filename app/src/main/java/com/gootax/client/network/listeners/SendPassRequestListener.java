package com.gootax.client.network.listeners;


import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.SignUpEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class SendPassRequestListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        JSONObject jsonResult;
        int success = 0;
        try {
            jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");
            success = jsonResult.getInt("password_result");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        EventBus.getDefault().post(new SignUpEvent(success));
    }

}
