package com.gootax.client.network.listeners.geo_listeners;

import com.gootax.client.events.geo_events.FullReverseEvent;

public class FullReverseListener extends GeoListener {

    public FullReverseListener() {
        super(new FullReverseEvent());
    }

}