package com.gootax.client.network.listeners;

import android.util.Log;

import com.activeandroid.query.Delete;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.ProfileEvent;
import com.gootax.client.models.Company;
import com.gootax.client.models.Profile;
import com.gootax.client.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class UpdateProfileRequestListener implements RequestListener<Response> {

    private Profile profile;
    private JSONObject jsonProfile;
    private List<Company> companies;

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        JSONObject jsonResult = null;
        jsonProfile = null;
        profile = Profile.getProfile();
        companies = new ArrayList<>();
        int success = 0;
        try {
            jsonResult = new JSONObject(new String(((TypedByteArray) response.getBody())
                    .getBytes())).getJSONObject("result");
            jsonProfile = jsonResult.getJSONObject("client_profile");
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
        if (jsonResult != null && jsonProfile != null) {
            success = Validator.validateInt(jsonResult, "update_result");
            fillСommonFields();
            fillPaymentPersonal();
            fillPaymentCorp();
        }
        EventBus.getDefault().post(new ProfileEvent(profile, companies, success));
    }

    private void fillСommonFields() {
        profile.setClientId(Validator.validateStr(jsonProfile, "client_id"));
        profile.setSurname(Validator.validateStr(jsonProfile, "surname"));
        profile.setName(Validator.validateStr(jsonProfile, "name"));
        profile.setPatronymic(Validator.validateStr(jsonProfile, "patronymic"));
        profile.setEmail(Validator.validateStr(jsonProfile, "email"));
        profile.setBirth(Validator.validateStr(jsonProfile, "birth"));
        profile.setPhoto(Validator.validateStr(jsonProfile, "photo"));
    }

    private void fillPaymentPersonal() {
        JSONObject jsonBalance = Validator.validateJSONObj(jsonProfile, "personal_balance");
        profile.setBalanceValue(Validator
                    .validateStr(jsonBalance, "personal_balance_value"));
        profile.setBalanceCurrency(Validator
                    .validateStr(jsonBalance, "personal_balance_currency"));
        JSONArray jsonBonusArray = Validator.validateJSONArr(jsonProfile, "personal_bonus");
        JSONObject jsonBonus = null;
        if (jsonBonusArray != null) {
            try {
                jsonBonus = jsonBonusArray.getJSONObject(0);
            } catch (JSONException | NullPointerException ignored) {}
        }
        profile.setBonusValue(Validator.validateStr(jsonBonus, "personal_bonus_value"));
    }

    private void fillPaymentCorp() {
        JSONArray jsonCompanies = Validator.validateJSONArr(jsonProfile, "client_companies");
        if (jsonCompanies != null && jsonCompanies.length() > 0) {
            profile.setPaymentType("CORP_BALANCE");
            for (int i = 0; i < jsonCompanies.length(); i++) {
                try {
                    JSONObject jsonCompany = jsonCompanies.getJSONObject(i);
                    Company company = new Company();
                    company.setCompanyId(Validator.validateStr(jsonCompany, "company_id"));
                    company.setName(Validator.validateStr(jsonCompany, "company_name"));
                    company.setFullName(Validator.validateStr(jsonCompany, "company_full_name"));
                    company.setLogo(Validator.validateStr(jsonCompany, "company_logo"));
                    company.setProfile(profile);
                    companies.add(company);
                } catch (JSONException | NullPointerException ignored) {}
            }
        } else {
            new Delete().from(Company.class).execute();

            profile.setPaymentType("CASH");
        }
    }

}
