package com.gootax.client.network;

import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;


public class RetrofitSpiceService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(IGootaxApi.class);
    }

    @Override
    protected String getServerUrl() {
        return AppParams.URL;
    }
}
