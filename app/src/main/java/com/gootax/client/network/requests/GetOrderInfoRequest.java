package com.gootax.client.network.requests;

import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.app.AppParams;
import com.gootax.client.models.Order;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetOrderInfoRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;

    private String time;
    private String order_id;
    private String need_car_photo;
    private String need_driver_photo;

    public GetOrderInfoRequest(Context context, String time, Order order, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.time = time;
        if (AppParams.USE_CAR_PHOTO) {
            this.need_car_photo = "true";
            this.need_driver_photo = "false";
        } else {
            this.need_car_photo = "false";
            this.need_driver_photo = "true";
        }
        this.order_id = order.getOrderId();

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("current_time", time);
        requestParams.put("need_car_photo", this.need_car_photo);
        requestParams.put("need_driver_photo", this.need_driver_photo);
        requestParams.put("order_id", this.order_id);

        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);

    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getOrderInfo(signature, typeclient, tenantid, lang, deviceid,
                time, need_car_photo, need_driver_photo, order_id);
    }

}
