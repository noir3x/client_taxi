package com.gootax.client.network.requests;

import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetCarsRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String current_time;
    private String city_id;
    private String radius;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;
    private String from_lat, from_lon;

    public GetCarsRequest(Context context, String city_id,
                          String lat, String lon, String tenantid) {
        super(Response.class, IGootaxApi.class);

        this.current_time = String.valueOf(new Date().getTime());
        this.from_lat = lat;
        this.from_lon = lon;
        this.radius = AppParams.RADIUS;

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", city_id);
        requestParams.put("current_time", this.current_time);
        requestParams.put("from_lat", this.from_lat);
        requestParams.put("from_lon", this.from_lon);
        requestParams.put("radius", radius);

        this.city_id = city_id;
        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getCars(signature, typeclient, tenantid,
                lang, deviceid, city_id, current_time, from_lat, from_lon, radius);
    }
}
