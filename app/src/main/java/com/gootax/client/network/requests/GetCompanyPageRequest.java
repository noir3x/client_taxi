package com.gootax.client.network.requests;


import android.content.Context;

import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;

public class GetCompanyPageRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String time;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;

    public GetCompanyPageRequest(Context context, String time, String tenantid) {
        super(Response.class, IGootaxApi.class);
        this.time = time;
        LinkedHashMap<String,String> params = new LinkedHashMap<>();
        params.put("current_time", time);
        this.signature = HashMD5.getSignature(params);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getCompanyPage(signature, typeclient, tenantid, lang, deviceid, time);
    }

}
