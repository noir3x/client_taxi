package com.gootax.client.network.requests;

import android.content.Context;

import com.gootax.client.app.App;
import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGootaxApi;
import com.gootax.client.utils.AppPreferences;
import com.gootax.client.utils.GenUDID;
import com.gootax.client.utils.HashMD5;
import com.octo.android.robospice.request.retrofit.RetrofitSpiceRequest;

import java.util.LinkedHashMap;
import java.util.Locale;

import retrofit.client.Response;


public class GetTariffsListRequest extends RetrofitSpiceRequest<Response, IGootaxApi> {

    private String time;
    private String city_id;
    private String signature;
    private String typeclient;
    private String tenantid;
    private String lang;
    private String deviceid;

    public GetTariffsListRequest(Context context, String time, String city_id, String tenantid) {
        super(Response.class, IGootaxApi.class);

        LinkedHashMap<String,String> requestParams = new LinkedHashMap<>();
        requestParams.put("city_id", city_id);
        requestParams.put("current_time", time);

        this.time = time;
        this.city_id = city_id;
        this.signature = HashMD5.getSignature(requestParams);
        this.typeclient = "android";
        this.tenantid = tenantid;
        this.lang = Locale.getDefault().getLanguage();
        this.deviceid = GenUDID.getUDID(context);

        AppPreferences appPreferences = new AppPreferences(context);
        appPreferences.saveText("save_tariff", String.valueOf(time));
    }

    @Override
    public Response loadDataFromNetwork() throws Exception {
        return getService().getTariffs(signature, typeclient, tenantid, lang, deviceid, city_id, time);
    }

}
