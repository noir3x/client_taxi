package com.gootax.client.network.listeners.geo_listeners;

import com.gootax.client.events.geo_events.AutoCompleteEvent;

public class AutoCompleteListener extends GeoListener {

    public AutoCompleteListener() {
        super(new AutoCompleteEvent());
    }

}
