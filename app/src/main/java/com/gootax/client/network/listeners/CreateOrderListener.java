package com.gootax.client.network.listeners;

import android.util.Log;

import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.events.CreateOrderEvent;

import org.json.JSONException;
import org.json.JSONObject;

import org.greenrobot.eventbus.EventBus;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class CreateOrderListener implements RequestListener<Response> {
    @Override
    public void onRequestFailure(SpiceException spiceException) {
        try {
            Log.d("CREATE_ORDER", "fail");
            spiceException.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            Log.d("CREATE_ORDER", new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            //{"code":0,"info":"OK","result":{"type":"pre_order","order_id":29748,"order_number":2988}}


            JSONObject jsonOrder = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes()));

            if (jsonOrder.getString("info").equals("OK"))
                EventBus.getDefault().post(new CreateOrderEvent(jsonOrder.getJSONObject("result").getString("order_id"),
                        jsonOrder.getJSONObject("result").getString("order_number"),
                        jsonOrder.getString("info"), jsonOrder.getJSONObject("result").getString("type")));
            else
                EventBus.getDefault().post(new CreateOrderEvent(null, null, jsonOrder.getString("info"), null));

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
