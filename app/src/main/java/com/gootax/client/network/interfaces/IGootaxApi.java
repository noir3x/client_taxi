package com.gootax.client.network.interfaces;


import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

public interface IGootaxApi {

    @GET("/get_tenant_city_list")
    Response getCities(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Query("current_time") String current_time);


    @GET("/get_tariffs_list")
    Response getTariffs(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Query("city_id") String city_id,
            @Query("current_time") String current_time);

    @GET("/get_info_page")
    Response getCompanyPage(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Query("current_time") String current_time);

    @GET("/get_order_info")
    Response getOrderInfo(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Query("current_time") String current_time,
            @Query("need_car_photo") String need_car_photo,
            @Query("need_driver_photo") String need_driver_photo,
            @Query("order_id") String order_id);

    @GET("/get_cars")
    Response getCars(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Query("city_id") String city_id,
            @Query("current_time") String current_time,
            @Query("from_lat") String from_lat,
            @Query("from_lon") String from_lon,
            @Query("radius") String radius);

    @FormUrlEncoded
    @POST("/send_password")
    Response sendPassword(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Field("current_time") String current_time,
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/accept_password")
    Response acceptPassword(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Field("city_id") int city_id,
            @Field("current_time") String current_time,
            @Field("password") String password,
            @Field("phone") String phone);

    @Multipart
    @POST("/update_client_profile")
    Response updateProfile(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Part("client_id") String clientId,
            @Part("current_time") String current_time,
            @Part("email") String email,
            @Part("name") String name,
            @Part("new_phone") String newPhone,
            @Part("old_phone") String oldPhone,
            @Part("photo") TypedFile photo,
            @Part("surname") String surname);

    @FormUrlEncoded
    @POST("/create_order")
    Response createOrder(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Field("additional_options") String additional_options,
            @Field("address") String address,
            @Field("city_id") String city_id,
            @Field("client_id") String client_id,
            @Field("client_phone") String client_phone,
            @Field("comment") String comment,
            @Field("company_id") String company_id,
            @Field("current_time") String time,
            @Field("device_token") String device_token,
            @Field("order_time") String order_time,
            @Field("pan") String pan,
            @Field("pay_type") String pay_type,
            @Field("tariff_id") String tariff_id,
            @Field("type_request") String type_request,
            @Field("bonus_payment") int bonus_payment);

    @FormUrlEncoded
    @POST("/reject_order")
    Response rejectOrder(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Field("current_time") String time,
            @Field("order_id") String order_id);

    @FormUrlEncoded
    @POST("/send_response")
    Response addReview(
            @Header("signature") String signature,
            @Header("typeclient") String typeclient,
            @Header("tenantid") String tenantid,
            @Header("lang") String lang,
            @Header("deviceid") String deviceid,
            @Field("current_time") String current_time,
            @Field("grade") String grade,
            @Field("order_id") String order_id,
            @Field("text") String text);

    @FormUrlEncoded
    @POST("/create_client_card")
    Response postCreateClientCard(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Field("client_id") String clientId,
            @Field("current_time") String currentTime,
            @Field("phone") String phone);

    @FormUrlEncoded
    @POST("/check_client_card")
    Response postCheckClientCard(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Field("client_id") String clientId,
            @Field("current_time") String currentTime,
            @Field("order_id") String orderId,
            @Field("phone") String phone);

    @GET("/get_client_cards")
    Response getClientCards(
            @Header("signature") String signature,
            @Header("tenantid") String tenantid,
            @Query("client_id") String clientId,
            @Query("current_time") String currentTime,
            @Query("phone") String phone);

}
