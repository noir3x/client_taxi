package com.gootax.client.network.listeners;


import android.util.Log;

import com.gootax.client.events.SaveTariffsEvent;
import com.gootax.client.utils.AppPreferences;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.gootax.client.models.City;
import com.gootax.client.models.Option;
import com.gootax.client.models.Tariff;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

public class TariffsRequestListener implements RequestListener<Response> {

    @Override
    public void onRequestFailure(SpiceException spiceException) {
        //
    }

    @Override
    public void onRequestSuccess(Response response) {
        try {
            JSONArray jsonArrayTariffs = new JSONObject(new String(((TypedByteArray)
                    response.getBody()).getBytes())).getJSONArray("result");
            /*
            for (int i = 0; i < jsonArrayTariffs.length(); i++) {
                City city = City.getCity(jsonArrayTariffs.getJSONObject(i).getString("city_id"));
                if (city != null) {
                    Tariff tariff = new Tariff();
                    tariff.setCityID(city.getId());
                    tariff.setTariffId(jsonArrayTariffs.getJSONObject(i).getString("tariff_id"));
                    tariff.setTariffName(jsonArrayTariffs.getJSONObject(i).getString("tariff_name"));
                    tariff.setTariffIcon(jsonArrayTariffs.getJSONObject(i).getString("logo"));
                    tariff.setTariffIconMini(jsonArrayTariffs.getJSONObject(i).getString("logo_thumb"));
                    tariff.setDescription(jsonArrayTariffs.getJSONObject(i).getString("description"));
                    tariff.save();
                    try {
                        JSONArray jsonArrayOptions = new JSONArray(jsonArrayTariffs.getJSONObject(i).getString("additional_options"));
                        for (int k = 0; k < jsonArrayOptions.length(); k++) {
                            Option option = new Option();
                            option.setOptionIdPrymary(jsonArrayOptions.getJSONObject(k).getString("id"));
                            option.setOptionIdSecondary(jsonArrayOptions.getJSONObject(k).getString("additional_option_id"));
                            option.setPrice(jsonArrayOptions.getJSONObject(k).getString("price"));
                            option.setTariffType(jsonArrayOptions.getJSONObject(k).getString("tariff_type"));
                            option.setTariffID(tariff.getId());
                            option.setOptionName(jsonArrayOptions.getJSONObject(k).getJSONObject("option").getString("name"));
                            option.setChecked(false);
                            option.save();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            */
            List<Tariff> tariffList = new ArrayList<>();
            List<Option> optionList = new ArrayList<>();
            Option.deleteAllOptions();
            Tariff.deleteAllTariffs();
            for (int i = 0; i < jsonArrayTariffs.length(); i++) {
                City city = City.getCity(jsonArrayTariffs.getJSONObject(i).getString("city_id"));
                if (city != null) {
                    Tariff tariff = new Tariff();
                    tariff.setCityID(city.getId());
                    tariff.setTariffId(jsonArrayTariffs.getJSONObject(i).getString("tariff_id"));
                    tariff.setTariffName(jsonArrayTariffs.getJSONObject(i).getString("tariff_name"));
                    tariff.setTariffIcon(jsonArrayTariffs.getJSONObject(i).getString("logo"));
                    tariff.setTariffIconMini(jsonArrayTariffs.getJSONObject(i).getString("logo_thumb"));
                    tariff.setDescription(jsonArrayTariffs.getJSONObject(i).getString("description"));
                    tariffList.add(tariff);
                    tariff.save();
                    try {
                        JSONArray jsonArrayOptions = new JSONArray(jsonArrayTariffs.getJSONObject(i).getString("additional_options"));
                        for (int k = 0; k < jsonArrayOptions.length(); k++) {
                            Option option = new Option();
                            option.setOptionIdPrymary(jsonArrayOptions.getJSONObject(k).getString("id"));
                            option.setOptionIdSecondary(jsonArrayOptions.getJSONObject(k).getString("additional_option_id"));
                            option.setPrice(jsonArrayOptions.getJSONObject(k).getString("price"));
                            option.setTariffType(jsonArrayOptions.getJSONObject(k).getString("tariff_type"));
                            option.setTariffID(tariff.getId());
                            option.setOptionName(jsonArrayOptions.getJSONObject(k).getJSONObject("option").getString("name"));
                            option.setChecked(false);
                            option.save();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            EventBus.getDefault().post(new SaveTariffsEvent(tariffList, optionList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
