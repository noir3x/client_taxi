package com.gootax.client.network;


import com.octo.android.robospice.retrofit.RetrofitGsonSpiceService;
import com.gootax.client.app.AppParams;
import com.gootax.client.network.interfaces.IGeoGootaxApi;

public class GeoSpiceService extends RetrofitGsonSpiceService {

    @Override
    public void onCreate() {
        super.onCreate();
        addRetrofitInterface(IGeoGootaxApi.class);
    }

    @Override
    protected String getServerUrl() {
        return AppParams.GEO_URL;
    }

}
