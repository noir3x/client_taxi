package com.gootax.client.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gootax.client.R;
import com.gootax.client.activities.AutoCompleteActivity;
import com.gootax.client.activities.MainActivity;
import com.gootax.client.events.CoordsForReverseEvent;
import com.gootax.client.events.DeleteItemListEvent;
import com.gootax.client.events.PolygonDetectEvent;
import com.gootax.client.events.UpdatePointsEvent;
import com.gootax.client.events.geo_events.ReverseEvent;
import com.gootax.client.models.Address;
import com.gootax.client.utils.AddressList;
import com.gootax.client.views.adapters.AddressAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class MainHeaderFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ListView listView;
    private AddressAdapter addressAdapter;
    private String latForReverse;
    private String lonForReverse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_header, container, false);

        listView = (ListView) view.findViewById(R.id.lv_header);
        addressAdapter = new AddressAdapter(getActivity(),
                ((MainActivity) getActivity()).getAddressList());
        listView.setOnItemClickListener(this);
        listView.setAdapter(addressAdapter);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String[] coords = ((MainFragment) getParentFragment()).callMapFragmentForCoords();
        Intent autoCompleteIntent = new Intent(getActivity(), AutoCompleteActivity.class);
        autoCompleteIntent.putExtra(MainActivity.POINT_NUM_KEY, i);
        autoCompleteIntent.putExtra(AutoCompleteActivity.USER_COORDS_KEY, coords);
        startActivity(autoCompleteIntent);
    }

    public void updateAddressList() {
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();
        addressAdapter.updateList(addressList, true);
        if (addressList.size() > 1 && addressList.size() < 5) {
            ((MainFragment) getParentFragment()).showFab();
        } else {
            ((MainFragment) getParentFragment()).hideFab();
        }
    }

    @Subscribe
    public void onMessage(ReverseEvent event) {
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();
        if (((MainFragment) getParentFragment()).enableAddress) {
            if (addressList.size() <= 1) {
                Address address = event.getAddress();
                boolean isGpsAddress = event.isGpsAddress();
                if (isGpsAddress) {
                    String gpsAddress =
                            getString(R.string.fragments_MainHeaderFragment_gps_address);
                    address.setLabel(gpsAddress);
                    address.setStreet(gpsAddress);
                    address.setCity("");
                    address.setLat(latForReverse);
                    address.setLon(lonForReverse);
                    address.setGps(true);
                }
                ((MainActivity) getActivity()).getAddressList().set(0, address);
                addressAdapter.updateList(addressList, true);
                if (addressList.size() > 1 && addressList.size() < 5) {
                    ((MainFragment) getParentFragment()).showFab();
                } else {
                    ((MainFragment) getParentFragment()).hideFab();
                }
            }
        }
        ((MainFragment) getParentFragment()).enableAddress = true;
    }

    @Subscribe
    public void onMessage(PolygonDetectEvent polygonDetectEvent) {
        if (polygonDetectEvent.getCity() == null) {
            ((MainFragment) getParentFragment()).enableAddress = false;
            Address address = new Address();
            address.setLabel(getString(R.string.fragments_MainHeaderFragment_unknown_city));
            ((MainActivity) getActivity()).getAddressList().set(0, address);
            addressAdapter.updateList(((MainActivity) getActivity()).getAddressList(), false);
            ((MainFragment) getParentFragment()).hideFab();
        } else {
            ((MainFragment) getParentFragment()).enableAddress = true;
        }
    }

    public void addItemList(int position) {
        String[] coords = ((MainFragment) getParentFragment()).callMapFragmentForCoords();
        Intent autoCompleteIntent = new Intent(getActivity(), AutoCompleteActivity.class);
        autoCompleteIntent.putExtra(MainActivity.POINT_NUM_KEY, position);
        autoCompleteIntent.putExtra(AutoCompleteActivity.USER_COORDS_KEY, coords);
        startActivity(autoCompleteIntent);
    }

    @Subscribe
    public void onMessage(DeleteItemListEvent deleteItemListEvent) {
        int position = deleteItemListEvent.getPosition();
        List<Address> addressList = ((MainActivity) getActivity()).getAddressList();
        if (addressList.size() == 2 && position == AddressList.SECOND_POINT) {
            // set second point to empty state
            ((MainActivity) getActivity()).getAddressList()
                    .get(AddressList.SECOND_POINT).setEmpty(true);
            ((MainFragment) getParentFragment()).hideFab();
        } else {
            // remove point
            ((MainActivity) getActivity()).getAddressList()
                    .remove(position);
        }
        addressAdapter = new AddressAdapter(getActivity(), addressList);
        listView.setAdapter(addressAdapter);
        EventBus.getDefault().post(new UpdatePointsEvent());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessage(CoordsForReverseEvent event) {
        latForReverse = event.getLat();
        lonForReverse = event.getLon();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}
