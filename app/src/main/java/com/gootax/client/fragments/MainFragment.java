package com.gootax.client.fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.gootax.client.R;
import com.gootax.client.activities.AuthActivity;
import com.gootax.client.activities.DetailAddressActivity;
import com.gootax.client.activities.MainActivity;
import com.gootax.client.activities.OrderCompletedActivity;
import com.gootax.client.activities.OrderDetailActivity;
import com.gootax.client.activities.PreOrderActivity;
import com.gootax.client.activities.WishesActivity;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.CarBusyEvent;
import com.gootax.client.events.CreateOrderEvent;
import com.gootax.client.events.GetCurrentLocationEvent;
import com.gootax.client.events.MotionBlockMapEvent;
import com.gootax.client.events.MotionCameraChangeEvent;
import com.gootax.client.events.MotionDownEvent;
import com.gootax.client.events.MotionUnblockMapEvent;
import com.gootax.client.events.MotionUpEvent;
import com.gootax.client.events.OrderInfoEvent;
import com.gootax.client.events.PointsMapEvent;
import com.gootax.client.events.PolygonDetectEvent;
import com.gootax.client.events.RestoreFooterEvent;
import com.gootax.client.maps.FragmentAbstractMap;
import com.gootax.client.maps.MapFactory;
import com.gootax.client.models.Address;
import com.gootax.client.models.Car;
import com.gootax.client.models.City;
import com.gootax.client.models.Option;
import com.gootax.client.models.Profile;
import com.gootax.client.models.RoutePoint;
import com.gootax.client.network.listeners.CarsListener;
import com.gootax.client.network.listeners.OrderInfoListener;
import com.gootax.client.network.listeners.RejectOrderListener;
import com.gootax.client.network.requests.GetCarsRequest;
import com.gootax.client.network.requests.GetOrderInfoRequest;
import com.gootax.client.network.requests.RejectOrderRequest;
import com.gootax.client.utils.AnalyticsHelper;
import com.gootax.client.views.DateTimeListener;
import com.gootax.client.views.ToastWrapper;
import com.gootax.client.views.menu.Menu;
import com.gootax.client.views.menu.MenuItem;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainFragment extends BaseSpiceFragment {

    private LinearLayout llUp, llDown, llMenu;

    private boolean bMotionEventHide;
    private boolean upEvent, cameraChangeEvent;
    private boolean isActive = false;

    private Menu menu;
    private String statusOrder;
    private Fragment footerFragment, headerFragment;
    private ProgressDialog cancelProgressDialog;
    private ImageButton ibCurrentPosition;
    private FloatingActionButton addItemFab;

    private Handler getInfoHandler;
    private Runnable getInfoRunnable;

    public static MainFragment getInstance() {
        return new MainFragment();
    }

    public boolean enableAddress;

    private Profile profile;
    public String driverPhone;
    private City city;
    private TypedValue typedValue;
    private TypedValue typedValueText;

    public MainFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // GOVNOCODE
        profile = Profile.getProfile();
        typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.content_bg, typedValue, true);
        typedValueText = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        bMotionEventHide = true;
        upEvent = true;
        cameraChangeEvent = true;
        enableAddress = true;
        driverPhone = "";

        statusOrder = "empty";

        city = City.getCity(profile.getCityId());

        cancelProgressDialog = new ProgressDialog(getActivity());

        ibCurrentPosition = (ImageButton) view.findViewById(R.id.ib_current_position);

        Fragment mapFragment = MapFactory.getFragmentMap(getActivity().getApplicationContext());
        getChildFragmentManager()
                .beginTransaction()
                .replace(R.id.mapContainer, mapFragment, "main")
                .commit();

        if (((MainActivity) getActivity()).getOrder().getStatus().equals("empty")) {

            //getActivity().setTitle(getString(R.string.activities_MainActivity_title));
            ((MainActivity) getActivity()).setTitleText(getString(R.string.activities_MainActivity_title));

            footerFragment = new MainFooterFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_footer_fragment, footerFragment, "mainFooter")
                    .commit();

            headerFragment = new MainHeaderFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_header_fragment, headerFragment, "mainHeader")
                    .commit();

            List<MenuItem> menuItems = new ArrayList<>();
            String timeMenuTitle = ((MainActivity) getActivity()).getOrder().getOrderTime();
            if (timeMenuTitle == null || timeMenuTitle.isEmpty())
                timeMenuTitle = getString(R.string.menu_footer_clock);
            menuItems.add(new MenuItem(R.drawable.clock, timeMenuTitle,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (isActive) showDateTimeDialog();
                        }
                    }));
            menuItems.add(new MenuItem(R.drawable.pogelania, getString(R.string.menu_footer_pogelania), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((MainActivity) getActivity()).getOrder().getTariff() != null && ((MainActivity) getActivity()).getOrder().getTariff().length() == 0) {
                        getActivity();
                    } else {
                        getActivity().startActivityForResult(new Intent(getActivity(), WishesActivity.class).putExtra("tariff_id", ((MainActivity) getActivity()).getOrder().getTariff()), MainActivity.REQUEST_CODE_WISHES);
                    }
                }
            }));
            menuItems.add(new MenuItem(R.drawable.adress, getString(R.string.menu_footer_adress), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intentAddress = new Intent(getActivity(), DetailAddressActivity.class);
                    Address firstAddress = ((MainActivity) getActivity()).getAddressList().get(0);
                    if (firstAddress.isGps()) {
                        intentAddress.putExtra("street", firstAddress.getStreet());
                    }
                    intentAddress.putExtra("porch", firstAddress.getPorch());
                    intentAddress.putExtra("apt", firstAddress.getApt());
                    intentAddress.putExtra("comment", ((MainActivity) getActivity()).getOrder().getComment());
                    getActivity().startActivityForResult(intentAddress, MainActivity.REQUEST_CODE_DETAIL);
                }
            }));
            menu = new Menu(menuItems, (LinearLayout) view.findViewById(R.id.ll_menu), getActivity(), 0, 0);
        } else {
            ((MainActivity) getActivity()).activeOrder = true;

            view.findViewById(R.id.iv_center).setVisibility(View.GONE);

            List<Address> addressList = RoutePoint.getRoute(((MainActivity) getActivity()).getOrder());
            EventBus.getDefault().post(new PointsMapEvent(addressList, ((MainActivity) getActivity()).activeOrder, ((MainActivity) getActivity()).getOrder().getStatus(), ""));

            headerFragment = new TrackingOrderHeaderFragment();
            getChildFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fl_header_fragment, headerFragment, "mainHeader")
                    .commit();

            List<MenuItem> menuItems = new ArrayList<>();
            if (AppParams.USE_CALLS) {
                menuItems.add(new MenuItem(R.drawable.phone, getString(R.string.menu_footer_call), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if ((city != null && city.getPhone().length() > 0) || driverPhone.length() > 1) {
                            DialogFragment dialogFragment = new CallDialogFragment();
                            dialogFragment.show(getChildFragmentManager(), "dialogFragmentCall");
                        }
                    }
                }));
            }
            menuItems.add(new MenuItem(R.drawable.cencel_disabled, getString(R.string.menu_footer_rejectedorder), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (statusOrder.equals("new") || statusOrder.equals("car_assigned")) {
                        try {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle(getString(R.string.fragments_MainFragment_delete_order1));
                            alertDialog.setMessage(getString(R.string.fragments_MainFragment_delete_order2));
                            alertDialog.setPositiveButton(getString(R.string.fragments_MainFragment_yes),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            //(findViewById(R.id.btn_cancel_order)).setEnabled(false);
                                            cancelProgressDialog.setMessage(getResources()
                                                    .getString(R.string.fragments_MainFragment_delete_order_execute));
                                            cancelProgressDialog.setCancelable(false);
                                            cancelProgressDialog.show();
                                            getSpiceManager().execute(new RejectOrderRequest(getActivity(),
                                                            String.valueOf((new Date()).getTime()),
                                                            ((MainActivity) getActivity()).getOrder(),
                                                            profile.getTenantId()),
                                                    new RejectOrderListener());
                                        }
                                    });
                            alertDialog.setNegativeButton(getString(R.string.fragments_MainFragment_no),
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.cancel();
                                        }
                                    });
                            alertDialog.show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }));
            menu = new Menu(menuItems, (LinearLayout) view.findViewById(R.id.ll_menu), getActivity(), typedValue.data, typedValueText.data);

            if ((city != null && city.getPhone().length() > 0) || driverPhone.length() > 1) {
                menu.menuItemEnable(0, R.drawable.phone, true, typedValueText.data);
            } else {
                menu.menuItemEnable(0, R.drawable.phone_disabled, false, typedValueText.data);
            }

            //getActivity().setTitle(((MainActivity) getActivity()).getOrder().getStatusLabel());
            ((MainActivity) getActivity()).setTitleText(((MainActivity) getActivity()).getOrder().getStatusLabel());
            startOrderInfo();

            getSpiceManager().execute(new GetOrderInfoRequest(getActivity(),
                            String.valueOf((new Date()).getTime()),
                            ((MainActivity) getActivity()).getOrder(),
                            profile.getTenantId()),
                    new OrderInfoListener());
        }

        llDown = (LinearLayout) view.findViewById(R.id.ll_down);
        llUp = (LinearLayout) view.findViewById(R.id.ll_up);
        llMenu = (LinearLayout) view.findViewById(R.id.ll_menu);

        llDown.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        bMotionEventHide = false;
                        EventBus.getDefault().post(new MotionBlockMapEvent());
                        break;
                    case MotionEvent.ACTION_UP:
                        bMotionEventHide = true;
                        EventBus.getDefault().post(new MotionUnblockMapEvent());
                        break;
                }


                return false;
            }
        });

        llUp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        bMotionEventHide = false;
                        EventBus.getDefault().post(new MotionBlockMapEvent());
                        break;
                    case MotionEvent.ACTION_UP:
                        bMotionEventHide = true;
                        EventBus.getDefault().post(new MotionUnblockMapEvent());
                        break;
                }

                return false;
            }
        });

        ibCurrentPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableAddress = true;
                EventBus.getDefault().post(new GetCurrentLocationEvent());
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        customizeViews();
    }

    private void initViews(View view) {
        addItemFab = (FloatingActionButton)
                view.findViewById(R.id.frag_main_fab);
    }

    private void customizeViews() {
        addItemFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity = (MainActivity) getActivity();
                MainHeaderFragment headerFragment = (MainHeaderFragment)
                        getChildFragmentManager().findFragmentById(R.id.fl_header_fragment);
                int currentPosition = 1;
                if (activity != null) currentPosition = activity.getAddressList().size();
                if (headerFragment != null) headerFragment.addItemList(currentPosition);
            }
        });
        hideFab();
    }

    private void showDateTimeDialog() {
        Date timeToSet = null;
        String savedTime = ((MainActivity) getActivity()).getOrder().getOrderTime();
        if (!savedTime.isEmpty()) {
            SimpleDateFormat formatter = new SimpleDateFormat(
                    "dd.MM.yyyy HH:mm:ss", Locale.getDefault());
            try {
                timeToSet = formatter.parse(savedTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if (savedTime.isEmpty() || timeToSet == null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            cal.add(Calendar.MINUTE, 10);
            timeToSet = cal.getTime();
        }
        new SlideDateTimePicker.Builder(getChildFragmentManager())
                .setListener(new DateTimeListener())
                .setInitialDate(timeToSet)
                .setIs24HourTime(true)
                .setIndicatorColor(Color.parseColor("#" + Integer.toHexString(
                        ContextCompat.getColor(getContext(), R.color.colorPrimary))))
                .build()
                .show();
    }

    public void callUpdateAddressList() {
        ((MainHeaderFragment) getChildFragmentManager()
                .findFragmentByTag("mainHeader")).updateAddressList();
    }

    public void callUpdatePayment() {
        MainFooterFragment footerFragment = (MainFooterFragment) getChildFragmentManager()
                .findFragmentByTag("mainFooter");
        if (footerFragment != null) footerFragment.initPaymentViews();
    }

    public void callUpdateOrderTime() {
        String timeMenuTitle = ((MainActivity) getActivity()).getOrder().getOrderTime();
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd.MM.yyyy HH:mm:ss", Locale.getDefault());
        SimpleDateFormat newFormatter = new SimpleDateFormat(
                "dd.MM HH:mm", Locale.getDefault());
        try {
            timeMenuTitle = newFormatter.format(formatter.parse(timeMenuTitle));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (timeMenuTitle == null || timeMenuTitle.isEmpty())
            timeMenuTitle = getString(R.string.menu_footer_clock);
        MenuItem menuItem = new MenuItem(R.drawable.clock, timeMenuTitle,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isActive) showDateTimeDialog();
                    }
                });
        menu.updateMenuItem(menuItem, 0);
    }

    public String[] callMapFragmentForCoords() {
        return ((FragmentAbstractMap) getChildFragmentManager()
                .findFragmentByTag("main")).getMarkerCoords();
    }

    @Subscribe
    public void onMessage(CreateOrderEvent createOrderEvent) {
        String[] coords = ((FragmentAbstractMap)
                getChildFragmentManager().findFragmentByTag("main")).getMarkerCoords();
        switch (createOrderEvent.getStatusLabel()) {
            case "OK":
                AnalyticsHelper.sendEvent(AnalyticsHelper.METRICA_EVENT_CREATE_ORDER);
                if (createOrderEvent.getType().equals("new")) {
                    ((MainActivity) getActivity()).activeOrder = true;

                    ((MainActivity) getActivity()).getOrder().setStatus("new");
                    ((MainActivity) getActivity()).getOrder().setOrderId(createOrderEvent.getOrderId());
                    ((MainActivity) getActivity()).getOrder().setOrderNumber(createOrderEvent.getOrderNumber());
                    ((MainActivity) getActivity()).getOrder().save();

                    RoutePoint.saveOrderAddressesAndRoute(((MainActivity) getActivity()).getOrder(), ((MainActivity) getActivity()).getAddressList());
                    getActivity().findViewById(R.id.iv_center).setVisibility(View.GONE);
                    EventBus.getDefault().post(new PointsMapEvent(((MainActivity) getActivity()).getAddressList(),
                            ((MainActivity) getActivity()).activeOrder,
                            ((MainActivity) getActivity()).getOrder().getStatus(),
                            ""));

                    List<Option> optionList = Option.getAllOptions();
                    for (Option option : optionList) {
                        option.setChecked(false);
                        option.save();
                    }

                    getChildFragmentManager()
                            .beginTransaction()
                            .remove(footerFragment)
                            .commit();

                    headerFragment = new TrackingOrderHeaderFragment();
                    getChildFragmentManager()
                            .beginTransaction()
                            .replace(R.id.fl_header_fragment, headerFragment, "mainHeader")
                            .commit();

                    List<MenuItem> menuItems = new ArrayList<>();
                    if (AppParams.USE_CALLS) {
                        menuItems.add(new MenuItem(R.drawable.phone_disabled, getString(R.string.menu_footer_call), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if ((city != null && city.getPhone().length() > 0) || driverPhone.length() > 1) {
                                    DialogFragment dialogFragment = new CallDialogFragment();
                                    dialogFragment.show(getChildFragmentManager(), "dialogFragmentCall");
                                }
                            }
                        }));
                    }
                    menuItems.add(new MenuItem(R.drawable.cencel, getString(R.string.menu_footer_rejectedorder), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (statusOrder.equals("new") || statusOrder.equals("car_assigned")) {
                                try {
                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                                    alertDialog.setTitle(getString(R.string.fragments_MainFragment_delete_order1));
                                    alertDialog.setMessage(getString(R.string.fragments_MainFragment_delete_order2));
                                    alertDialog.setPositiveButton(getString(R.string.fragments_MainFragment_yes),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    //(findViewById(R.id.btn_cancel_order)).setEnabled(false);
                                                    cancelProgressDialog.setMessage(getResources()
                                                            .getString(R.string.fragments_MainFragment_delete_order_execute));
                                                    cancelProgressDialog.setCancelable(false);
                                                    cancelProgressDialog.show();
                                                    getSpiceManager().execute(new RejectOrderRequest(getActivity(),
                                                                    String.valueOf((new Date()).getTime()),
                                                                    ((MainActivity) getActivity()).getOrder(),
                                                                    profile.getTenantId()),
                                                            new RejectOrderListener());
                                                }
                                            });
                                    alertDialog.setNegativeButton(getString(R.string.fragments_MainFragment_no),
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });
                                    alertDialog.show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }));
                    menu.updateMenu(menuItems, typedValue.data, typedValueText.data);

                    if ((city != null && city.getPhone().length() > 0) || driverPhone.length() > 1) {
                        menu.menuItemEnable(0, R.drawable.phone, true, typedValueText.data);
                    } else {
                        menu.menuItemEnable(0, R.drawable.phone_disabled, false, typedValueText.data);
                    }
                    ((MainActivity) getActivity()).setTitleText(getString(R.string.fragments_MainFragment_status_new));
                    startOrderInfo();
                } else {
                    ((MainActivity) getActivity()).getOrder().setStatus("pre_order");
                    ((MainActivity) getActivity()).getOrder().setOrderId(createOrderEvent.getOrderId());
                    ((MainActivity) getActivity()).getOrder().setOrderNumber(createOrderEvent.getOrderNumber());
                    ((MainActivity) getActivity()).getOrder().save();
                    RoutePoint.saveOrderAddressesAndRoute(((MainActivity) getActivity()).getOrder(), ((MainActivity) getActivity()).getAddressList());
                    List<Option> optionList = Option.getAllOptions();
                    for (Option option : optionList) {
                        option.setChecked(false);
                        option.save();
                    }

                    startActivity(new Intent(getActivity(), PreOrderActivity.class).putExtra("order_id", createOrderEvent.getOrderId()));
                }
                break;
            case "NEED_REAUTH":
                Intent startActivityIntent = new Intent(getActivity(), AuthActivity.class);
                startActivityIntent
                        .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_UP);
                startActivity(startActivityIntent);
                break;
            case "BAD_PAY_TYPE":
                new ToastWrapper(getContext(),
                        R.string.fragments_MainFragment_status_bad_pay).show();
                break;
            case "NO_MONEY":
                new ToastWrapper(getContext(),
                        R.string.fragments_MainFragment_status_no_money).show();
                break;
            default:
                new ToastWrapper(getContext(), createOrderEvent.getStatusLabel()).show();
        }
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {
        //getActivity().setTitle(orderInfoEvent.getStatusLabel());

        ((MainActivity) getActivity()).setTitleText(orderInfoEvent.getStatusLabel());

        try {
            driverPhone = orderInfoEvent.getDriverPhone();
            if (driverPhone.length() > 1) {
                menu.menuItemEnable(0, R.drawable.phone, true, typedValueText.data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        statusOrder = orderInfoEvent.getStatus();

        int menuCancelItemIndex = AppParams.USE_CALLS ? 1 : 0;
        if (statusOrder.equals("new") || statusOrder.equals("car_assigned")) {
            menu.menuItemEnable(
                    menuCancelItemIndex, R.drawable.cencel, true, typedValueText.data);
        } else {
            menu.menuItemEnable(
                    menuCancelItemIndex, R.drawable.cencel_disabled, false, typedValueText.data);
        }

        ((MainActivity) getActivity()).getOrder().setStatusLabel(orderInfoEvent.getStatusLabel());
        ((MainActivity) getActivity()).getOrder().setStatus(orderInfoEvent.getStatus());
        ((MainActivity) getActivity()).getOrder().setCar(orderInfoEvent.getCarDesc());
        ((MainActivity) getActivity()).getOrder().setDriver(orderInfoEvent.getDriverName());
        ((MainActivity) getActivity()).getOrder().setPhoto(orderInfoEvent.getPhoto());
        ((MainActivity) getActivity()).getOrder().setCost(orderInfoEvent.getCost());
        ((MainActivity) getActivity()).getOrder().setStatusId(orderInfoEvent.getStatusId());
        ((MainActivity) getActivity()).getOrder().save();


        switch (orderInfoEvent.getStatus()) {
            case "new":

                break;
            case "pre_order":

                break;
            case "completed":
                cancelProgressDialog.cancel();
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);

                startActivity(new Intent(getActivity(), OrderCompletedActivity.class).putExtra("order_id", ((MainActivity) getActivity()).getOrder().getOrderId()));
                getActivity().finish();
                break;
            case "rejected":
                AnalyticsHelper.sendEvent(AnalyticsHelper.METRICA_EVENT_REJECT_ORDER);
                cancelProgressDialog.cancel();
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);
                ((MainActivity) getActivity()).getOrder().setStatusLabel(getString(R.string.fragments_MainFragment_status_label_rejected));
                ((MainActivity) getActivity()).getOrder().save();
                startActivity(new Intent(getActivity(), OrderDetailActivity.class).putExtra("order_id", ((MainActivity) getActivity()).getOrder().getOrderId()).putExtra("type", "main").putExtra("status", "rejected"));
                getActivity().finish();
                break;
            default:
                EventBus.getDefault().post(new PointsMapEvent(RoutePoint.getRoute(((MainActivity) getActivity()).getOrder()), ((MainActivity) getActivity()).activeOrder, orderInfoEvent.getStatus(), orderInfoEvent.getTime()));
                EventBus.getDefault().post(new CarBusyEvent(new Car(orderInfoEvent.getLat(), orderInfoEvent.getLon(), orderInfoEvent.getDegree())));
        }
    }

    @Subscribe
    public void onMessage(RestoreFooterEvent restoreFooterEvent) {
        showContent();
    }

    private void startOrderInfo() {
        if (getInfoHandler != null && getInfoRunnable != null) {
            getInfoHandler.removeCallbacks(getInfoRunnable);
        }
        getInfoHandler = new Handler();
        getInfoRunnable = new Runnable() {
            @Override
            public void run() {

                getSpiceManager().execute(new GetOrderInfoRequest(getActivity(),
                                String.valueOf((new Date()).getTime()),
                                ((MainActivity) getActivity()).getOrder(),
                                profile.getTenantId()),
                        new OrderInfoListener());

                getInfoHandler.postDelayed(getInfoRunnable, 5000);

            }
        };
        getInfoHandler.postDelayed(getInfoRunnable, 5000);
    }


    @Override
    public void onDestroy() {
        if (addItemFab != null) addItemFab.setOnClickListener(null);
        if (getInfoHandler != null)
            getInfoHandler.removeCallbacks(getInfoRunnable);
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        isActive = true;
        if (profile.getCityId().length() > 0 &&
                ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0 &&
                !statusOrder.equals("car_assigned")) {
            getSpiceManager().execute(new GetCarsRequest(getActivity(), profile.getCityId(),
                            ((MainActivity) getActivity()).getAddressList().get(0).getLat(),
                            ((MainActivity) getActivity()).getAddressList().get(0).getLon(),
                            profile.getTenantId()),
                    new CarsListener());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (((MainActivity) getActivity()).activeOrder)
            ibCurrentPosition.setVisibility(View.GONE);
        else
            ibCurrentPosition.setVisibility(View.VISIBLE);
    }

    @Override
    public void onStop() {
        isActive = false;
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onMessage(MotionUpEvent motionUpEvent) {
        upEvent = motionUpEvent.isUp();
        if (upEvent && cameraChangeEvent) {
            showContent();

        }
        EventBus.getDefault().post(new MotionUnblockMapEvent());
        bMotionEventHide = true;
    }

    @Subscribe
    public void onMessage(MotionCameraChangeEvent motionCameraChangeEvent) {
        cameraChangeEvent = motionCameraChangeEvent.isChange();
        if (upEvent && cameraChangeEvent)
            showContent();
    }

    @Subscribe
    public void onMessage(MotionDownEvent motionDownEvent) {
        if (!((MainActivity) getActivity()).activeOrder) {
            upEvent = false;
            cameraChangeEvent = false;

            if (bMotionEventHide) {
                hideContent();
            }
        }
    }

    @Subscribe
    public void onMessage(PolygonDetectEvent polygonDetectEvent) {
        if (polygonDetectEvent.getCity() == null && !((MainActivity) getActivity()).activeOrder) {
            hideContent();
        } else {
            if (!((MainActivity) getActivity()).activeOrder &&
                    profile.getCityId().length() > 0 &&
                    ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0 &&
                    !statusOrder.equals("car_assigned")) {
                getSpiceManager().execute(new GetCarsRequest(getActivity(), profile.getCityId(),
                                ((MainActivity) getActivity()).getAddressList().get(0).getLat(),
                                ((MainActivity) getActivity()).getAddressList().get(0).getLon(),
                                profile.getTenantId()),
                        new CarsListener());
            }
        }
    }

    private void hideContent() {
        llDown.animate().translationY(400);
    }

    private void showContent() {
        llDown.animate().translationY(0);
    }

    public void startCostRequest() {
        MainFooterFragment footerFragment = (MainFooterFragment)
                getChildFragmentManager().findFragmentByTag("mainFooter");
        if (footerFragment != null) footerFragment.startCostRequest();
    }

    public void showFab() {
        addItemFab.show();
    }

    public void hideFab() {
        addItemFab.hide();
    }

}
