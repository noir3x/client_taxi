package com.gootax.client.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.OrderInfoEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class TrackingOrderHeaderFragment extends Fragment {

    View view;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_trackingorder_header, container, false);

        view.findViewById(R.id.rl_driver).setVisibility(View.INVISIBLE);

        return view;
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {


        switch (orderInfoEvent.getStatus()) {
            case "new":

                break;
            default:
                view.findViewById(R.id.rl_driver).setVisibility(View.VISIBLE);
                ((TextView)getActivity().findViewById(R.id.tv_cardesc)).setText(orderInfoEvent.getCarDesc());
                ((TextView)getActivity().findViewById(R.id.tv_driver_name)).setText(orderInfoEvent.getDriverName());

                if (AppParams.USE_PHOTO) {
                    Uri uri = Uri.parse(orderInfoEvent.getPhoto());
                    ((SimpleDraweeView) getActivity()
                            .findViewById(R.id.sdv_driver_face)).setImageURI(uri);
                } else {
                    (getActivity().findViewById(R.id.sdv_driver_face)).setVisibility(View.GONE);
                }

                if (orderInfoEvent.getRaiting() > 0) {
                    view.findViewById(R.id.ll_trackingorder_raiting).setVisibility(View.VISIBLE);
                    ((TextView)getActivity().findViewById(R.id.tv_trackingorder_raiting)).setText(orderInfoEvent.getRaiting() + "");
                }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
