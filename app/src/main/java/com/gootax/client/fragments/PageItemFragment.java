package com.gootax.client.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.models.Tariff;

public class PageItemFragment extends Fragment {

    public static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";

    private int pageNumber;
    private Tariff tariff;

    public static PageItemFragment newInstance(int page, String tariffId) {
        PageItemFragment pageFragment = new PageItemFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        arguments.putString("tariff_id", tariffId);
        pageFragment.setArguments(arguments);

        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
        tariff = Tariff.getTariffById(getArguments().getString("tariff_id"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_page, container, false);

        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) view.findViewById(R.id.sdv_image_page);
        TextView tvTariffName = (TextView) view.findViewById(R.id.tv_label_tariff) ;
        TextView tvTariffDesc = (TextView) view.findViewById(R.id.tv_desc_tariff) ;
        Button btnTariffConfirm = (Button) view.findViewById(R.id.btn_confirm_tariff);

        simpleDraweeView.setImageURI(Uri.parse(tariff.getTariffIcon()));
        tvTariffName.setText(tariff.getTariffName());
        tvTariffDesc.setText(tariff.getDescription());

        btnTariffConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().setResult(getActivity().RESULT_OK, new Intent().putExtra("tariff_id", tariff.getTariffId()));
                getActivity().finish();
            }
        });


        /*
        switch (pageNumber) {
            case 0:
                simpleDraweeView.setImageURI(Uri.parse(tariff.getTariffIcon()));
                break;
            case 1:
                simpleDraweeView.setImageURI(Uri.parse(tariff.getTariffIcon()));
                break;
            case 2:
                simpleDraweeView.setImageURI(Uri.parse(tariff.getTariffIcon()));
                break;
            case 3:
                simpleDraweeView.setImageURI(Uri.parse(tariff.getTariffIcon()));
                break;
            default:
                break;
        }
        */

        return view;
    }

}
