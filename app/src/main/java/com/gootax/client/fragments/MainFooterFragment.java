package com.gootax.client.fragments;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.activities.AuthActivity;
import com.gootax.client.activities.MainActivity;
import com.gootax.client.activities.PaymentActivity;
import com.gootax.client.activities.TariffActivity;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.CallCostEvent;
import com.gootax.client.events.CreateOrderEvent;
import com.gootax.client.events.PolygonDetectEvent;
import com.gootax.client.events.SaveTariffsEvent;
import com.gootax.client.events.geo_events.ChangeTariff;
import com.gootax.client.models.City;
import com.gootax.client.models.Profile;
import com.gootax.client.models.Tariff;
import com.gootax.client.network.listeners.CostListener;
import com.gootax.client.network.listeners.CreateOrderListener;
import com.gootax.client.network.listeners.TariffsRequestListener;
import com.gootax.client.network.requests.CreateOrderRequest;
import com.gootax.client.network.requests.GetCostRequest;
import com.gootax.client.network.requests.GetTariffsListRequest;
import com.gootax.client.utils.AppPreferences;
import com.gootax.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;
import java.util.List;

public class MainFooterFragment extends BaseSpiceFragment {

    private String cityId = "";
    private LinearLayout linearLayout;
    private TextView tvCallCost;
    private Button btnCreateOrder;
    private ProgressBar pbCreateOrder;
    private Profile profile;

    private LinearLayout paymentLayout;
    private ImageView paymentImg;
    private TextView paymentTitle;
    private String currency;
    private TypedValue typedValue;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_footer, container, false);

        typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(R.attr.accent_bg_tariff, typedValue, true);

        btnCreateOrder = (Button) view.findViewById(R.id.btn_createorder);

        pbCreateOrder = (ProgressBar) view.findViewById(R.id.pb_createorder);
        linearLayout = (LinearLayout) view.findViewById(R.id.ll_tariffs);
        tvCallCost = (TextView) view.findViewById(R.id.tv_fragmentmain_callcost);
        setInitialCostText();

        paymentLayout = (LinearLayout) view.findViewById(R.id.payment_layout);
        paymentImg = (ImageView) view.findViewById(R.id.payment_img);
        paymentTitle = (TextView) view.findViewById(R.id.payment_title);
        initPaymentViews();

        btnCreateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean tariff = false;
                boolean auth = false;
                boolean isAddrCountEnough = true;
                boolean address = false;
                boolean solvency = false;
                if (((MainActivity) getActivity()).getOrder().getTariff() != null &&
                        ((MainActivity) getActivity()).getOrder().getTariff().length() != 0)
                    tariff = true;
                else
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_empty_tariff).show();
                if (Profile.getProfile().isAuthorized()) {
                    auth = true;
                } else {
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_empty_profile).show();

                    Intent startActivityIntent = new Intent(getActivity(), AuthActivity.class);
                    startActivityIntent
                            .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_UP);
                    startActivity(startActivityIntent);
                    return;
                }
                if (!AppParams.ORDER_WITH_ONE_ADDRESS) {
                    int addrCount = ((MainActivity) getActivity()).getAddressList().size();
                    if (addrCount < 2) {
                        isAddrCountEnough = false;
                        new ToastWrapper(getActivity(),
                                R.string.fragments_MainFooterFragment_not_enough_addresses).show();
                    }
                }

                if (((MainActivity) getActivity()).getAddressList().size() > 0 &&
                        ((MainActivity) getActivity()).getAddressList().get(0).getLat().length() > 0)
                    address = true;

                if (profile.getPaymentType().equals("CARD") && profile.getPan().equals("")){
                    solvency = false;
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_no_card_error).show();
                    Intent startActivityIntent = new Intent(getActivity(), PaymentActivity.class);
                    startActivity(startActivityIntent);
                } else {
                    solvency = true;
                }

                /*if (profile.getPaymentType().equals("CARD") && profile.getPan().equals("") &&
                        Profile.getProfile().isAuthorized()){
                    pay = false;
                    new ToastWrapper(getActivity(),
                            R.string.fragments_MainFooterFragment_no_card_error).show();
                    Intent startActivityIntent = new Intent(getActivity(), PaymentActivity.class);
                    startActivity(startActivityIntent);
                } else {
                    pay = true;
                }*/

                if (tariff && auth && isAddrCountEnough && address && solvency) {
                    Profile profile = Profile.getProfile();
                    ((MainActivity) getActivity()).getOrder().setCreateTime(new Date().getTime());
                    getSpiceManager().execute(new CreateOrderRequest(getActivity(),
                            String.valueOf(new Date().getTime()),
                            ((MainActivity) getActivity()).getOrder(), profile,
                            ((MainActivity) getActivity()).getAddressList()),
                            new CreateOrderListener());
                    btnCreateOrder.setVisibility(View.GONE);
                    pbCreateOrder.setVisibility(View.VISIBLE);
                }
            }
        });

        EventBus.getDefault().register(this);

        return view;
    }

    @Subscribe
    public void onMessage(CreateOrderEvent createOrderEvent) {
        btnCreateOrder.setVisibility(View.VISIBLE);
        pbCreateOrder.setVisibility(View.GONE);
    }

    public void initPaymentViews() {
        profile = Profile.getProfile();
        String paymentType = profile.getPaymentType();
        switch (paymentType) {
            case "CASH":
                paymentImg.setImageResource(R.drawable.wallet);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_cash);
                break;
            case "PERSONAL_ACCOUNT":
                paymentImg.setImageResource(R.drawable.parsonal);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_account);
                break;
            case "CORP_BALANCE":
                paymentImg.setImageResource(R.drawable.man);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_corp);
                break;
            case "CARD":
                paymentImg.setImageResource(R.drawable.visa_logo);
                paymentTitle.setText(R.string.fragments_MainFooterFragment_type_card);
                break;
        }
        paymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent paymentIntent = new Intent(getActivity(), PaymentActivity.class);
                startActivity(paymentIntent);
            }
        });
    }

    @Subscribe
    public void onMessage(PolygonDetectEvent polygonDetectEvent) {
        if (polygonDetectEvent.getCity() != null) {

            AppPreferences appPreferences = new AppPreferences(getActivity());
            List<Tariff> tariffList = Tariff.getTariffs(City.getCity(polygonDetectEvent.getCity().getCityId()));

            boolean bTime = false;
            boolean bCityId = false;
            boolean bBdTariff = false;

            if (appPreferences.getText("save_tariff").length() > 0 &&
                    ((Long.valueOf(appPreferences.getText("save_tariff").toString()) +
                            AppParams.TIME_UPDATE_TARIFFS) > new Date().getTime()))
                bTime = true;

            if (tariffList.size() > 0)
                bBdTariff = true;

            if (cityId.equals(polygonDetectEvent.getCity().getCityId()))
                bCityId = true;
            else
                cityId = polygonDetectEvent.getCity().getCityId();

            if (bTime && bBdTariff && bCityId) {
                if (linearLayout.getChildCount() == 0)
                    setTariffs(tariffList);

            } else {
                getSpiceManager().execute(new GetTariffsListRequest(getActivity(),
                                String.valueOf((new Date()).getTime()),
                                polygonDetectEvent.getCity().getCityId(), Profile.getProfile().getTenantId()),
                        new TariffsRequestListener());
            }
        } else {
            cityId = "";
            linearLayout.removeAllViews();
        }
    }

    @Subscribe
    public void onMessage(SaveTariffsEvent saveTariffsEvent) {
        setTariffs(saveTariffsEvent.getTariffList());
    }

    private void setTariffs(List<Tariff> tariffList) {
        linearLayout.removeAllViews();
        for (final Tariff tariff : tariffList) {
            final View viewTariff = LayoutInflater.from(getActivity()).inflate(R.layout.item_footertariff, null);
            if (!tariff.getTariffIcon().isEmpty()) {
                SimpleDraweeView tariffImage = (SimpleDraweeView)
                        viewTariff.findViewById(R.id.iv_tariff_image);
                Uri imgUri = Uri.parse(tariff.getTariffIconMini());
                tariffImage.setImageURI(imgUri);
            }
            ((TextView) viewTariff.findViewById(R.id.tv_tariff_name)).setText(tariff.getTariffName());
            viewTariff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int count = linearLayout.getChildCount();
                    try {
                        if ((int) view.findViewById(R.id.ll_tariff_cont).getTag(R.id.tag_tariff) == 1) {
                            getActivity().startActivityForResult(new Intent(getActivity(),
                                            TariffActivity.class).putExtra("tariff_id",
                                    ((MainActivity) getActivity()).getOrder().getTariff()),
                                    MainActivity.REQUEST_CODE_TARIFF);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < count; i++) {
                        linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundColor(Color.TRANSPARENT);
                        linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 0);
                    }

                    view.findViewById(R.id.ll_tariff_cont).setBackgroundResource(0);
                    view.findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);

                    view.findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 1);
                    ((MainActivity) getActivity()).getOrder().setTariff(tariff.getTariffId());
                    startCostRequest();
                }
            });
            viewTariff.setTag(R.id.tag_id, tariff.getTariffId());
            linearLayout.addView(viewTariff);
        }
        if (linearLayout.getChildCount() > 0) {
            linearLayout.getChildAt(0).findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);
            linearLayout.getChildAt(0).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 1);
            ((MainActivity) getActivity()).getOrder().setTariff(tariffList.get(0).getTariffId());
        }
    }


    @Subscribe
    public void onMessage(ChangeTariff changeTariff) {
        ((MainActivity) getActivity()).getOrder().setTariff(changeTariff.getTariffId());
        int count = linearLayout.getChildCount();
        for (int i = 0; i < count; i++) {

            if (linearLayout.getChildAt(i).getTag(R.id.tag_id).toString().equals(changeTariff.getTariffId())) {
                //linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundResource(R.color.layout_back_primary);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundColor(typedValue.data);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 1);
            } else {
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setBackgroundColor(Color.TRANSPARENT);
                linearLayout.getChildAt(i).findViewById(R.id.ll_tariff_cont).setTag(R.id.tag_tariff, 0);
            }
        }
    }

    @Subscribe
    public void onMessage(CallCostEvent callCostEvent) {
        try {
            City city = City.getCity(Profile.getProfile().getCityId());

            if (city.getCurrency().equals("RUB"))
                currency = getString(R.string.currency);
            else
                currency = city.getCurrency();
        } catch (Exception e) {
            currency = getString(R.string.currency);
        }
        if (callCostEvent.getCost().length() > 0) {
            tvCallCost.setText("~ " + callCostEvent.getCost() + " " + currency + ", "
                    + callCostEvent.getDis() + " " + getString(R.string.activities_OrderDetailActivity_distance) + ", "
                    + callCostEvent.getTime() + " " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long));
        } else {
            tvCallCost.setText(R.string.fragments_MainFooterFragment_infocost_error);
        }
    }

    private void setInitialCostText() {
        if (((MainActivity) getActivity()).getAddressList().size() < 2) {
            tvCallCost.setText(R.string.fragments_MainFooterFragment_infocost_second_point);
        } else {
            tvCallCost.setText(R.string.fragments_MainFooterFragment_infocost_default);
        }
    }

    public void startCostRequest() {
        if (((MainActivity) getActivity())
                .getAddressList().size() > 1) {
            tvCallCost.setText(R.string.fragments_MainFooterFragment_infocost_default);
            Profile profile = Profile.getProfile();
            getSpiceManager().execute(new GetCostRequest(getActivity(),
                    String.valueOf(new Date().getTime()),
                    ((MainActivity) getActivity()).getOrder(), profile,
                    ((MainActivity) getActivity()).getAddressList()), new CostListener());
        } else {
            tvCallCost.setText(R.string.fragments_MainFooterFragment_infocost_second_point);
        }
    }

    @Override
    public void onDestroyView() {
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }
}
