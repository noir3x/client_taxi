package com.gootax.client.views.menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gootax.client.R;

import java.util.List;

public class Menu {

    private Context context;
    private LinearLayout llContainer;
    private LayoutInflater lp;
    LinearLayout.LayoutParams params;

    public Menu(List<MenuItem> menuItems, LinearLayout linearLayout, Context context, int color, int textColor) {
        this.context = context;
        this.llContainer = linearLayout;
        lp = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        params = new LinearLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT, 1.0f);
        for (MenuItem menuItem : menuItems) {
            RelativeLayout viewMenuItem = (RelativeLayout)lp.inflate(R.layout.item_footermenu, null);
            if (color != 0)
                viewMenuItem.setBackgroundColor(color);
            viewMenuItem.setLayoutParams(params);
            ((ImageView) viewMenuItem.findViewById(R.id.iv_iconmenu)).setImageResource(menuItem.getIcon());
            ((TextView) viewMenuItem.findViewById(R.id.tv_labelmenu)).setText(menuItem.getLabel());
            if (textColor != 0)
                ((TextView) viewMenuItem.findViewById(R.id.tv_labelmenu)).setTextColor(textColor);
            viewMenuItem.setOnClickListener(menuItem.getOnClickListener());
            this.llContainer.addView(viewMenuItem);
        }
    }

    public void updateMenu(List<MenuItem> menuItems, int color, int textColor) {
        llContainer.removeAllViews();
        for (MenuItem menuItem : menuItems) {
            RelativeLayout viewMenuItem = (RelativeLayout)lp.inflate(R.layout.item_footermenu, null);
            viewMenuItem.setBackgroundColor(color);
            viewMenuItem.setLayoutParams(params);
            ((ImageView) viewMenuItem.findViewById(R.id.iv_iconmenu)).setImageResource(menuItem.getIcon());
            ((TextView) viewMenuItem.findViewById(R.id.tv_labelmenu)).setText(menuItem.getLabel());
            ((TextView) viewMenuItem.findViewById(R.id.tv_labelmenu)).setTextColor(textColor);
            viewMenuItem.setOnClickListener(menuItem.getOnClickListener());
            this.llContainer.addView(viewMenuItem);
        }
    }

    public void updateMenuItem(MenuItem menuItem, int position) {
        llContainer.removeViewAt(position);
        RelativeLayout viewMenuItem = (RelativeLayout)lp.inflate(R.layout.item_footermenu, null);
        viewMenuItem.setLayoutParams(params);
        ((ImageView) viewMenuItem.findViewById(R.id.iv_iconmenu)).setImageResource(menuItem.getIcon());
        ((TextView) viewMenuItem.findViewById(R.id.tv_labelmenu)).setText(menuItem.getLabel());
        viewMenuItem.setOnClickListener(menuItem.getOnClickListener());
        llContainer.addView(viewMenuItem, position);
    }

    public void menuItemEnable(int item, int drawable, boolean enable, int textColor) {
        View view = llContainer.getChildAt(item);
        ((ImageView) view.findViewById(R.id.iv_iconmenu)).setImageResource(drawable);
        if (enable)
            ((TextView) view.findViewById(R.id.tv_labelmenu)).setTextColor(textColor);
        else
            ((TextView) view.findViewById(R.id.tv_labelmenu)).setTextColor(context.getResources().getColor(R.color.textColorGrey));
    }

}
