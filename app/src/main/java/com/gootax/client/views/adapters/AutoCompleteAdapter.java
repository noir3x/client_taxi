package com.gootax.client.views.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.models.Address;

import java.util.List;

public class AutoCompleteAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private List<Address> addresses;

    public AutoCompleteAdapter(Context context, List<Address> addresses) {
        this.inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addresses = addresses;
    }

    static class CompleteViewHolder {
        public TextView itemStreet;
        public TextView itemCity;
    }

    @Override
    public int getCount() {
        return addresses.size();
    }

    @Override
    public Object getItem(int position) {
        return addresses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_autocomplete, parent, false);
            CompleteViewHolder completeViewHolder = new CompleteViewHolder();
            completeViewHolder.itemStreet = (TextView) rowView
                    .findViewById(R.id.item_autocomplete_street);
            completeViewHolder.itemCity = (TextView) rowView
                    .findViewById(R.id.item_autocomplete_city);
            rowView.setTag(completeViewHolder);
        }
        // fill data
        CompleteViewHolder holder = (CompleteViewHolder) rowView.getTag();
        holder.itemStreet.setText(addresses.get(position).getLabel());
        holder.itemCity.setText(addresses.get(position).getCity());
        return rowView;
    }

}
