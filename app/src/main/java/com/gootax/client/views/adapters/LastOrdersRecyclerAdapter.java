package com.gootax.client.views.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.activities.OrderDetailActivity;
import com.gootax.client.activities.PreOrderActivity;
import com.gootax.client.app.AppParams;
import com.gootax.client.models.Address;
import com.gootax.client.models.City;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.models.RoutePoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class LastOrdersRecyclerAdapter extends
        RecyclerView.Adapter<LastOrdersRecyclerAdapter.LastOrdersViewHolder>
        implements View.OnClickListener {

    private List<Order> orderList;
    private List<Address> addressList;
    private Context context;
    private String currency;
    private String type;

    public LastOrdersRecyclerAdapter(Context context, List<Order> orderList, String type) {
        this.orderList = orderList;
        this.context = context;
        this.type = type;

        City city = City.getCity(Profile.getProfile().getCityId());
        if (city.getCurrency().equals("RUB"))
            currency = this.context.getString(R.string.currency);
        else
            currency = city.getCurrency();

        addressList = new ArrayList<>();
        Iterator<Order> orderIterator = orderList.iterator();
        while (orderIterator.hasNext()) {
            Order order = orderIterator.next();
            Address address = RoutePoint.getFirstAddress(order);
            if (address != null) {
                addressList.add(address);
            } else {
                orderIterator.remove();
            }
        }
    }

    @Override
    public LastOrdersViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;
        if (type.equals("pre"))
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_preorders, null);
        else
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_lastorders, null);
        LastOrdersViewHolder viewHolder = new LastOrdersViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(LastOrdersViewHolder customViewHolder, int i) {

        if (type.equals("pre")) {
            customViewHolder.tvCost.setText(context.getString(R.string.activities_LastOrdersActivity_pre));
            customViewHolder.tvStatus.setText(formatTime(orderList.get(i).getCreateTime(), orderList.get(i).getOrderTime()));
        } else {
            if (orderList.get(i).getCost() != null && orderList.get(i).getCost().length() > 0) {
                String cost = "0";
                try {
                    JSONObject jsonCost = new JSONObject(orderList.get(i).getCost());
                    cost = jsonCost.getString("summary_cost");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                customViewHolder.tvCost.setText(cost + " " + currency);
            }

            if (orderList.get(i).getStatus().equals("completed"))
                customViewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.textColorGreen));
            else
                customViewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.textColorRed));

            customViewHolder.tvStatus.setText(orderList.get(i).getStatusLabel());
        }


        if (orderList.get(i).getCar().length() > 0)
            customViewHolder.tvCar.setText(orderList.get(i).getCar());
        else
            customViewHolder.tvCar.setText(context.getString(R.string.activities_OrderDetailActivity_empty_driver));

        customViewHolder.tvDate.setText(formatDate(orderList.get(i).getCreateTime()));

        Uri uriFace = Uri.parse(orderList.get(i).getPhoto());
        customViewHolder.draweeViewFace.setImageURI(uriFace);

        Address address = addressList.get(i);
        String sCoords = address.getLat() + "," + address.getLon();
        Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap?center=" + sCoords
                + "&zoom=13&size=600x300&maptype=roadmap&markers=color:black%7Clabel:A%7C" + sCoords
                + "&key=" + AppParams.GOOGLE_API_KEY);
        customViewHolder.draweeView.setImageURI(uri);

        customViewHolder.customView.setTag(R.id.tag_order_id, orderList.get(i).getOrderId());
        customViewHolder.customView.setTag(R.id.tag_order_status, orderList.get(i).getStatus());
        customViewHolder.customView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    @Override
    public void onClick(View view) {
        if (type.equals("pre"))
            context.startActivity(new Intent(context, PreOrderActivity.class).putExtra("order_id", view.getTag(R.id.tag_order_id).toString()));
        else
            context.startActivity(new Intent(context, OrderDetailActivity.class).putExtra("order_id", view.getTag(R.id.tag_order_id).toString()).putExtra("type", "last").putExtra("status", view.getTag(R.id.tag_order_status).toString()));
    }

    public class LastOrdersViewHolder extends RecyclerView.ViewHolder {

        private View customView;
        private TextView tvCost;
        private TextView tvStatus;
        private TextView tvDate;
        private TextView tvCar;
        private SimpleDraweeView draweeView;
        private SimpleDraweeView draweeViewFace;

        public LastOrdersViewHolder(View view) {
            super(view);
            this.tvCost = (TextView) view.findViewById(R.id.tv_lastorders_cost);
            this.tvStatus = (TextView) view.findViewById(R.id.tv_lastorders_status);
            this.tvDate = (TextView) view.findViewById(R.id.tv_lastorders_date);
            this.tvCar = (TextView) view.findViewById(R.id.tv_lastorders_car);
            this.draweeView = (SimpleDraweeView) view.findViewById(R.id.sdv_map);
            this.draweeViewFace = (SimpleDraweeView) view.findViewById(R.id.sdv_face);
            if (!AppParams.USE_PHOTO) draweeViewFace.setVisibility(View.GONE);
            this.customView = view;
        }

    }

    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

    private String formatTime(long longDateCreate, String strDatePlant) {
        Date datePlant = null;

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        String str = "%d " + context.getString(R.string.activities_PreOrderActivity_date_day_short) +
                ". %d " + context.getString(R.string.activities_PreOrderActivity_date_hour_short) +
                ". %d " + context.getString(R.string.fragments_FragmentAbstractMap_time_minute_short) + ".";

        try {
            datePlant = formatter.parse(strDatePlant);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (datePlant != null) {
            long mSec = datePlant.getTime() - longDateCreate;
            if (mSec > 0) {
                long days = TimeUnit.MILLISECONDS.toDays(mSec);
                long hours = TimeUnit.MILLISECONDS.toHours(mSec) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(mSec));
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mSec) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mSec));
                return String.format(str, days, hours, minutes);
            } else {
                return String.format(str, 0, 0, 0);
            }
        } else {
            return String.format(str, 0, 0, 0);
        }
    }
}