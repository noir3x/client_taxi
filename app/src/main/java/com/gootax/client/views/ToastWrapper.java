package com.gootax.client.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.widget.Toast;

public class ToastWrapper {

    private Toast toast;

    @SuppressLint("ShowToast")
    public ToastWrapper(Context context, @StringRes int text) {
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
    }

    @SuppressLint("ShowToast")
    public ToastWrapper(Context context, CharSequence text) {
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
    }

    public void show() {
        toast.show();
    }

}
