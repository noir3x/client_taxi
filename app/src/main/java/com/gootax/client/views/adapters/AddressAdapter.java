package com.gootax.client.views.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.events.DeleteItemListEvent;
import com.gootax.client.models.Address;
import com.gootax.client.utils.AddressList;

import java.util.ArrayList;
import java.util.List;

import org.greenrobot.eventbus.EventBus;

public class AddressAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private List<Address> addressList;
    private List<Integer> icons;
    private boolean type;

    public AddressAdapter(Context context, List<Address> list) {
        this.context = context;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.addressList = list;
        this.type = true;
        initIcons();
    }

    private void initIcons() {
        icons = new ArrayList<>();
        icons.add(R.drawable.a);
        icons.add(R.drawable.b);
        icons.add(R.drawable.c);
        icons.add(R.drawable.d);
        icons.add(R.drawable.e);
    }

    static class AddressViewHolder {
        public ImageView itemIcon;
        public TextView itemStreet;
        public TextView itemCity;
        public ImageButton itemRemove;
    }

    @Override
    public int getCount() {
        return ((AddressList) addressList).realSize();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            rowView = inflater.inflate(R.layout.item_header, parent, false);
            AddressViewHolder viewHolder = new AddressViewHolder();
            viewHolder.itemIcon = (ImageView) rowView.findViewById(R.id.iv_item_icon);
            viewHolder.itemStreet = (TextView) rowView.findViewById(R.id.tv_item_street);
            viewHolder.itemCity = (TextView) rowView.findViewById(R.id.tv_item_city);
            viewHolder.itemRemove = (ImageButton) rowView.findViewById(R.id.btn_item_remove);
            rowView.setTag(viewHolder);
        }
        // fill data
        AddressViewHolder holder = (AddressViewHolder) rowView.getTag();
        customizeViews(position, holder);
        return rowView;
    }

    private void customizeViews(final int position, AddressViewHolder holder) {
        holder.itemIcon.setImageResource(icons.get(position));
        Address currentAddress = addressList.get(position);
        setStreetText(currentAddress, holder.itemStreet);
        setStreetColor(currentAddress, holder.itemStreet);
        setCity(currentAddress, holder.itemCity);
        setRemoveButton(position, currentAddress, holder.itemRemove);
    }

    private void setStreetText(Address currentAddress, TextView itemStreet) {
        if (!currentAddress.isEmpty()) {
            itemStreet.setText(currentAddress.getLabel());
        } else {
            itemStreet.setText(R.string.fragments_MainHeaderFragment_empty_address);
        }
    }

    private void setStreetColor(Address currentAddress, TextView itemStreet) {
        if (type) {
            if (!currentAddress.isEmpty()) {
                itemStreet.setTextColor(ContextCompat.getColor(context, R.color.textColorBlack));
            } else {
                itemStreet.setTextColor(ContextCompat.getColor(context, R.color.textColorGrey));
            }
        } else {
            itemStreet.setTextColor(ContextCompat.getColor(context, R.color.textColorRed));
        }
    }

    private void setCity(Address currentAddress, TextView itemCity) {
        String city = currentAddress.getCity();
        if (currentAddress.isEmpty() || city == null || city.isEmpty()) {
            itemCity.setVisibility(View.GONE);
        } else {
            itemCity.setText(city);
            itemCity.setVisibility(View.VISIBLE);
        }
    }

    private void setRemoveButton(final int position,
                                 Address currentAddress, ImageButton itemRemove) {
        itemRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new DeleteItemListEvent(position));
            }
        });
        boolean isFakeSecondPoint =
                (position == AddressList.SECOND_POINT) && currentAddress.isEmpty();
        if (position == 0 || isFakeSecondPoint) {
            itemRemove.setVisibility(View.GONE);
        } else {
            itemRemove.setVisibility(View.VISIBLE);
        }
    }

    public void updateList(List<Address> list, boolean type) {
        this.addressList = list;
        this.type = type;
        notifyDataSetChanged();
    }

}
