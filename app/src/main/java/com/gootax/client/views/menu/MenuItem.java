package com.gootax.client.views.menu;


import android.view.View;

public class MenuItem {

    private int icon;
    private String label;
    private View.OnClickListener onClickListener;

    public MenuItem(int icon, String label, View.OnClickListener onClickListener) {
        this.icon = icon;
        this.label = label;
        this.onClickListener = onClickListener;
    }

    public int getIcon() {
        return icon;
    }

    public String getLabel() {
        return label;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

}
