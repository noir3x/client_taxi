package com.gootax.client.views;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.gootax.client.events.OrderTimeEvent;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Listener for SlideDateTimePicker
 */
public class DateTimeListener extends SlideDateTimeListener {

    @Override
    public void onDateTimeSet(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd.MM.yyyy HH:mm:ss", Locale.getDefault());
        String dateTime = dateFormat.format(date);
        EventBus.getDefault().post(new OrderTimeEvent(dateTime));
    }

    @Override
    public void onDateTimeCancel()
    {
        EventBus.getDefault().post(new OrderTimeEvent(""));
    }



}
