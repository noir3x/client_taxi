package com.gootax.client.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.gootax.client.events.CurrentLocationEvent;
import com.gootax.client.events.GetCurrentLocationEvent;
import com.gootax.client.utils.LocManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LocationService extends Service {

    private LocManager manager;
    private Location location;

    @Override
    public void onCreate() {
        super.onCreate();
        EventBus.getDefault().register(this);
        manager = new LocManager(getApplicationContext());
        manager.startUpdatesRequesting();
        location = manager.getLastLocation();
        if (location != null) {
            EventBus.getDefault().post(new CurrentLocationEvent(
                    location.getLatitude(), location.getLongitude(), true));
        }
    }

    @Override
    public void onDestroy() {
        manager.stopUpdatesRequesting();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onMessage(GetCurrentLocationEvent getCurrentLocationEvent) {
        location = manager.getLocation();
        if (location != null) {
            EventBus.getDefault().post(new CurrentLocationEvent(
                    location.getLatitude(), location.getLongitude(), true));
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
