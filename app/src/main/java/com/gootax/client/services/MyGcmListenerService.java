package com.gootax.client.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;
import com.gootax.client.R;
import com.gootax.client.activities.MainActivity;
import com.gootax.client.activities.PreOrderActivity;
import com.gootax.client.app.AppParams;

import org.json.JSONException;
import org.json.JSONObject;


public class MyGcmListenerService extends GcmListenerService {

    private boolean isSendingAllowed = true;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        if (from != null && from.equals(AppParams.PUSH_SENDER_ID)) {
            Intent sendData = new Intent(AppParams.PUSH_ORDERINFO);
            JSONObject contentData = null;
            try {
                contentData = new JSONObject(data.getString("command_params"));
            } catch (JSONException | NullPointerException e) {
                e.printStackTrace();
            }
            if (contentData != null) {
                // Let's start govnocode:
                try {
                    sendData.putExtra("order_id", contentData.getString("order_id"));
                } catch (Exception e) {
                    isSendingAllowed = false;
                }
                try {
                    sendData.putExtra("status", contentData.getString("status"));
                } catch (Exception e) {
                    isSendingAllowed = false;
                }
                try {
                    sendData.putExtra("status_label", contentData.getString("status_label"));
                } catch (Exception e) {
                    isSendingAllowed = false;
                }
                try {
                    sendData.putExtra("driver", contentData.getString("driver"));
                } catch (Exception e) {
                    sendData.putExtra("driver", "");
                }
                try {
                    sendData.putExtra("tel", contentData.getString("tel"));
                } catch (Exception e) {
                    sendData.putExtra("tel", "");
                }
                try {
                    sendData.putExtra("car_time", contentData.getString("car_time"));
                } catch (Exception e) {
                    sendData.putExtra("car_time", "");
                }
                try {
                    sendData.putExtra("car", contentData.getString("car"));
                } catch (Exception e) {
                    sendData.putExtra("car", "");
                }
                try {
                    sendData.putExtra("car_lat", contentData.getString("car_lat"));
                } catch (Exception e) {
                    sendData.putExtra("car_lat", "");
                }
                try {
                    sendData.putExtra("car_lon", contentData.getString("car_lon"));
                } catch (Exception e) {
                    sendData.putExtra("car_lon", "");
                }
                try {
                    sendData.putExtra("degree", contentData.getString("degree"));
                } catch (Exception e) {
                    sendData.putExtra("degree", "");
                }
                if (isSendingAllowed) {
                    sendBroadcast(sendData);
                    String message = data.getString("message", "");
                    String contentText = !message.isEmpty() ?
                            message : sendData.getStringExtra("status_label");
                    sendNotification(getString(R.string.app_name),
                            contentText,
                            sendData.getStringExtra("order_id"),
                            sendData.getStringExtra("status"));
                }
            }
        }
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String title, String message, String id, String status) {
        Intent intent;
        int requestCode;
        if (status != null && !status.equals("pre_order")) {
            intent = new Intent(this, MainActivity.class);
            requestCode = 0;
        } else {
            intent = new Intent(this, PreOrderActivity.class);
            requestCode = 1;
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("order_id", id);
        intent.putExtra("status", status);
        intent.putExtra("type", "push");
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.push)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setLights(Color.RED, 1, 1)
                .setContentIntent(pendingIntent);
        if (status != null && (status.equals("car_assigned") || status.equals("car_at_place")))
            notificationBuilder.setSound(Uri.parse("android.resource://" +
                    getApplicationContext().getPackageName() + "/" + R.raw.sound));
        else
            notificationBuilder.setSound(Uri.parse("android.resource://" +
                    getApplicationContext().getPackageName() + "/" + R.raw.marimba_chord));
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(AppParams.NOTIFICATION_ID, notificationBuilder.build());
        PowerManager powerManager = (PowerManager)
                getApplication().getSystemService(Context.POWER_SERVICE);
        boolean isInteractive = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isInteractive = powerManager.isInteractive();
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            isInteractive = powerManager.isScreenOn();
        }
        if (!isInteractive) {
            PowerManager.WakeLock wl = powerManager.newWakeLock(
                    PowerManager.FULL_WAKE_LOCK
                            | PowerManager.ACQUIRE_CAUSES_WAKEUP
                            | PowerManager.ON_AFTER_RELEASE, "MyLock");
            wl.acquire(5000);
            PowerManager.WakeLock wl_cpu = powerManager
                    .newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyCpuLock");
            wl_cpu.acquire(5000);
        }
    }

}
