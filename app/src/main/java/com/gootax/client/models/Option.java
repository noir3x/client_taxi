package com.gootax.client.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "options")
public class Option extends Model {

    @Column(name = "optionIdPrymary")
    private String optionIdPrymary;
    @Column(name = "optionIdSecondary")
    private String optionIdSecondary;
    @Column(name = "price")
    private String price;
    @Column(name = "tariffType")
    private String tariffType;
    @Column(name = "optionName")
    private String optionName;
    @Column(name = "checked")
    private boolean checked;
    @Column(name = "tariffID")
    private long tariffID;

    public Option() {
    }

    public static List<Option> getOptions(Tariff tariff) {
        return new Select().from(Option.class).where("tariffID = ?", tariff.getId()).execute();
    }

    public static List<Option> getAllOptions() {
        return new Select().from(Option.class).execute();
    }

    public static void deleteAllOptions() {
        new Delete().from(Option.class).execute();
    }

    public static List<Option> getOptionsChecked(Tariff tariff) {
        return new Select().from(Option.class).where("tariffID = ?", tariff.getId()).where("checked = ?", true).execute();
    }

    public String getOptionIdPrymary() {
        return optionIdPrymary;
    }

    public void setOptionIdPrymary(String optionIdPrymary) {
        this.optionIdPrymary = optionIdPrymary;
    }

    public String getOptionIdSecondary() {
        return optionIdSecondary;
    }

    public void setOptionIdSecondary(String optionIdSecondary) {
        this.optionIdSecondary = optionIdSecondary;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTariffType() {
        return tariffType;
    }

    public void setTariffType(String tariffType) {
        this.tariffType = tariffType;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public long getTariffID() {
        return tariffID;
    }

    public void setTariffID(long tariffID) {
        this.tariffID = tariffID;
    }
}
