package com.gootax.client.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "cities")
public class City extends Model {

    @Column(name = "cityId")
    private String cityId;

    @Column(name = "cityName")
    private String cityName;

    @Column(name = "lat")
    private double lat;

    @Column(name = "lon")
    private double lon;

    @Column(name = "currency")
    private String currency;

    @Column(name = "phone")
    private String phone;

    @Column(name = "polygon")
    private String polygon;

    public City() {
        super();
    }

    public City(String cityId, String cityName, double lat, double lon) {
        super();
        this.cityId = cityId;
        this.cityName = cityName;
        this.lat = lat;
        this.lon = lon;
    }

    public City(String cityId, String cityName, double lat, double lon, String currency, String phone, String polygon) {
        super();
        this.cityId = cityId;
        this.cityName = cityName;
        this.lat = lat;
        this.lon = lon;
        this.currency = currency;
        this.phone = phone;
        this.polygon = polygon;
    }

    public static City getFirstCity() {
        return new Select().from(City.class).executeSingle();
    }

    public static List<City> getCities() {
        return new Select().from(City.class).execute();
    }

    public static City getCity(String cityId) {
        return new Select().from(City.class).where("cityId = ?", cityId).executeSingle();
    }

    public static void deleteAllCities() {
        new Delete().from(City.class).execute();
    }

    public List<Tariff> getTarifffs() {
        return getMany(Tariff.class, "cityID");
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPolygon() {
        return polygon;
    }

    public void setPolygon(String polygon) {
        this.polygon = polygon;
    }

}
