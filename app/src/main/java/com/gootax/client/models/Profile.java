package com.gootax.client.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.gootax.client.app.AppParams;

import java.util.List;

@Table(name = "profile")
public class Profile extends Model {


    @Column(name = "client_id")
    private String clientId = "";
    @Column(name = "name")
    private String name = "";
    @Column(name = "surname")
    private String surname = "";
    @Column(name = "patronymic")
    private String patronymic = "";
    @Column(name = "birth")
    private String birth = "";
    @Column(name = "photo")
    private String photo = "";
    @Column(name = "email")
    private String email = "";
    @Column(name = "phone")
    private String phone = "";
    @Column(name = "phone_mask")
    private String phoneMask = "";
    @Column(name = "country")
    private String country = "";
    @Column(name = "city_id")
    private String cityId = "";
    @Column(name = "balance_value")
    private String balanceValue = "";
    @Column(name = "balance_currency")
    private String balanceCurrency = "";
    @Column(name = "bonus_value")
    private String bonusValue = "";
    @Column(name = "payment_type")
    private String paymentType = "";
    @Column(name = "pan")
    private String pan = "";
    @Column(name = "company")
    private String company = "";
    @Column(name = "authorized")
    private boolean authorized = false;
    @Column(name = "colorID")
    private long colorID = 0;
    @Column(name = "theme")
    private int themeId;
    @Column(name = "tenant_id")
    private String tenantId = "";
    @Column(name = "map")
    private int map = 1;
    @Column(name = "payment_bonus")
    private int paymentBonus = 0;

    public Profile() {
        super();
    }

    public static Profile getProfile() {
        return new Select().from(Profile.class).executeSingle();
    }

    public String getClientId() {
        return clientId.equals("null") ? "" : clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getName() {
        return name.equals("null") ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname.equals("null") ? "" : surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic.equals("null") ? "" : patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getBirth() {
        return birth.equals("null") ? "" : birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getPhoto() {
        return photo.equals("null") ? "" : photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhone() {
        return phone.equals("null") ? "" : phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneMask() {
        return phoneMask.equals("null") ? "" : phoneMask;
    }

    public void setPhoneMask(String phoneMask) {
        this.phoneMask = phoneMask;
    }

    public String getEmail() {
        return email.equals("null") ? "" : email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country.equals("null") ? "" : country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCityId() {
        return cityId.equals("null") ? "" : cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getBalanceValue() {
        return balanceValue.equals("null") ? "" : balanceValue;
    }

    public void setBalanceValue(String balanceValue) {
        this.balanceValue = balanceValue;
    }

    public String getBalanceCurrency() {
        return balanceCurrency.equals("null") ? "" : balanceCurrency;
    }

    public void setBalanceCurrency(String balanceCurrency) {
        this.balanceCurrency = balanceCurrency;
    }

    public String getBonusValue() {
        return bonusValue.equals("null") ? "" : bonusValue;
    }

    public void setBonusValue(String bonusValue) {
        this.bonusValue = bonusValue;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String type) {
        this.paymentType = type;
    }

    public String getPan() {
        return pan.equals("null") ? "" : pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCompany() {
        return company.equals("null") ? "" : company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public long getColorID() {
        return colorID;
    }

    public void setColorID(long colorID) {
        this.colorID = colorID;
    }

    public int getThemeId() {
        return themeId;
    }

    public void setThemeId(int themeId) {
        this.themeId = themeId;
    }

    public int getMap() {
        return map;
    }

    public void setMap(int map) {
        this.map = map;
    }

    public List<Company> companies() {
        return getMany(Company.class, "profile");
    }

    public int getPaymentBonus() {
        return paymentBonus;
    }

    public void setPaymentBonus(int paymentBonus) {
        this.paymentBonus = paymentBonus;
    }

    public void clearProfileFields() {
        clientId = "";
        name = "";
        surname = "";
        patronymic = "";
        birth = "";
        photo = "";
        email = "";
        phone = "";
        phoneMask = "";
        country = "";
        balanceValue = "";
        balanceCurrency = "";
        bonusValue = "";

        if (AppParams.PAYMENT_WITH_CASH) setPaymentType("CASH");
        else setPaymentType("CARD");

        pan = "";
        company = "";
        authorized = false;
        tenantId = AppParams.TENANT_ID;
        colorID = AppParams.DEFAULT_THEME;
        new Delete().from(Company.class).execute();
    }

}
