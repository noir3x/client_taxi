package com.gootax.client.models;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "tariffs")
public class Tariff extends Model {

    @Column(name = "tariffId")
    private String tariffId;
    @Column(name = "tariffName")
    private String tariffName;
    @Column(name = "tariffIcon")
    private String tariffIcon;
    @Column(name = "tariffIconMini")
    private String tariffIconMini;
    @Column(name = "description")
    private String description;
    @Column(name = "cityID")
    private long cityID;

    public Tariff() {
        super();
    }

    public Tariff(String tariffId, String tariffName, long cityID) {
        super();
        this.tariffId = tariffId;
        this.tariffName = tariffName;
        this.cityID = cityID;
    }

    public static void deleteAllTariffs() {
        new Delete().from(Tariff.class).execute();
    }

    public static Tariff getTariffById(String tariffId) {
        return new Select().from(Tariff.class).where("tariffId = ?", tariffId).executeSingle();
    }

    public static List<Tariff> getTariffs(City city) {
        return new Select().from(Tariff.class).where("cityID = ?", city.getId()).execute();
    }

    public String getTariffId() {
        return tariffId;
    }

    public void setTariffId(String tariffId) {
        this.tariffId = tariffId;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getTariffIcon() {
        return tariffIcon;
    }

    public void setTariffIcon(String tariffIcon) {
        this.tariffIcon = tariffIcon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTariffIconMini() {
        return tariffIconMini;
    }

    public void setTariffIconMini(String tariffIconMini) {
        this.tariffIconMini = tariffIconMini;
    }

    public long getCityID() {
        return cityID;
    }

    public void setCityID(long cityID) {
        this.cityID = cityID;
    }
}
