package com.gootax.client.models;

public class Car {

    private double lat;
    private double lon;
    private float bearing;

    public Car(double lat, double lon, float bearing) {
        this.lat = lat;
        this.lon = lon;
        this.bearing = bearing;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public float getBearing() {
        return bearing;
    }

    public void setBearing(float bearing) {
        this.bearing = bearing;
    }
}
