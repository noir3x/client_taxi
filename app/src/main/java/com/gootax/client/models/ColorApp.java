package com.gootax.client.models;

import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "colorsApp")
public class ColorApp extends Model {

    @Column(name = "accentBg")
    private String accentBg;

    @Column(name = "accentText")
    private String accentText;

    @Column(name = "menuBg")
    private String menuBg;

    @Column(name = "menuText")
    private String menuText;

    @Column(name = "menuStroke")
    private String menuStroke;

    @Column(name = "contentBg")
    private String contentBg;

    @Column(name = "contentText")
    private String contentText;

    @Column(name = "contentStroke")
    private String contentStroke;

    @Column(name = "contentIconBg")
    private String contentIconBg;

    @Column(name = "contentIconStroke")
    private String contentIconStroke;

    @Column(name = "mapMarkerBg")
    private String mapMarkerBg;

    @Column(name = "mapMarkerBgStroke")
    private String mapMarkerBgStroke;

    @Column(name = "mapMarkerText")
    private String mapMarkerText;

    @Column(name = "mapCarBg")
    private String mapCarBg;

    @Column(name = "profileID")
    private Profile profile;

    public ColorApp() {

    }

    public ColorApp(String accentBg, String accentText, String menuBg, String menuText, String menuStroke, String contentBg, String contentText, String contentStroke, String contentIconBg, String contentIconStroke, String mapMarkerBg, String mapMarkerBgStroke, String mapMarkerText, String mapCarBg) {
        this.accentBg = accentBg;
        this.accentText = accentText;
        this.menuBg = menuBg;
        this.menuText = menuText;
        this.menuStroke = menuStroke;
        this.contentBg = contentBg;
        this.contentText = contentText;
        this.contentStroke = contentStroke;
        this.contentIconBg = contentIconBg;
        this.contentIconStroke = contentIconStroke;
        this.mapMarkerBg = mapMarkerBg;
        this.mapMarkerBgStroke = mapMarkerBgStroke;
        this.mapMarkerText = mapMarkerText;
        this.mapCarBg = mapCarBg;
    }

    public static ColorApp getColor(Profile profile) {
        return new Select().from(ColorApp.class).where("Id = ?", profile.getColorID()).executeSingle();
    }

    public static void deleteColors() {
        new Delete().from(ColorApp.class).execute();
    }

    public static List<ColorApp> getColors() {
        return new Select().from(ColorApp.class).execute();
    }

    public String getAccentBg() {
        return accentBg;
    }

    public String getAccentText() {
        return accentText;
    }

    public String getMenuBg() {
        return menuBg;
    }

    public String getMenuText() {
        return menuText;
    }

    public String getMenuStroke() {
        return menuStroke;
    }

    public String getContentBg() {
        return contentBg;
    }

    public String getContentText() {
        return contentText;
    }

    public String getContentStroke() {
        return contentStroke;
    }

    public String getContentIconBg() {
        return contentIconBg;
    }

    public String getContentIconStroke() {
        return contentIconStroke;
    }

    public String getMapMarkerBg() {
        return mapMarkerBg;
    }

    public String getMapMarkerBgStroke() {
        return mapMarkerBgStroke;
    }

    public String getMapMarkerText() {
        return mapMarkerText;
    }

    public String getMapCarBg() {
        return mapCarBg;
    }

}
