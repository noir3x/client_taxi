package com.gootax.client.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "companies")
public class Company extends Model {

    @Column(name = "company_id")
    private String companyId = "";
    @Column(name = "name")
    private String name = "";
    @Column(name = "full_name")
    private String fullName = "";
    @Column(name = "logo")
    private String logo = "";
    @Column(name = "profile")
    private Profile profile;

    public Company() {
        super();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

}
