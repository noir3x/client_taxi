package com.gootax.client.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "orders")
public class Order extends Model {

    @Column(name = "order_id")
    private String orderId = "";
    @Column(name = "order_number")
    private String orderNumber = "";
    @Column(name = "status")
    private String status = "";
    @Column(name = "status_label")
    private String statusLabel = "";
    @Column(name = "driver")
    private String driver = "";
    @Column(name = "photo")
    private String photo = "";
    @Column(name = "car")
    private String car = "";
    @Column(name = "cost")
    private String cost = "";
    @Column(name = "path")
    private String path = "";
    @Column(name = "create_time")
    private long createTime;
    @Column(name = "order_time")
    private String orderTime = "";
    @Column(name = "tariff")
    private String tariff;
    @Column(name = "review")
    private String review = "";
    @Column(name = "stars")
    private int stars;
    @Column(name = "comment")
    private String comment = "";
    @Column(name = "detail_cost")
    private String detailCost;
    @Column(name = "status_id")
    private String statusId;

    public Order() {
        super();
    }

    public static Order getOrder(String orderId) {
        return new Select()
                .from(Order.class)
                .where("order_id = ?", orderId)
                .executeSingle();
    }

    public static List<Order> getOrders() {
        return new Select().from(Order.class).where("status = 'rejected' OR status = 'completed'").orderBy("order_id DESC").execute();
    }

    public static List<Order> getPreOrders() {
        return new Select().from(Order.class).where("status = 'pre_order'").orderBy("order_id DESC").execute();
    }

    public static List<Order> getOrdersTest() {
        return new Select().from(Order.class).execute();
    }

    public static Order getActiveOrder() {
        return new Select().from(Order.class).where("status != 'rejected' AND status != 'completed' AND status != 'pre_order'").executeSingle();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getCar() {
        return car;
    }

    public void setCar(String car) {
        this.car = car;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getDetailCost() {
        return detailCost;
    }

    public void setDetailCost(String detailCost) {
        this.detailCost = detailCost;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }
}
