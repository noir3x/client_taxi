package com.gootax.client.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.SaveCitiesEvent;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.CitiesRequestListener;
import com.gootax.client.network.requests.GetTenantCityListRequest;
import com.gootax.client.services.RegistrationIntentService;
import com.gootax.client.utils.AppPreferences;

import java.util.Date;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class SplashActivity extends BaseSpiceActivity {

    private static final int SPLASH_DELAY = 15000;
    private Handler splashHandler;

    private Profile profile;
    private String regid;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isServicesAvailable, bDeviceToken, bCityRequest;
    private AlertDialog dialog;
    private TypedValue typedValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
            );
        }



        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean("sentTokenToServer", false);
                if (sentToken) {
                    try {
                        regid = intent.getExtras().getString("token");
                        new AppPreferences(getApplicationContext()).saveText("device_token", regid);
                        bDeviceToken = true;
                        if (bCityRequest) {
                            nextActivity();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };



        /*FrameLayout frameLayout = (FrameLayout) findViewById(R.id.splash);
        setTheme(AppParams.DEFAULT_THEME);
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = this.getTheme();
        theme.resolveAttribute(R.attr.splash_bg, typedValue, true);
        frameLayout.setBackgroundColor(typedValue.data);*/

        initDB();
        initColor();
        startRequest();
        startPlayServices();
        initHandler();
    }

    private void initDB() {
        profile = Profile.getProfile();
        if (profile == null) {
            profile = new Profile();
            profile.setCityId("0");
            profile.setMap(AppParams.DEFAULT_MAP);
            profile.setThemeId(AppParams.DEFAULT_THEME);
            profile.setTenantId(AppParams.TENANT_ID);
            if (AppParams.PAYMENT_WITH_CASH) profile.setPaymentType("CASH");
            else profile.setPaymentType("CARD");
            profile.save();
        }
    }

    private void initColor() {
        /*
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray((new OpenFile(this, "colors.json")).getJson());
            try {
                ColorApp.deleteColors();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonColor = new JSONObject(jsonArray.getString(i));
                ColorApp color = new ColorApp(
                        jsonColor.getString("accent_bg"),
                        jsonColor.getString("accent_text"),
                        jsonColor.getString("menu_bg"),
                        jsonColor.getString("menu_text"),
                        jsonColor.getString("menu_stroke"),
                        jsonColor.getString("content_bg"),
                        jsonColor.getString("content_text"),
                        jsonColor.getString("content_stroke"),
                        jsonColor.getString("content_icon_bg"),
                        jsonColor.getString("content_icon_stroke"),
                        jsonColor.getString("map_marker_bg"),
                        jsonColor.getString("map_marker_bg_stroke"),
                        jsonColor.getString("map_marker_text"),
                        jsonColor.getString("map_car_bg"));

                color.save();
<<<<<<< HEAD

                if (i == 3) {
                    Profile profile = Profile.getProfile();
                    profile.setColorID(color.getId());
                    profile.save();
                }
=======
>>>>>>> 2173748ffd8a582539ce65d233c7e5b12337e78b
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        */

    }

    private void startRequest() {
        getSpiceManager().execute(new GetTenantCityListRequest(getApplicationContext(),
                        String.valueOf((new Date()).getTime()), profile.getTenantId()),
                new CitiesRequestListener());
    }

    private void startPlayServices() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int resultCode = api.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            final int requestCode = 1;
            Dialog errorDialog = api.getErrorDialog(this, resultCode, requestCode);
            errorDialog.setCancelable(false);
            errorDialog.show();
        } else {
            isServicesAvailable = true;
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }
    }

    private void initHandler() {
        if (splashHandler != null) splashHandler.removeCallbacksAndMessages(null);
        splashHandler = new Handler();
        splashHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isServicesAvailable && (!bCityRequest || !bDeviceToken)) {
                    showErrorDialog(getString(R.string.activities_SplashActivity_dialog_title),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // Do nothing here because we override this button later to change the close behaviour.
                                    // However, we still need this because on older versions of Android unless we
                                    // pass a handler the button doesn't get instantiated
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // As well as the last
                                }
                            }, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (!bCityRequest) startRequest();
                                    if (!bDeviceToken) startPlayServices();
                                    initHandler();
                                }
                            });
                }
            }
        }, SPLASH_DELAY);
    }

    private void showErrorDialog(String message,
                                 DialogInterface.OnClickListener settingsListener,
                                 DialogInterface.OnClickListener quitListener,
                                 DialogInterface.OnClickListener repeatListener) {
        dialog = new AlertDialog.Builder(SplashActivity.this)
                .setMessage(message)
                .setNegativeButton(getString(
                        R.string.activities_SplashActivity_dialog_btn_settings), settingsListener)
                .setPositiveButton(getString(
                        R.string.activities_SplashActivity_dialog_btn_quit), quitListener)
                .setNeutralButton(getString(
                        R.string.activities_SplashActivity_dialog_btn_repeat), repeatListener)
                .setCancelable(false)
                .create();
        dialog.show();
        // Overriding standard behaviour
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Settings.ACTION_SETTINGS));
            }
        });
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void nextActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    @Subscribe
    public void onMessage(SaveCitiesEvent saveCitiesEvent) {
        if (saveCitiesEvent.isSaveCities()) {
            bCityRequest = true;

            if (bDeviceToken) {
                nextActivity();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mRegistrationBroadcastReceiver,
                        new IntentFilter("registrationComplete"));
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (dialog != null) dialog.dismiss();
        if (splashHandler != null) splashHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (splashHandler != null) splashHandler.removeCallbacks(null);
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        SplashActivity.this.finish();
    }

}
