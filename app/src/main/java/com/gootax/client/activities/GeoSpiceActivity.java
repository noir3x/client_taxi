package com.gootax.client.activities;

import android.support.v7.app.AppCompatActivity;

import com.octo.android.robospice.SpiceManager;
import com.gootax.client.network.GeoSpiceService;


public abstract class GeoSpiceActivity extends AppCompatActivity {

    private SpiceManager spiceManager = new SpiceManager(GeoSpiceService.class);

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

}
