package com.gootax.client.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.events.ProfileEvent;
import com.gootax.client.models.Company;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.UpdateProfileRequestListener;
import com.gootax.client.network.requests.UpdateProfileRequest;
import com.gootax.client.utils.InputMask;
import com.gootax.client.views.ToastWrapper;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import retrofit.mime.TypedFile;

public class EditProfileActivity
        extends BaseSpiceActivity implements TextView.OnEditorActionListener {

    public static final int REQUEST_CODE_PERMISSION_STORAGE = 5702;
    private static final int PHOTO_REQ_CODE = 533;

    private SimpleDraweeView imgView;
    private Uri newImgUri;
    private boolean photoIsChanged;

    private Profile currentProfile;
    private String clientIdFromDB;
    private String imgUrlFromDB;
    private String nameFromDB;
    private String surnameFromDB;
    private String phoneMaskFromDB;
    private String phoneFromDB;
    private String emailFromDB;

    private InputMask inputMask;
    private boolean mFormatting;

    private TextInputEditText editName;
    private TextInputEditText editSurname;
    private TextInputEditText editPhone;
    private TextInputEditText editEmail;
    private Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_edit_profile);
        setTitle(R.string.activities_EditProfileActivity_title);

        initToolbar();
        initVars();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initVars() {
        currentProfile = Profile.getProfile();
        clientIdFromDB = currentProfile.getClientId();
        imgUrlFromDB = currentProfile.getPhoto();
        nameFromDB = currentProfile.getName();
        surnameFromDB = currentProfile.getSurname();
        phoneMaskFromDB = currentProfile.getPhoneMask();
        phoneFromDB = currentProfile.getPhone();
        emailFromDB = currentProfile.getEmail();
    }

    private void initViews() {
        imgView = (SimpleDraweeView) findViewById(R.id.edit_profile_img);
        editName = (TextInputEditText) findViewById(R.id.edit_profile_name);
        editSurname = (TextInputEditText) findViewById(R.id.edit_profile_surname);
        editPhone = (TextInputEditText) findViewById(R.id.edit_profile_phone);
        editEmail = (TextInputEditText) findViewById(R.id.edit_profile_email);
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, PHOTO_REQ_CODE);
            }
        });
        if (!imgUrlFromDB.isEmpty()) {
            Uri uri = Uri.parse(imgUrlFromDB);
            imgView.setImageURI(uri);
        }
        editName.setText(nameFromDB);
        editSurname.setText(surnameFromDB);
        editEmail.setText(emailFromDB);
        editEmail.setOnEditorActionListener(this);
        setEditPhoneValues();
        setEditPhoneListeners();
        saveBtn = (Button) findViewById(R.id.edit_profile_save_btn);
        assert saveBtn != null;
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photoIsChanged) {
                    requestStorageWrapper();
                } else {
                    sendRequest();
                }
            }
        });
    }

    private void setEditPhoneValues() {
        inputMask = new InputMask(phoneMaskFromDB, phoneFromDB);
        mFormatting = false;
        String newText = inputMask.getNewText();
        editPhone.setText(newText);
        editPhone.setSelection(inputMask.getSelection(newText));
    }

    private void setEditPhoneListeners() {
        editPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editPhone.setSelection(inputMask
                        .getSelection(editPhone.getText().toString().trim()));
                mFormatting = false;
            }
        });
        editPhone.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {

            String phoneStr;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //empty
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                phoneStr = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mFormatting) {
                    mFormatting = true;
                    String result = inputMask.afterTextChangedSetText(phoneStr, s);
                    editPhone.setText(result);
                    editPhone.setSelection(inputMask.afterTextChangedSetSelection(result));
                } else {
                    mFormatting = false;
                }
            }
        });
    }

    @Subscribe
    public void onMessage(ProfileEvent event) {
        saveBtn.setEnabled(true);
        if (event.getSuccess() == 1) {
            event.getProfile().save();
            List<Company> clientCompanies = event.getCompanies();
            if (!clientCompanies.isEmpty()) {
                ActiveAndroid.beginTransaction();
                try {
                    for (Company company : clientCompanies) {
                        company.save();
                    }
                    ActiveAndroid.setTransactionSuccessful();
                } finally {
                    ActiveAndroid.endTransaction();
                }
            }
            NavUtils.navigateUpFromSameTask(this);
        } else if (event.getSuccess() == 0) {
            new ToastWrapper(this, R.string.activities_EditProfileActivity_req_error).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_REQ_CODE && resultCode == RESULT_OK) {
            photoIsChanged = true;
            newImgUri = data.getData();
            imgView.setImageURI(newImgUri);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            if (photoIsChanged) {
                requestStorageWrapper();
            } else {
                sendRequest();
            }
            return true;
        }
        return false;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void requestStorageWrapper() {
        int hasReadStoragePermission = ContextCompat
                .checkSelfPermission(EditProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (hasReadStoragePermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showMessageOKCancel(getString(R.string.activities_EditProfileActivity_permission_request),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(EditProfileActivity.this,
                                        new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                                        REQUEST_CODE_PERMISSION_STORAGE);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(EditProfileActivity.this,
                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_CODE_PERMISSION_STORAGE);
            return;
        }
        sendRequest();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EditProfileActivity.this)
                .setMessage(message)
                .setNegativeButton(
                        getString(R.string.activities_EditProfileActivity_permission_dialog_ok), okListener)
                .setPositiveButton(
                        getString(R.string.activities_EditProfileActivity_permission_dialog_cancel), null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendRequest();
                } else {
                    new ToastWrapper(this,
                            R.string.activities_EditProfileActivity_permission_rejected).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void sendRequest() {
        saveBtn.setEnabled(false);
        int countNum = phoneMaskFromDB
                .replaceAll("_","0")
                .replaceAll("\\D", "")
                .length();
        if (editPhone.getText().toString().trim()
                .replaceAll("\\D+", "").length() != countNum) {
            editPhone.setError(getString(R.string.activities_AuthActivity_edit_phone_error));
            saveBtn.setEnabled(true);
        } else {
            String newPhone = editPhone.getText().toString().trim().replaceAll("\\D+", "");
            String surname = editSurname.getText().toString().trim();
            String name = editName.getText().toString().trim();
            String email = editEmail.getText().toString().trim();
            String time = String.valueOf((new Date()).getTime());
            TypedFile photo = null;
            if (photoIsChanged) {
                photo = new TypedFile("multipart/form-data",
                        new File(getRealPathFromURI(newImgUri)));
            }
            currentProfile.setPhone(newPhone);
            currentProfile.save();
            getSpiceManager().execute(new UpdateProfileRequest(getApplicationContext(),
                            clientIdFromDB, phoneFromDB, newPhone,
                    surname, name, email, time, photo, currentProfile.getTenantId()),
                    new UpdateProfileRequestListener());
        }
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

}
