package com.gootax.client.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.geo_events.AutoCompleteEvent;
import com.gootax.client.events.geo_events.FullReverseEvent;
import com.gootax.client.events.geo_events.SearchEvent;
import com.gootax.client.models.Address;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.geo_listeners.AutoCompleteListener;
import com.gootax.client.network.listeners.geo_listeners.FullReverseListener;
import com.gootax.client.network.listeners.geo_listeners.SearchListener;
import com.gootax.client.network.requests.GetAutoCompleteRequest;
import com.gootax.client.network.requests.GetReverseRequest;
import com.gootax.client.network.requests.GetSearchRequest;
import com.gootax.client.views.adapters.AutoCompleteAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.Field;
import java.util.List;

public class AutoCompleteActivity extends GeoSpiceActivity implements
        SearchView.OnQueryTextListener, AdapterView.OnItemClickListener {

    public static final String USER_COORDS_KEY = "auto_complete_activity.user_coords_key";

    private MenuItem menuItem;
    private ListView completeListView;
    private TextView completeTextView;

    private Handler handler;
    private String textSearch;
    private Profile profile;
    private String[] userCoords;
    private int pointNum;
    private List<Address> addresses;
    private Button completeBtn;
    private TypedValue typedValueText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_auto_complete);
        setTitle(R.string.activities_AutoCompleteActivity_title);

        handler = new Handler();
        typedValueText = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_icon_bg, typedValue, true);

        initToolbar();
        initViews();
        initVars();
    }

    private void initViews() {
        completeListView = (ListView) findViewById(R.id.auto_complete_list);
        assert completeListView != null;
        completeListView.setOnItemClickListener(this);
        completeTextView = (TextView) findViewById(R.id.auto_complete_back);
        completeBtn = (Button) findViewById(R.id.auto_complete_btn);
        assert completeBtn != null;
        completeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent(AutoCompleteActivity.this, MapActivity.class);
                mapIntent.putExtra(MainActivity.POINT_NUM_KEY, pointNum);
                startActivity(mapIntent);
            }
        });
    }

    private void initVars() {
        profile = Profile.getProfile();
        userCoords = getIntent().getStringArrayExtra(USER_COORDS_KEY);
        pointNum = getIntent().getIntExtra(MainActivity.POINT_NUM_KEY, 0);
        if (pointNum == 0) {
            completeBtn.setVisibility(View.GONE);
            getSpiceManager().execute(new GetReverseRequest(
                    profile.getCityId(), userCoords[0], userCoords[1],
                    "1", "30", profile.getTenantId()),
                    new FullReverseListener());
        } else {
            completeBtn.setVisibility(View.VISIBLE);
            executeGeoEvent(Address.getAddresses());
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }


    @Override
    public boolean onQueryTextChange(String newText) {
        textSearch = newText;
        if (newText.length() >= 3) {
            if (completeListView.getCount() == 0) {
                completeTextView.setText(R.string.activities_AutoCompleteActivity_text_process);
            }
            handler.removeCallbacksAndMessages(null);
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (AppParams.USE_SEARCH && pointNum != 0) {
                        getSpiceManager().execute(
                                new GetSearchRequest(
                                        profile.getCityId(), textSearch,
                                        userCoords[0], userCoords[1], profile.getTenantId()),
                                new SearchListener());
                    } else {
                        getSpiceManager().execute(
                                new GetAutoCompleteRequest(
                                        profile.getCityId(), textSearch,
                                        userCoords[0], userCoords[1], profile.getTenantId()),
                                new AutoCompleteListener());
                    }
                }
            }, 500);
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent returnIntent = new Intent(this, MainActivity.class);
        returnIntent.putExtra(MainActivity.POINT_NUM_KEY, pointNum);
        Address newAddress = addresses.get(position);
        returnIntent.putExtra(MainActivity.ADDRESS_KEY, newAddress);
        returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_COMPLETE);
        NavUtils.navigateUpTo(this, returnIntent);
    }

    @Subscribe
    public void onMessage(SearchEvent event) {
        executeGeoEvent(event.getAddresses());
    }

    @Subscribe
    public void onMessage(AutoCompleteEvent event) {
        executeGeoEvent(event.getAddresses());
    }

    @Subscribe
    public void onMessage(FullReverseEvent event) {
        executeGeoEvent(event.getAddresses());
    }

    private void executeGeoEvent(List<Address> addresses) {
        this.addresses = addresses;
        if (addresses != null && addresses.size() > 0) {
            completeTextView.setText(R.string.activities_AutoCompleteActivity_text_greeting);
            AutoCompleteAdapter completeAdapter = new AutoCompleteAdapter(
                    getApplicationContext(), addresses);
            completeListView.setAdapter(completeAdapter);
            completeListView.setVisibility(View.VISIBLE);
            completeTextView.setVisibility(View.GONE);
        } else {
            completeTextView.setText(R.string.activities_AutoCompleteActivity_text_error);
            completeListView.setAdapter(null);
            completeListView.setVisibility(View.GONE);
            completeTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_complete, menu);
        menuItem = menu.findItem(R.id.action_complete_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);
        searchView.setQueryHint(getString(R.string.activities_AutoCompleteActivity_search_hint));
        searchView.setOnQueryTextListener(this);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        AutoCompleteTextView searchTextView = (AutoCompleteTextView)
                searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchTextView.setHintTextColor(typedValueText.data);

        TypedArray a = getTheme().obtainStyledAttributes(R.style.AppTheme, new int[] {R.attr.custom_cursor});
        int attributeResourceId = a.getResourceId(0, 0);
        Drawable drawable = getResources().getDrawable(attributeResourceId);
        a.recycle();

        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, attributeResourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MenuItemCompat.expandActionView(menuItem);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_complete_search:
                MenuItemCompat.expandActionView(menuItem);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
