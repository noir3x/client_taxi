package com.gootax.client.activities;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.OrderInfoEvent;
import com.gootax.client.models.Address;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.models.RoutePoint;
import com.gootax.client.network.listeners.OrderInfoListener;
import com.gootax.client.network.listeners.RejectOrderListener;
import com.gootax.client.network.requests.GetOrderInfoRequest;
import com.gootax.client.network.requests.RejectOrderRequest;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PreOrderActivity extends BaseSpiceActivity {

    private Profile profile;
    private Order order;
    private SimpleDraweeView simpleDraweeDriver;
    private Handler getInfoHandler;
    private Runnable getInfoRunnable;
    private ProgressDialog cancelProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profile = Profile.getProfile();
        setTheme(profile.getThemeId());
        order = Order.getOrder(getIntent().getStringExtra("order_id"));

        setContentView(R.layout.activity_preorder);
        cancelProgressDialog = new ProgressDialog(this);

        EventBus.getDefault().register(this);

        loadOrderInfo();

        startOrderInfo();

    }

    @Override
    protected void onStart() {
        super.onStart();

        getSpiceManager().execute(new GetOrderInfoRequest(getApplicationContext(),
                        String.valueOf((new Date()).getTime()), order, profile.getTenantId()),
                new OrderInfoListener());
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (getInfoHandler != null)
            getInfoHandler.removeCallbacks(getInfoRunnable);
        super.onDestroy();
    }

    private void loadOrderInfo() {

        setTitle(formatDate(order.getCreateTime()));

        initToolbar();

        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.sdv_map);
        simpleDraweeDriver = (SimpleDraweeView) findViewById(R.id.sdv_detailorder_face);
        if (!AppParams.USE_PHOTO) simpleDraweeDriver.setVisibility(View.GONE);

        List<Address> addressList = RoutePoint.getRoute(order);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_addresslist);

        int count = 0;
        int[] icons = {R.drawable.a,
                R.drawable.b,
                R.drawable.c,
                R.drawable.d,
                R.drawable.e};

        for (Address address : addressList) {
            try {

                View view = LayoutInflater.from(this).inflate(R.layout.item_address, null);
                ((ImageView) view.findViewById(R.id.iv_item_icon)).setImageResource(icons[count]);
                ((TextView) view.findViewById(R.id.tv_item_street)).setText(address.getLabel());
                ((TextView) view.findViewById(R.id.tv_item_city)).setText(address.getCity());

                linearLayout.addView(view);
                count++;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        String sCoords = addressList.get(0).getLat() + "," + addressList.get(0).getLon();
        Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap?center=" + sCoords
                + "&zoom=13&size=" + width + "x" + height + "&maptype=roadmap&markers=color:black%7Clabel:A%7C" + sCoords
                + "&key=" + AppParams.GOOGLE_API_KEY);

        simpleDraweeView.setImageURI(uri);

        if (order.getCar().length() > 0) {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(order.getCar());
            ((TextView) findViewById(R.id.tv_detailorder_drivername)).setText(order.getDriver());
        } else {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(getString(R.string.activities_PreOrderActivity_driver_empty));
        }

        if (order.getPhoto() != null && order.getPhoto().length() > 0) {
            simpleDraweeDriver.setImageURI(Uri.parse(order.getPhoto()));
        }

        ((TextView) findViewById(R.id.tv_lastorders_cost)).setText(formatTime(order.getCreateTime(), order.getOrderTime()));
    }

    public void rejectOrder(View v) {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(getString(R.string.fragments_MainFragment_delete_order1));
            alertDialog.setMessage(getString(R.string.fragments_MainFragment_delete_order2));
            alertDialog.setPositiveButton(getString(R.string.fragments_MainFragment_yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            //(findViewById(R.id.btn_cancel_order)).setEnabled(false);
                            cancelProgressDialog.setMessage(getResources()
                                    .getString(R.string.fragments_MainFragment_delete_order_execute));
                            cancelProgressDialog.setCancelable(false);
                            cancelProgressDialog.show();
                            getSpiceManager().execute(new RejectOrderRequest(getApplicationContext(),
                                    String.valueOf((new Date()).getTime()),
                                    order, profile.getTenantId()),
                                    new RejectOrderListener());
                        }
                    });
            alertDialog.setNegativeButton(getString(R.string.fragments_MainFragment_no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {

        order.setStatusLabel(orderInfoEvent.getStatusLabel());
        order.setStatus(orderInfoEvent.getStatus());
        order.setCar(orderInfoEvent.getCarDesc());
        order.setDriver(orderInfoEvent.getDriverName());
        order.setPhoto(orderInfoEvent.getPhoto());
        order.setCost(orderInfoEvent.getCost());
        order.setStatusId(orderInfoEvent.getStatusId());
        order.save();

        if (order.getCar().length() > 0) {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(order.getCar());
            ((TextView) findViewById(R.id.tv_detailorder_drivername)).setText(order.getDriver());
        }

        if (order.getPhoto() != null && order.getPhoto().length() > 0) {
            simpleDraweeDriver.setImageURI(Uri.parse(order.getPhoto()));
        }

        switch (orderInfoEvent.getStatus()) {
            case "completed":
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);

                startActivity(new Intent(getApplicationContext(), OrderCompletedActivity.class).putExtra("order_id", order.getOrderId()));
                break;
            case "rejected":
                if (getInfoHandler != null)
                    getInfoHandler.removeCallbacks(getInfoRunnable);

                order.setStatusLabel(getString(R.string.fragments_MainFragment_status_label_rejected));
                order.save();

                startActivity(new Intent(getApplicationContext(), OrderDetailActivity.class).putExtra("order_id", order.getOrderId()).putExtra("type", "main").putExtra("status", "rejected"));
                break;
            case "pre_order":

                break;
            default:
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER);
                intent.putExtra("order_id", order.getOrderId());
                startActivity(intent);
                finish();

        }
    }

    private void startOrderInfo() {
        if (getInfoHandler != null && getInfoRunnable != null) {
            getInfoHandler.removeCallbacks(getInfoRunnable);
        }
        getInfoHandler = new Handler();
        getInfoRunnable = new Runnable() {
            @Override
            public void run() {

                getSpiceManager().execute(new GetOrderInfoRequest(getApplicationContext(),
                        String.valueOf((new Date()).getTime()), order, profile.getTenantId()),
                        new OrderInfoListener());

                getInfoHandler.postDelayed(getInfoRunnable, 5000);

            }
        };
        getInfoHandler.postDelayed(getInfoRunnable, 5000);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

    private String formatTime(long longDateCreate, String strDatePlant) {
        Date datePlant = null;

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        String str = "%d " + getString(R.string.activities_PreOrderActivity_date_day_short) + ". %d " + getString(R.string.activities_PreOrderActivity_date_hour_short) + ". %d " + getString(R.string.fragments_FragmentAbstractMap_time_minute_short) + ".";


        try {
            datePlant = formatter.parse(strDatePlant);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (datePlant != null) {
            long mSec = datePlant.getTime() - longDateCreate;
            if (mSec > 0) {
                long days = TimeUnit.MILLISECONDS.toDays(mSec);
                long hours = TimeUnit.MILLISECONDS.toHours(mSec) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(mSec));
                long minutes = TimeUnit.MILLISECONDS.toMinutes(mSec) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(mSec));
                return String.format(str, days, hours, minutes);
            } else {
                return String.format(str, 0, 0, 0);
            }
        } else {
            return String.format(str, 0, 0, 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, PreOrdersActivity.class));
        finish();
    }

}
