package com.gootax.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.SaveCitiesEvent;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.CitiesRequestListener;
import com.gootax.client.network.requests.GetTenantCityListRequest;
import com.gootax.client.utils.LocaleHelper;
import com.gootax.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class OptionsActivity extends BaseSpiceActivity {

    private Profile profile;
    private String newTenant;
    private HashMap<Integer,String> langs;

    private TextInputEditText tenantEdit;
    private RadioGroup langsGroup, radioGroupMaps, radioGroupColors;
    private ProgressDialog tenantRequestsDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_options);
        setTitle(R.string.activities_OptionsActivity_title);

        initVars();
        initToolbar();
        initViews();
        initColors();
        initMaps();
    }

    private void initVars() {
        profile = Profile.getProfile();
        langs = new HashMap<>();
        langs.put(R.id.options_lang_az, "az");
        langs.put(R.id.options_lang_ru, "ru");
        langs.put(R.id.options_lang_en, "en");
        langs.put(R.id.options_lang_ka, "ka");
        langs.put(R.id.options_lang_uz, "uz");
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        tenantEdit = (TextInputEditText) findViewById(R.id.options_tenant_edit);
        tenantEdit.setText(profile.getTenantId());
        radioGroupMaps = (RadioGroup) findViewById(R.id.options_maps);
        radioGroupColors = (RadioGroup) findViewById(R.id.options_colors);
        langsGroup = (RadioGroup) findViewById(R.id.options_langs);
        switch (Locale.getDefault().getLanguage()) {
            case "az":
                langsGroup.check(R.id.options_lang_az);
                break;
            case "ru":
                langsGroup.check(R.id.options_lang_ru);
                break;
            case "ka":
                langsGroup.check(R.id.options_lang_ka);
                break;
            case "uz":
                langsGroup.check(R.id.options_lang_uz);
                break;
            case "en":
            default:
                langsGroup.check(R.id.options_lang_en);
                break;
        }
        if (!AppParams.IS_DEMO) {
            View tenantTitle = findViewById(R.id.options_tenant_title);
            if (tenantTitle != null) tenantTitle.setVisibility(View.GONE);
            View tenantLayout = findViewById(R.id.options_tenant_layout);
            if (tenantLayout != null) tenantLayout.setVisibility(View.GONE);
            View colorsTitle = findViewById(R.id.options_colors_title);
            if (colorsTitle != null) colorsTitle.setVisibility(View.GONE);
            radioGroupColors.setVisibility(View.GONE);
        }
        Button saveBtn = (Button) findViewById(R.id.options_confirm);
        if (saveBtn != null) saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkData();
            }
        });
    }

    private void initColors() {
        int count = 0;
        for (int themeId : AppParams.THEMES) {
            radioGroupColors.getChildAt(count).setTag(themeId);
            if (themeId == profile.getThemeId())
                ((RadioButton)radioGroupColors.getChildAt(count)).setChecked(true);
            count++;
        }
    }

    private void initMaps() {
        int count = 0;
        for (int iMap : AppParams.MAPS) {
            radioGroupMaps.getChildAt(count).setTag(iMap);
            if (iMap == profile.getMap())
                ((RadioButton)radioGroupMaps.getChildAt(count)).setChecked(true);
            count++;
        }
    }

    private void checkData() {
        tenantRequestsDialog = new ProgressDialog(this);
        tenantRequestsDialog.setTitle(getString(R.string.activities_OptionsActivity_progress_title));
        tenantRequestsDialog.setMessage(getString(R.string.activities_OptionsActivity_progress_message));
        tenantRequestsDialog.setCancelable(false);
        tenantRequestsDialog.show();
        newTenant = tenantEdit.getText().toString().trim();
        getSpiceManager().execute(new GetTenantCityListRequest(getApplicationContext(),
                        String.valueOf((new Date()).getTime()), newTenant),
                new CitiesRequestListener());
    }

    @Subscribe
    public void onMessage(SaveCitiesEvent saveCitiesEvent) {
        int infoText = saveCitiesEvent.getInfoText();
        if (saveCitiesEvent.isSaveCities()) {
            saveData(infoText);
        } else {
            showError(infoText);
        }
    }

    private void saveData(int infoText) {
        tenantRequestsDialog.dismiss();
        profile.setTenantId(newTenant);
        profile.setThemeId((int)radioGroupColors
                .findViewById(radioGroupColors.getCheckedRadioButtonId()).getTag());
        profile.setMap((int)radioGroupMaps
                .findViewById(radioGroupMaps.getCheckedRadioButtonId()).getTag());
        profile.save();
        String lang = langs.get(langsGroup.getCheckedRadioButtonId());
        if (lang == null) lang = langs.get(R.id.options_lang_en);
        LocaleHelper.setLocale(this, lang);
        new ToastWrapper(this, infoText).show();
    }

    private void showError(int errorText) {
        tenantRequestsDialog.dismiss();
        new ToastWrapper(this, errorText).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this, MainActivity.class);
        startActivity(returnIntent);
    }

}
