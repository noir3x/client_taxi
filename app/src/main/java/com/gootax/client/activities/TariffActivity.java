package com.gootax.client.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gootax.client.R;
import com.gootax.client.fragments.PageItemFragment;
import com.gootax.client.models.City;
import com.gootax.client.models.Profile;
import com.gootax.client.models.Tariff;

import java.util.List;

public class TariffActivity extends AppCompatActivity {

    List<Tariff> tariffList;
    private ViewPager.OnPageChangeListener pageChangeListener;
    private LinearLayout llNavi;
    private int countTariffs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_tariff);
        setTitle(R.string.activities_TariffActivity_title);
        initToolbar();

        tariffList = Tariff.getTariffs(City.getCity(Profile.getProfile().getCityId()));
        countTariffs = tariffList.size();
        llNavi = (LinearLayout) findViewById(R.id.ll_navi);
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        PagerAdapter pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        for (int i = 0; i < countTariffs; i++) {
            ImageView imageView = (ImageView) getLayoutInflater().inflate(R.layout.item_image_tariff, null);
            if (tariffList.get(i).getTariffId().equals(getIntent().getStringExtra("tariff_id"))) {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_blue));
                pager.setCurrentItem(i);
            } else {
                imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_gray));
            }

            llNavi.addView(imageView);
        }



        pageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                //sliderTxt.setText(String.valueOf(position + 1) + "/" + PAGE_COUNT);
                llNavi.removeAllViews();
                for (int i = 0; i < countTariffs; i++) {
                    //ImageView imageView = new ImageView(getApplicationContext());
                    ImageView imageView = (ImageView) getLayoutInflater().inflate(R.layout.item_image_tariff, null);
                    if (i == position)
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_blue));
                    else
                        imageView.setImageDrawable(getResources().getDrawable(R.drawable.point_gray));

                    llNavi.addView(imageView);
                }

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        };
        pager.addOnPageChangeListener(pageChangeListener);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PageItemFragment.newInstance(position, tariffList.get(position).getTariffId());
        }

        @Override
        public int getCount() {
            return tariffList.size();
        }
    }
}
