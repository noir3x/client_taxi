package com.gootax.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;

import com.gootax.client.R;
import com.gootax.client.events.ReviewEvent;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.AddReviewListener;
import com.gootax.client.network.requests.AddReviewRequest;
import com.gootax.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class ReviewActivity extends BaseSpiceActivity {

    private Profile profile;
    private Order order;
    private Button btnConfirm;
    private TextInputEditText etReview;
    private RatingBar rbReview;

    public static final int REQUEST_CODE_ORDER_INFO = 10;
    public static final int REQUEST_CODE_ORDER_COMPLETE = 11;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_review);

        profile = Profile.getProfile();
        order = Order.getOrder(getIntent().getStringExtra("order_id"));
        setTitle(R.string.activities_ReviewActivity_title);
        initToolbar();

        btnConfirm = (Button) findViewById(R.id.btn_review_confirm);
        etReview = (TextInputEditText) findViewById(R.id.et_review);
        rbReview = (RatingBar) findViewById(R.id.rb_review);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    public void sendReview(View v) {
        if (rbReview.getRating() == 0){
            new ToastWrapper(this, R.string.activities_ReviewActivity_error_review).show();
            return;
        }
        getSpiceManager().execute(
                new AddReviewRequest(this, order.getOrderId(),
                        String.valueOf(Math.round(rbReview.getRating())),
                        etReview.getText().toString().trim(), profile.getTenantId()),
                new AddReviewListener());

        btnConfirm.setEnabled(false);
    }

    @Subscribe
    public void onMessage(ReviewEvent reviewEvent) {
        if (reviewEvent.getStatus().equals("OK")) {

            order.setReview(Math.round(rbReview.getRating()) + "");
            order.save();

            new ToastWrapper(this, R.string.activities_ReviewActivity_toast).show();
            setResult(RESULT_OK, new Intent().putExtra("rating", Math.round(rbReview.getRating())));
            finish();
        } else {
            new ToastWrapper(this, R.string.activities_ReviewActivity_request_fail).show();
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    @Override
    public void onStart() {
        EventBus.getDefault().register(this);
        super.onStart();
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
