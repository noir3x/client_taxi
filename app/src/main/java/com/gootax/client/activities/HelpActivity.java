package com.gootax.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gootax.client.R;
import com.gootax.client.events.CompanyPageEvent;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.CompanyRequestListener;
import com.gootax.client.network.requests.GetCompanyPageRequest;

import java.util.Date;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class HelpActivity extends BaseSpiceActivity {

    private WebView webView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_help);
        setTitle(R.string.activities_HelpActivity_title);

        initToolbar();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        webView = (WebView) findViewById(R.id.webview_company);
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.startsWith("http://") && !url.startsWith("https://"))
                    url = "http://" + url;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressDialog.cancel();
            }

        });
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.activities_HelpActivity_text_load));
        progressDialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Profile profile = Profile.getProfile();
        getSpiceManager().execute(new GetCompanyPageRequest(this,
                        String.valueOf((new Date()).getTime()), profile.getTenantId()),
                new CompanyRequestListener());
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onMessage(CompanyPageEvent pageEvent) {
        String webPageString = pageEvent.getWebPageString();
        if (!webPageString.isEmpty()) {
            webView.loadDataWithBaseURL(null, webPageString, "text/html", "UTF-8", null);
        }
        progressDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
