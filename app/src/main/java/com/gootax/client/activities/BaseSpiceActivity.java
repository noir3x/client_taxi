package com.gootax.client.activities;


import android.support.v7.app.AppCompatActivity;

import com.gootax.client.network.RetrofitSpiceService;
import com.octo.android.robospice.SpiceManager;

public abstract class BaseSpiceActivity extends AppCompatActivity {

    private SpiceManager spiceManager = new SpiceManager(RetrofitSpiceService.class);

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();
    }

    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }

}
