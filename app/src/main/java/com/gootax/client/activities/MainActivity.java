package com.gootax.client.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.events.CurrentLocationEvent;
import com.gootax.client.events.OrderRepeatEvent;
import com.gootax.client.events.OrderTimeEvent;
import com.gootax.client.events.PointsMapEvent;
import com.gootax.client.events.RestoreFooterEvent;
import com.gootax.client.events.UpdatePointsEvent;
import com.gootax.client.events.geo_events.ChangeTariff;
import com.gootax.client.fragments.MainFragment;
import com.gootax.client.models.Address;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.models.RoutePoint;
import com.gootax.client.services.LocationService;
import com.gootax.client.utils.AddressList;
import com.gootax.client.utils.LocManager;
import com.gootax.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

public class MainActivity extends BaseSpiceActivity {

    public static final String BACK_KEY = "main_activity.back_key";
    public static final String BACK_VALUE_PROFILE = "main_activity.value_profile";
    public static final String BACK_VALUE_AUTH = "main_activity.value_auth";
    public static final String BACK_VALUE_COMPLETE = "main_activity.value_complete";
    public static final String BACK_VALUE_MAP = "main_activity.value_map";
    public static final String BACK_VALUE_NEW_ORDER = "main_activity.new_order";
    public static final String BACK_VALUE_OLD_ORDER = "main_activity.old_order";
    public static final String BACK_VALUE_PAYMENT = "main_activity.payment";
    public static final String BACK_VALUE_OPTIONS = "main_activity.options";

    public static final String POINT_NUM_KEY = "main_activity.point_num_key";
    public static final String ADDRESS_KEY = "complete_activity.address_key";

    public static final int REQUEST_CODE_DETAIL = 2;
    public static final int REQUEST_CODE_WISHES = 3;
    public static final int REQUEST_CODE_TARIFF = 4;

    private DrawerLayout drawer;
    private ActionBarDrawerToggle drawerToggle;

    private Profile profile;
    private Order oldOrder;
    private Order order;
    private List<Address> addressList;
    private MainFragment mainFragment;
    private Toolbar toolbar;

    public boolean activeOrder;
    private boolean isServiceStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme(Profile.getProfile().getThemeId());

        setContentView(R.layout.activity_main);
        setTitle(R.string.activities_MainActivity_title);

        initVars();
        initToolbarAndDrawer();
        setMainFragment();

        requestLocationWrapper();
        EventBus.getDefault().register(this);
    }

    private void initVars() {
        activeOrder = false;
        profile = Profile.getProfile();
        oldOrder = Order.getActiveOrder();
        if (oldOrder != null) {
            order = oldOrder;
        } else {
            order = new Order();
            order.setStatus("empty");
        }
        addressList = new AddressList();
        addressList.add(new Address());
        Address addressB = new Address();
        addressB.setEmpty(true);
        addressList.add(addressB);
        backNavigation();
    }

    private void backNavigation() {
        returnFromPush(getIntent());
        String backKey = getIntent().getStringExtra(BACK_KEY);
        if (backKey != null && backKey.equals(BACK_VALUE_OLD_ORDER)) {
            returnToOldOrder(getIntent());
        } else if (backKey != null && backKey.equals(BACK_VALUE_NEW_ORDER)) {
            returnToNewOrder(oldOrder);
        }
    }

    private void initToolbarAndDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.menu_drawer_open, R.string.menu_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                EventBus.getDefault().post(new RestoreFooterEvent());
            }
        };
        drawer.addDrawerListener(drawerToggle);
        NavigationView nvDrawer = (NavigationView) findViewById(R.id.nvView);
        setupDrawerHeader(nvDrawer);
        setupDrawerContent(nvDrawer);
    }

    private void setupDrawerHeader(NavigationView navigationView) {
        View headerLayout = navigationView.getHeaderView(0);
        LinearLayout rootLayout = (LinearLayout) headerLayout
                .findViewById(R.id.nav_header_layout);
        SimpleDraweeView photoView = (SimpleDraweeView)
                headerLayout.findViewById(R.id.nav_header_photo);
        TextView headerName = (TextView) headerLayout.findViewById(R.id.nav_header_name);
        String photoUrl = profile.getPhoto();
        if (!photoUrl.isEmpty()) {
            Uri uri = Uri.parse(photoUrl);
            photoView.setImageURI(uri);
        } else {
            photoView.getHierarchy().reset();
        }
        if (profile.isAuthorized()) {
            String fullName = profile.getName() + " " + profile.getSurname();
            if (!fullName.trim().isEmpty()) {
                headerName.setText(fullName);
            } else {
                headerName.setText(R.string.menu_header_name);
            }
            headerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(profileIntent);
                    drawer.closeDrawers();
                }
            });
            rootLayout.setBackgroundResource(R.color.colorPrimary);
        } else {
            headerName.setText(R.string.menu_header_not_auth_name);
            headerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent startActivityIntent = new Intent(MainActivity.this, AuthActivity.class);
                    startActivityIntent
                            .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_UP);
                    startActivity(startActivityIntent);
                    drawer.closeDrawers();
                }
            });
            rootLayout.setBackgroundResource(R.color.drawer_header_back);
        }
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setItemIconTintList(null);
        View signLayout = findViewById(R.id.nav_sign_layout);
        if (profile.isAuthorized()) {
            if (signLayout != null) signLayout.setVisibility(View.GONE);
        } else {
            if (signLayout != null) signLayout.setVisibility(View.VISIBLE);
        }
        Button signUpBtn = (Button) findViewById(R.id.nav_signup_btn);
        if (signUpBtn != null) signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startActivityIntent = new Intent(MainActivity.this, AuthActivity.class);
                startActivityIntent
                        .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_UP);
                startActivity(startActivityIntent);
                drawer.closeDrawers();
            }
        });
        Button signInBtn = (Button) findViewById(R.id.nav_signin_btn);
        if (signInBtn != null) signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startActivityIntent = new Intent(MainActivity.this, AuthActivity.class);
                startActivityIntent
                        .putExtra(AuthActivity.FRAGMENT_KEY, AuthActivity.FRAGMENT_SIGN_IN);
                startActivity(startActivityIntent);
                drawer.closeDrawers();
            }
        });
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    public void selectDrawerItem(MenuItem menuItem) {
        Intent startActivityIntent;
        switch(menuItem.getItemId()) {
            case R.id.nav_payment:
                startActivityIntent = new Intent(this, PaymentActivity.class);
                break;
            case R.id.nav_history:
                startActivityIntent = new Intent(this, LastOrdersActivity.class);
                break;
            case R.id.nav_preorders:
                startActivityIntent = new Intent(this, PreOrdersActivity.class);
                break;
            case R.id.nav_options:
                startActivityIntent = new Intent(this, OptionsActivity.class);
                finish();
                break;
            case R.id.nav_help:
                startActivityIntent = new Intent(this, HelpActivity.class);
                break;
            /*case R.id.nav_third_item:
                startActivityIntent = new Intent(this, AddressesActivity.class);
                break;*/
            default:
                startActivityIntent = new Intent(this, LastOrdersActivity.class);
        }
        startActivity(startActivityIntent);
        drawer.closeDrawers();
    }

    private void setMainFragment() {
        mainFragment = MainFragment.getInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, mainFragment);
        transaction.commit();
    }

    private void reloadMainFragment() {
        setTitle(R.string.activities_MainActivity_title);
        mainFragment = MainFragment.getInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.flContent, mainFragment);
        transaction.commitAllowingStateLoss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_DETAIL:
                    Address address = addressList.get(0);
                    String street = data.getStringExtra("street");
                    if (street != null && address.isGps()) {
                        address.setStreet(street);
                        address.setLabel(street);
                    }
                    address.setPorch(data.getStringExtra("porch"));
                    address.setApt(data.getStringExtra("apt"));
                    addressList.set(0, address);
                    order.setComment(data.getStringExtra("comment"));
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdateAddressList();
                    break;
                case REQUEST_CODE_WISHES:
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).startCostRequest();
                    break;
                case REQUEST_CODE_TARIFF:
                    EventBus.getDefault().post(new ChangeTariff(data.getStringExtra("tariff_id")));
                    break;
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        returnFromPush(intent);

        Order oldOrder = Order.getActiveOrder();
        String backKey = intent.getStringExtra(BACK_KEY);
        if (backKey != null) {
            switch (backKey) {
                case BACK_VALUE_PROFILE:
                case BACK_VALUE_AUTH:
                    initToolbarAndDrawer();
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdatePayment();
                    break;
                case BACK_VALUE_COMPLETE:
                case BACK_VALUE_MAP:
                    int pointNum = intent.getIntExtra(POINT_NUM_KEY, 0);
                    Address address = intent.getParcelableExtra(ADDRESS_KEY);
                    if (pointNum >= ((AddressList) addressList).realSize()) {
                        addressList.add(address);
                    } else {
                        addressList.set(pointNum, address);
                    }
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdateAddressList();
                    if (pointNum == 0) {
                        String lat = address.getLat();
                        String lon = address.getLon();
                        if (lat != null && !lat.isEmpty() && lon != null && !lon.isEmpty()) {
                            EventBus.getDefault().post(new CurrentLocationEvent(
                                    Double.valueOf(lat),
                                    Double.valueOf(lon), false));
                        }
                    }
                    break;
                case BACK_VALUE_NEW_ORDER:
                    returnToNewOrder(oldOrder);
                    break;
                case BACK_VALUE_OLD_ORDER:
                    returnToOldOrder(intent);
                    break;
                case BACK_VALUE_PAYMENT:
                    if (oldOrder != null) {
                        order = oldOrder;
                        activeOrder = true;
                    } else {
                        //order = new Order();
                        //order.setStatus("empty");
                        activeOrder = false;
                    }
                    ((MainFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.flContent)).callUpdatePayment();
                    break;
            }
        }
        super.onNewIntent(intent);
    }

    private void returnFromPush(Intent intent) {
        String type = intent.getStringExtra("type");
        String orderId = intent.getStringExtra("order_id");
        if (type != null && type.equals("push") && orderId != null) {
            order = Order.getOrder(orderId);
            reloadMainFragment();
        }
    }

    private void returnToOldOrder(Intent intent) {
        EventBus.getDefault().postSticky(new OrderRepeatEvent());
        activeOrder = false;
        order = new Order();
        Order orderOld = Order.getOrder(intent.getStringExtra("order_id"));
        order.setStatus("empty");
        order.setTariff(orderOld.getTariff());
        addressList = RoutePoint.getRouteWithFakeAddress(orderOld);
        reloadMainFragment();
    }

    private void returnToNewOrder(Order oldOrder) {
        reloadMainFragment();
        if (oldOrder != null) {
            order = oldOrder;
            activeOrder = true;
        } else {
            order = new Order();
            order.setStatus("empty");
            activeOrder = false;
        }
        addressList.clear();
        addressList.add(new Address());
        Address addressB = new Address();
        addressB.setEmpty(true);
        addressList.add(addressB);
    }

    @Subscribe
    public void onMessage(UpdatePointsEvent updatePointsEvent) {
        updatePoints(activeOrder);
    }

    @Subscribe
    public void onMessage(OrderTimeEvent orderTimeEvent) {
        order.setOrderTime(orderTimeEvent.getOrderTime());
        ((MainFragment) getSupportFragmentManager()
                .findFragmentById(R.id.flContent)).callUpdateOrderTime();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePoints(activeOrder);
    }

    public void updatePoints(boolean activeOrder) {
        try {
            View imgView = findViewById(R.id.iv_center);
            assert imgView != null;
            if (addressList.size() > 1) {
                imgView.setVisibility(View.INVISIBLE);
            } else {
                if (!activeOrder)
                    imgView.setVisibility(View.VISIBLE);
            }
            if (!activeOrder) {
                ((MainFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.flContent)).startCostRequest();
            }
            EventBus.getDefault().post(new PointsMapEvent(
                    addressList, activeOrder, order.getStatus(), ""));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTitleText(String title) {
        toolbar.setTitle(title);
    }

    public Order getOrder() {
        return order;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawer.openDrawer(GravityCompat.START);
                return true;
        }
        return drawerToggle.onOptionsItemSelected(item)
                || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        drawer.removeDrawerListener(drawerToggle);
        if (isServiceStarted) stopService(new Intent(this, LocationService.class));
        EventBus.getDefault().unregister(this);

        super.onDestroy();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setTitle(getString(R.string.activities_MainActivity_exit_title));
        alertDialog.setMessage(getString(R.string.activities_MainActivity_exit_question));
        alertDialog.setPositiveButton(getString(R.string.activities_MainActivity_exit_answer_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton(getString(R.string.activities_MainActivity_exit_answer_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    private void requestLocationWrapper() {
        int hasWriteLocPermission = ContextCompat
                .checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (hasWriteLocPermission != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                showMessageOKCancel(getString(R.string.activities_MainActivity_permission_request),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                                        LocManager.REQUEST_CODE_PERMISSION_LOC);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[] {Manifest.permission.ACCESS_FINE_LOCATION},
                    LocManager.REQUEST_CODE_PERMISSION_LOC);
            return;
        }
        startLocSerivce();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MainActivity.this)
                .setMessage(message)
                .setNegativeButton(
                        getString(R.string.activities_MainActivity_permission_dialog_ok), okListener)
                .setPositiveButton(
                        getString(R.string.activities_MainActivity_permission_dialog_cancel), null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LocManager.REQUEST_CODE_PERMISSION_LOC:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocSerivce();
                } else {
                    new ToastWrapper(this,
                            R.string.activities_MainActivity_permission_rejected).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startLocSerivce() {
        isServiceStarted = true;
        startService(new Intent(this, LocationService.class));
    }

}
