package com.gootax.client.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.events.CoordsForReverseEvent;
import com.gootax.client.events.geo_events.ReverseEvent;
import com.gootax.client.maps.MapFactory;
import com.gootax.client.models.Address;
import com.gootax.client.models.Profile;
import com.gootax.client.views.ToastWrapper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MapActivity extends AppCompatActivity {

    private TextView labelText;
    private TextView cityText;
    private int pointNum;
    private int[] markerDrawables;
    private Address currentAddress;
    private String latForReverse;
    private String lonForReverse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_map);
        setTitle(R.string.activities_MapActivity_title);

        initToolbar();
        initVars();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.activity_map_toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initVars() {
        pointNum = getIntent().getIntExtra(MainActivity.POINT_NUM_KEY, 0);
        markerDrawables = new int[] {
                R.drawable.pin_a,
                R.drawable.pin_b,
                R.drawable.pin_c,
                R.drawable.pin_d,
                R.drawable.pin_e};
    }

    private void initViews() {
        Fragment mapFragment = MapFactory.getFragmentMap(getApplicationContext());
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.activity_map_container, mapFragment, "map")
                .commit();
        labelText = (TextView) findViewById(R.id.map_text_label);
        cityText = (TextView) findViewById(R.id.map_text_city);
        ImageView marker = (ImageView) findViewById(R.id.activity_map_marker);
        if (marker != null) marker.setImageResource(markerDrawables[pointNum]);
        Button mapBtn = (Button) findViewById(R.id.activity_map_btn);
        assert mapBtn != null;
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentAddress != null) {
                    Intent returnIntent = new Intent(MapActivity.this, MainActivity.class);
                    returnIntent.putExtra(MainActivity.POINT_NUM_KEY, pointNum);
                    returnIntent.putExtra(MainActivity.ADDRESS_KEY, currentAddress);
                    returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_MAP);
                    NavUtils.navigateUpTo(MapActivity.this, returnIntent);
                } else {
                    new ToastWrapper(MapActivity.this,
                            R.string.activities_MapActivity_address_not_defined).show();
                }
            }
        });
    }

    @Subscribe
    public void onMessage(ReverseEvent event) {
        currentAddress = event.getAddress();
        boolean isGpsAddress = event.isGpsAddress();
        if (isGpsAddress) {
            String gpsAddress =
                    getString(R.string.fragments_MainHeaderFragment_gps_address);
            currentAddress.setLabel(gpsAddress);
            currentAddress.setStreet(gpsAddress);
            currentAddress.setCity("");
            currentAddress.setLat(latForReverse);
            currentAddress.setLon(lonForReverse);
            currentAddress.setGps(true);
        }
        cityText.setText(currentAddress.getCity());
        labelText.setText(currentAddress.getLabel());
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessage(CoordsForReverseEvent event) {
        latForReverse = event.getLat();
        lonForReverse = event.getLon();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this, MainActivity.class);
        NavUtils.navigateUpTo(this, returnIntent);
    }

    public int getPointNum() {
        return pointNum;
    }

}
