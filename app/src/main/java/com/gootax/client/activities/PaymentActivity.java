package com.gootax.client.activities;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.ClientCardsEvent;
import com.gootax.client.models.Company;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.ClientCardsListener;
import com.gootax.client.network.requests.GetClientCards;
import com.gootax.client.views.ToastWrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class PaymentActivity extends BaseSpiceActivity {

    public static final int PAYMENT_ACT_REQ_CODE = 6642;

    private RadioGroup paymentGroup;
    private RadioGroup cardGroup;
    private Switch paymentBonus;
    private LayoutInflater inflater;
    private Profile profile;
    private List<Company> companies;
    private String lastPan;
    private boolean isAuthorized;
    private String clientId;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_payment);
        setTitle(R.string.activities_PaymentActivity_title);

        initToolbar();
        initVars();
        initViews();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initVars() {
        EventBus.getDefault().register(this);
        profile = Profile.getProfile();
        companies = profile.companies();
        lastPan = profile.getPan();
        isAuthorized = profile.isAuthorized();
        inflater = getLayoutInflater();
        clientId = profile.getClientId();
        phone = profile.getPhone();
        getSpiceManager().execute(new GetClientCards(clientId,
                        String.valueOf((new Date()).getTime()), phone, profile.getTenantId()),
                new ClientCardsListener());
    }

    private void initViews() {
        initRadioGroups();
        TextView paymentAddCard = (TextView) findViewById(R.id.payment_add_card);
        if (AppParams.PAYMENT_WITH_CARDS) {
            if (paymentAddCard != null) paymentAddCard
                    .setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAuthorized) {
                        Intent cardIntent = new Intent(PaymentActivity.this, CardActivity.class);
                        startActivityForResult(cardIntent, PAYMENT_ACT_REQ_CODE);
                    } else {
                        new ToastWrapper(PaymentActivity.this,
                                R.string.activities_PaymentActivity_add_card_auth_error).show();
                    }
                }
            });
        } else {

            if(paymentAddCard != null) {
                paymentAddCard.setVisibility(View.GONE);
            }
        }

        paymentBonus = (Switch) findViewById(R.id.payment_bonus);
        if (paymentBonus != null) {
            if (profile.getPaymentBonus() == 1) {
                paymentBonus.setChecked(true);
            }
        }
    }

    private void initRadioGroups() {
        paymentGroup = (RadioGroup) findViewById(R.id.payment_type_group);
        cardGroup = (RadioGroup) findViewById(R.id.card_group);
        String type = profile.getPaymentType();
        switch (type) {
            case "CASH":
                paymentGroup.check(R.id.cash_btn);
                break;
            case "PERSONAL_ACCOUNT":
                paymentGroup.check(R.id.acc_btn);
                break;
            case "CORP_BALANCE":
                paymentGroup.check(R.id.corp_btn);
                break;
            case "CARD":
                paymentGroup.check(R.id.card_btn);
                cardGroup.setVisibility(View.VISIBLE);
                break;
        }
        RadioButton cash = (RadioButton) findViewById(R.id.cash_btn);
        RadioButton corp = (RadioButton) findViewById(R.id.corp_btn);
        View card = findViewById(R.id.card_btn);
        if (!isAuthorized) {
            View acc = findViewById(R.id.acc_btn);
            if (acc != null) acc.setVisibility(View.GONE);
            if (corp != null) corp.setVisibility(View.GONE);
        }
        if (!AppParams.PAYMENT_WITH_CARDS) {
            if (card != null) card.setVisibility(View.GONE);
        }
        if (!AppParams.PAYMENT_WITH_CASH) {
            if (cash != null) cash.setVisibility(View.GONE);
        }
        if (companies.isEmpty()) {
            if (corp != null) corp.setVisibility(View.GONE);
        } else {
            Company company = companies.get(0);
            String corpTitle = getString(R.string.activities_PaymentActivity_type_company_defined)
                    + company.getName();
            if (corp != null) corp.setText(corpTitle);
        }
        paymentGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.card_btn) {
                    cardGroup.setVisibility(View.VISIBLE);
                } else {
                    cardGroup.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onMessage(ClientCardsEvent event) {
        ArrayList<String> cards = event.getClientCards();
        if (cards != null && !cards.isEmpty()) {
            cardGroup.removeAllViews();
            int checkedCardNum = 0;
            for (int i = 0; i < cards.size(); i++) {
                String pan = cards.get(i);
                RadioButton newCard = (RadioButton) inflater
                        .inflate(R.layout.item_card, cardGroup, false);
                newCard.setText(pan);
                cardGroup.addView(newCard, i);
                if (pan.equals(lastPan)) {
                    checkedCardNum = i;
                }
            }
            int checkedCardId = cardGroup.getChildAt(checkedCardNum).getId();
            cardGroup.check(checkedCardId);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PAYMENT_ACT_REQ_CODE && resultCode == RESULT_OK) {
            String returnKey = data.getStringExtra(CardActivity.RESULT_SUCCESS_KEY);
            if (returnKey != null && returnKey.equals(CardActivity.RESULT_SUCCESS_CARD)) {
                getSpiceManager().execute(new GetClientCards(clientId,
                                String.valueOf((new Date()).getTime()),
                        phone, profile.getTenantId()),
                        new ClientCardsListener());
                new ToastWrapper(this,
                        R.string.activities_PaymentActivity_add_card_success).show();
            }
        } else {
            new ToastWrapper(this,
                    R.string.activities_PaymentActivity_add_card_error).show();
        }
    }

    @Override
    public void onBackPressed() {
        int errorCode = 0;
        switch (paymentGroup.getCheckedRadioButtonId()) {
            case R.id.acc_btn:
                profile.setPaymentType("PERSONAL_ACCOUNT");
                break;
            case R.id.corp_btn:
                profile.setPaymentType("CORP_BALANCE");
                if (!companies.isEmpty()) {
                    Company company = companies.get(0);
                    profile.setCompany(company.getCompanyId());
                } else {
                    errorCode = 1;
                }
                break;
            case R.id.card_btn:
                profile.setPaymentType("CARD");
                boolean isCardSaved = false;
                if (cardGroup.getChildCount() != 0) {
                    for (int i = 0; i < cardGroup.getChildCount(); i++) {
                        RadioButton btn = (RadioButton) cardGroup.getChildAt(i);
                        if (btn.isChecked()) {
                            String panNum = btn.getText().toString();
                            profile.setPan(panNum);
                            isCardSaved = true;
                        }
                    }
                }
                if (cardGroup.getChildCount() == 0 || !isCardSaved) {
                    errorCode = 2;
                }
                break;
            case R.id.cash_btn:
                profile.setPaymentType("CASH");
            default:
        }

        if (paymentBonus.isChecked()) {
            profile.setPaymentBonus(1);
        }
        else {
            profile.setPaymentBonus(0);
        }

        if (errorCode == 1) {
            new ToastWrapper(this, R.string.activities_PaymentActivity_no_corp_error).show();
        }
        else {
            if (errorCode == 2) {
                new ToastWrapper(this, R.string.activities_PaymentActivity_no_card_error).show();
            }
            profile.save();
            Intent returnIntent = new Intent(this, MainActivity.class);
            returnIntent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_PAYMENT);
            NavUtils.navigateUpTo(this, returnIntent);
        }
    }

}
