package com.gootax.client.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.gootax.client.R;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.views.adapters.LastOrdersRecyclerAdapter;

import java.util.List;

public class PreOrdersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_pre_orders);
        setTitle(R.string.activities_PreOrdersActivity_title);

        initToolbar();

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.rv_lastorders);
        assert mRecyclerView != null;
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<Order> orderList = Order.getPreOrders();
        if (orderList.size() > 0) {
            LastOrdersRecyclerAdapter adapter = new LastOrdersRecyclerAdapter(this, orderList, "pre");
            mRecyclerView.setAdapter(adapter);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            View view = findViewById(R.id.rl_last_orders_empty);
            if (view != null) view.setVisibility(View.VISIBLE);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent(this, MainActivity.class);
        startActivity(returnIntent);
    }

}
