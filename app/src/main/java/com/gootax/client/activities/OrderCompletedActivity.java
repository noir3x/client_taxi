package com.gootax.client.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.models.City;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.views.menu.Menu;
import com.gootax.client.views.menu.MenuItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderCompletedActivity extends AppCompatActivity {

    private Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_complete);
        order = Order.getOrder(getIntent().getStringExtra("order_id"));
        setTitle(R.string.activities_OrderCompletedActivity_title);

        TypedValue typedValue = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_bg, typedValue, true);
        TypedValue typedValueText = new TypedValue();
        getTheme().resolveAttribute(R.attr.content_text, typedValueText, true);

        City city = City.getCity(Profile.getProfile().getCityId());
        String currency;
        if (city.getCurrency().equals("RUB"))
            currency = getString(R.string.currency);
        else
            currency = city.getCurrency();

        switch (order.getStatusId()) {
            case AppParams.STATUS_ID_COMPLETE_PAID:
                setTitle(R.string.activities_OrderCompletedActivity_paid);
                break;
            case AppParams.STATUS_ID_COMPLETE_NOPAID:
                setTitle(R.string.activities_OrderCompletedActivity_no_paid);
                break;
            default:
                setTitle(R.string.activities_OrderCompletedActivity_title);
        }


        initToolbar();

        ((TextView) findViewById(R.id.tv_ordercompleted_date)).setText(formatDate(order.getCreateTime()));

        if (order.getCost().length() > 0) {
            String cost = "0";
            try {
                JSONObject jsonCost = new JSONObject(order.getCost());
                cost = jsonCost.getString("summary_cost");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ((TextView) findViewById(R.id.tv_ordercompleted_cost)).setText(cost + " " + currency);
        }

        List<MenuItem> menuItems = new ArrayList<>();
        menuItems.add(new MenuItem(R.drawable.otchet, getString(R.string.menu_footer_report), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), OrderDetailActivity.class).putExtra("order_id", order.getOrderId()).putExtra("type", "complete").putExtra("status", order.getStatus()));
            }
        }));
        menuItems.add(new MenuItem(R.drawable.group, getString(R.string.menu_footer_about), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                String packageName = getApplicationContext().getPackageName();
                String send_text = getString(R.string.activities_OrderCompletedActivity_share1)
                        + getString(R.string.app_name)
                        + getString(R.string.activities_OrderCompletedActivity_share2)
                        + packageName;
                sendIntent.putExtra(Intent.EXTRA_TEXT, send_text);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.activities_OrderCompletedActivity_share_title)));
            }
        }));
        new Menu(menuItems, (LinearLayout) findViewById(R.id.ll_menu), this, typedValue.data, typedValueText.data);
    }


    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class)
                .putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_NEW_ORDER));
        finish();
    }

    public void reviewOrder(View v) {
        if (order.getReview() != null && order.getReview().length() > 0) {

        } else {
            startActivityForResult(new Intent(this, ReviewActivity.class).putExtra("order_id", order.getOrderId()), ReviewActivity.REQUEST_CODE_ORDER_COMPLETE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ReviewActivity.REQUEST_CODE_ORDER_COMPLETE:
                    order = Order.getOrder(getIntent().getStringExtra("order_id"));
                    LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_stars);
                    ImageView star1 = (ImageView) linearLayout.getChildAt(0);
                    ImageView star2 = (ImageView) linearLayout.getChildAt(1);
                    ImageView star3 = (ImageView) linearLayout.getChildAt(2);
                    ImageView star4 = (ImageView) linearLayout.getChildAt(3);
                    ImageView star5 = (ImageView) linearLayout.getChildAt(4);

                    int[] drawablesArray = new int[5];
                    for (int i = 0; i < 5; i++) {
                        if (i <= (Integer.valueOf(order.getReview()) - 1)) {
                            drawablesArray[i] = R.drawable.star;
                        } else {
                            drawablesArray[i] = R.drawable.star_grey;
                        }
                    }
                    star1.setImageResource(drawablesArray[0]);
                    star2.setImageResource(drawablesArray[1]);
                    star3.setImageResource(drawablesArray[2]);
                    star4.setImageResource(drawablesArray[3]);
                    star5.setImageResource(drawablesArray[4]);

                    break;
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

}
