package com.gootax.client.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gootax.client.R;
import com.gootax.client.models.City;
import com.gootax.client.models.Option;
import com.gootax.client.models.Profile;
import com.gootax.client.models.Tariff;

import java.util.List;


public class WishesActivity extends AppCompatActivity implements View.OnTouchListener{

    private List<Option> options;
    private LinearLayout llWishes;
    private RelativeLayout flWishesEmpty;
    private ScrollView svWishes;
    private String currency;
    private  View viewWishes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_wishes);
        setTitle(R.string.activities_WishesActivity_title);
        initToolbar();

        flWishesEmpty = (RelativeLayout) findViewById(R.id.rl_wishes_empty);
        llWishes = (LinearLayout) findViewById(R.id.ll_wishes);
        svWishes = (ScrollView) findViewById(R.id.sv_wishes);

        try {
            City city = City.getCity(Profile.getProfile().getCityId());
            if (city.getCurrency().equals("RUB"))
                currency = getString(R.string.currency);
            else
                currency = city.getCurrency();

            if (getIntent().getStringExtra("tariff_id") != null) {
                Tariff tariff = Tariff.getTariffById(getIntent().getStringExtra("tariff_id"));
                options = Option.getOptions(tariff);
                int count = 0;
                for (Option option : options) {
                    viewWishes = LayoutInflater.from(this).inflate(R.layout.item_wishes, null);
                    ((TextView) viewWishes.findViewById(R.id.tv_wishes)).setText(option.getOptionName());
                    ((TextView) viewWishes.findViewById(R.id.tv_wishes_cost)).setText("  " + option.getPrice() + " " + currency);
                    if (option.isChecked())
                        ((CheckBox) viewWishes.findViewById(R.id.cb_wishes)).setChecked(true);
                    (viewWishes.findViewById(R.id.cb_wishes)).setTag(R.id.tag_id, option.getId());
                    (viewWishes.findViewById(R.id.cb_wishes)).setTag(R.id.tag_count, count);

                    (viewWishes.findViewById(R.id.wishes_item_test)).setOnTouchListener(this);


                    llWishes.addView(viewWishes);
                    count++;
                }
                if (options.size() == 0)
                    emptyOptions();
            } else {
                emptyOptions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                view.setBackgroundResource(R.color.activity_back);
                break;
            case MotionEvent.ACTION_UP:
                view.setBackgroundResource(R.color.layout_back);
                CheckBox checkBox = ((CheckBox) view.findViewById(R.id.cb_wishes));
                if (checkBox.isChecked()) checkBox.setChecked(false);
                else checkBox.setChecked(true);
                options.get((int) checkBox.getTag(R.id.tag_count)).setChecked(checkBox.isChecked());
                options.get((int) checkBox.getTag(R.id.tag_count)).save();
                break;
            default:
                view.setBackgroundResource(R.color.layout_back);
        }
        return true;
    }


    private void emptyOptions() {
        svWishes.setVisibility(View.GONE);
        flWishesEmpty.setVisibility(View.VISIBLE);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK, new Intent());
        finish();
    }
}
