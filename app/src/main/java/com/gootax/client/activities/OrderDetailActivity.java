package com.gootax.client.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.gootax.client.R;
import com.gootax.client.app.AppParams;
import com.gootax.client.events.OrderInfoEvent;
import com.gootax.client.models.Address;
import com.gootax.client.models.City;
import com.gootax.client.models.Order;
import com.gootax.client.models.Profile;
import com.gootax.client.models.RoutePoint;
import com.gootax.client.network.listeners.OrderInfoListener;
import com.gootax.client.network.requests.GetOrderInfoRequest;
import com.gootax.client.utils.Validator;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class OrderDetailActivity extends BaseSpiceActivity {

    private Profile profile;
    private Order order;
    private String currency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        profile = Profile.getProfile();
        setTheme(profile.getThemeId());
        order = Order.getOrder(getIntent().getStringExtra("order_id"));
        City city = City.getCity(profile.getCityId());
        if (city.getCurrency().equals("RUB"))
            currency = getString(R.string.currency);
        else
            currency = city.getCurrency();

        if (getIntent().getStringExtra("status").equals("completed")) {
            setContentView(R.layout.activity_detail_order_completed);
        } else {
            setContentView(R.layout.activity_detail_order);
        }

        EventBus.getDefault().register(this);
        if (getIntent().getStringExtra("type").equals("push")) {
            getSpiceManager().execute(new GetOrderInfoRequest(this,
                            String.valueOf((new Date()).getTime()),
                            order,
                            profile.getTenantId()),
                    new OrderInfoListener());
        } else {
            loadOrderInfo();
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void loadOrderInfo() {
        setTitle(getString(R.string.activities_OrderDetailActivity_order_number)
                + order.getOrderNumber());

        initToolbar();

        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) findViewById(R.id.sdv_map);
        SimpleDraweeView simpleDraweeDriver = (SimpleDraweeView) findViewById(R.id.sdv_detailorder_face);
        if (!AppParams.USE_PHOTO) simpleDraweeDriver.setVisibility(View.GONE);


        List<Address> addressList = RoutePoint.getRoute(order);
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_addresslist);


        int count = 0;
        int[] icons = {R.drawable.a,
                R.drawable.b,
                R.drawable.c,
                R.drawable.d,
                R.drawable.e};

        for (Address address : addressList) {
            try {
                View view = LayoutInflater.from(this).inflate(R.layout.item_address, null);
                ((ImageView) view.findViewById(R.id.iv_item_icon)).setImageResource(icons[count]);
                ((TextView) view.findViewById(R.id.tv_item_street)).setText(address.getLabel());
                ((TextView) view.findViewById(R.id.tv_item_city)).setText(address.getCity());
                linearLayout.addView(view);
                count++;
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        String sCoords = addressList.get(0).getLat() + "," + addressList.get(0).getLon();
        Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/staticmap?center=" + sCoords
                + "&zoom=13&size=" + width + "x" + height + "&maptype=roadmap&markers=color:black%7Clabel:A%7C" + sCoords
                + "&key=" + AppParams.GOOGLE_API_KEY);

        simpleDraweeView.setImageURI(uri);

        if (order.getCar().length() > 0) {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(order.getCar());
            ((TextView) findViewById(R.id.tv_detailorder_drivername)).setText(order.getDriver());
        } else {
            ((TextView) findViewById(R.id.tv_detailorder_car)).setText(getString(R.string.activities_OrderDetailActivity_empty_driver));
        }

        if (order.getReview() != null && order.getReview().length() > 0) {
            (findViewById(R.id.ll_detailorder_rating)).setVisibility(View.VISIBLE);
            (findViewById(R.id.btn_review)).setVisibility(View.GONE);
            setRating(Integer.valueOf(order.getReview()));
        } else {
            (findViewById(R.id.ll_detailorder_rating)).setVisibility(View.GONE);
            (findViewById(R.id.btn_review)).setVisibility(View.VISIBLE);
        }

        if (order.getPhoto() != null && order.getPhoto().length() > 0) {
            simpleDraweeDriver.setImageURI(Uri.parse(order.getPhoto()));
        }


        parseDetailCost(order.getCost());
        ((TextView) findViewById(R.id.tv_detailorder_status)).setText(label(order));
    }

    @Subscribe
    public void onMessage(OrderInfoEvent orderInfoEvent) {
        switch (orderInfoEvent.getStatus()) {
            case "completed":
                order.setDetailCost(orderInfoEvent.getCost());
                break;
            case "rejected":

                break;

        }
        order.setStatusLabel(orderInfoEvent.getStatusLabel());
        order.setStatus(orderInfoEvent.getStatus());
        order.setCar(orderInfoEvent.getCarDesc());
        order.setDriver(orderInfoEvent.getDriverName());
        order.setPhoto(orderInfoEvent.getPhoto());
        order.setCost(orderInfoEvent.getCost());
        order.setStatusId(orderInfoEvent.getStatusId());
        order.save();

        loadOrderInfo();
    }

    private void parseDetailCost(String sDetailCost) {
        // FUCKING GOVNOCODE
        try {
            JSONObject jsonCost = new JSONObject(sDetailCost);

            Integer summaryCostValue = Validator.validateInt(jsonCost, "summary_cost");
            if (summaryCostValue < 0) summaryCostValue = 0;
            String summaryCost = summaryCostValue + " " + currency;
            TextView summaryCostTv = (TextView) findViewById(R.id.tv_detailorder_cost);
            if (summaryCostTv != null) summaryCostTv.setText(summaryCost);

            if (order.getStatus().equals("completed")) {
                LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_cost);

                double costCity = 0.0;
                double costOutCity = 0.0;
                if (!jsonCost.isNull("city_cost_time"))
                    costCity += Double.valueOf(jsonCost.getString("city_cost_time"));
                if (!jsonCost.isNull("city_cost"))
                    costCity += Double.valueOf(jsonCost.getString("city_cost"));
                if (!jsonCost.isNull("out_city_cost_time"))
                    costOutCity += Double.valueOf(jsonCost.getString("out_city_cost_time"));
                if (!jsonCost.isNull("out_city_cost"))
                    costOutCity += Double.valueOf(jsonCost.getString("out_city_cost"));

                ((TextView) findViewById(R.id.tv_detailorder_order_time)).setText(formatDate(order.getCreateTime()));

                TextView cityLabelTv = (TextView)
                        findViewById(R.id.tv_detailorder_complete_city_label);
                String cityLabel = jsonCost.isNull("city_distance") ?
                        getString(R.string.activities_OrderDetailActivity_city_cost) + "  0 " +
                                getString(R.string.activities_OrderDetailActivity_distance) :
                        getString(R.string.activities_OrderDetailActivity_city_cost) + "  " +
                                jsonCost.getString("city_distance") + " " +
                                getString(R.string.activities_OrderDetailActivity_distance)
                                + "  " + formatTime(Double.valueOf(jsonCost.getString("city_time")));
                if (cityLabelTv != null) cityLabelTv.setText(cityLabel);

                TextView outCityLabelTv = (TextView)
                        findViewById(R.id.tv_detailorder_complete_outcity_label);
                String outCityLabel = jsonCost.isNull("out_city_distance")
                        ? getString(R.string.activities_OrderDetailActivity_out_city_cost) + "  0 " + getString(R.string.activities_OrderDetailActivity_distance)
                        : getString(R.string.activities_OrderDetailActivity_out_city_cost) + "  " + jsonCost.getString("out_city_distance") + " " +
                        getString(R.string.activities_OrderDetailActivity_distance)
                        + "  " + formatTime(Double.valueOf(jsonCost.getString("out_city_time")));
                if (outCityLabelTv != null) outCityLabelTv.setText(outCityLabel);

                TextView placeTv = (TextView) findViewById(R.id.tv_detailorder_complete_place);
                Double plantingValue = Validator.validateDouble(jsonCost, "planting_price");
                if (plantingValue < 0.0) plantingValue = 0.0;
                String place = plantingValue + " " + currency;
                if (placeTv != null) placeTv.setText(place);

                TextView cityTv = (TextView) findViewById(R.id.tv_detailorder_complete_city);
                if (costCity < 0.0) costCity = 0.0;
                String city = String.valueOf(costCity) + " " + currency;
                if (cityTv != null) cityTv.setText(city);

                TextView outCityTv = (TextView) findViewById(R.id.tv_detailorder_complete_outcity);
                if (costOutCity < 0.0) costOutCity = 0.0;
                String outCity = String.valueOf(costOutCity) + " " + currency;
                if (outCityTv != null) outCityTv.setText(outCity);

                TextView summaryTv = (TextView) findViewById(R.id.tv_detailorder_complete_summary);
                Double summaryCostVal = Validator.validateDouble(jsonCost, "summary_cost");
                if (summaryCostVal < 0.0) summaryCostVal = 0.0;
                String summary = summaryCostVal + " " + currency;
                if (summaryTv != null) summaryTv.setText(summary);

                if (!jsonCost.isNull("additional_cost")) {
                    View viewAdditionalCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    ((TextView) viewAdditionalCost.findViewById(R.id.tv_item_label)).setText(R.string.activities_OrderDetailActivity_additional_cost);
                    Double addCost = Validator.validateDouble(jsonCost, "additional_cost");
                    if (addCost < 0.0) addCost = 0.0;
                    String addCostStr = addCost + " " + currency;
                    ((TextView) viewAdditionalCost.findViewById(R.id.tv_item_result)).setText(addCostStr);
                    if (linearLayout != null) linearLayout.addView(viewAdditionalCost);
                }

                if (!jsonCost.isNull("before_time_wait_cost")) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    String beforeTimeWait = getString(R.string.activities_OrderDetailActivity_before_time_wait_cost) + "  " +
                            formatTime(Double.valueOf(jsonCost.getString("before_time_wait")));
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(beforeTimeWait);
                    Double beforeTimeWaitVal = Validator.validateDouble(jsonCost, "before_time_wait_cost");
                    if (beforeTimeWaitVal < 0.0) beforeTimeWaitVal = 0.0;
                    String beforeTimeWaitCost = beforeTimeWaitVal + " " + currency;
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result))
                            .setText(beforeTimeWaitCost);
                    if (linearLayout != null) linearLayout.addView(viewTimeCost);
                }

                if (!jsonCost.isNull("city_wait_cost") && !jsonCost.isNull("out_wait_cost")) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    String cityTimeWait = getString(R.string.activities_OrderDetailActivity_wait_cost) + "  " +
                            formatTime((Double.valueOf(jsonCost.getString("city_time_wait")) / 60) +
                                    (Double.valueOf(jsonCost.getString("out_time_wait")) / 60));
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(cityTimeWait);
                    Double cityWaitVal = Validator.validateDouble(jsonCost, "city_wait_cost");
                    Double outWaitVal = Validator.validateDouble(jsonCost, "out_wait_cost");
                    Double totalVal = cityWaitVal + outWaitVal;
                    if (totalVal < 0.0) totalVal = 0.0;
                    String outWaitCost = totalVal + " " + currency;
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result)).setText(outWaitCost);
                    if (linearLayout != null) linearLayout.addView(viewTimeCost);
                }

                if (!jsonCost.isNull("planting_include")) {
                    View viewTimeCost = LayoutInflater.from(this).inflate(R.layout.item_detailorder_complete, null);
                    Double plantingVal = Validator.validateDouble(jsonCost, "planting_include");
                    if (plantingVal < 0.0) plantingVal = 0.0;
                    String plantingCost = getString(R.string.activities_OrderDetailActivity_planting_include) + "  " +
                            plantingVal + " " +
                            getString(R.string.activities_OrderDetailActivity_distance);
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_label)).setText(plantingCost);
                    String timeCost = "0 " + currency;
                    ((TextView) viewTimeCost.findViewById(R.id.tv_item_result)).setText(timeCost);
                    if (linearLayout != null) linearLayout.addView(viewTimeCost);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    public void sendReview(View v) {
        startActivityForResult(new Intent(this, ReviewActivity.class).putExtra("order_id", order.getOrderId()), ReviewActivity.REQUEST_CODE_ORDER_INFO);
    }

    private String label(Order order) {
        if (order.getStatus().equals("rejected")) {
            return order.getStatusLabel();
        } else {
            return order.getStatusLabel() + " " + (order.getStatusId().equals(AppParams.STATUS_ID_COMPLETE_PAID) ? getString(R.string.activities_OrderDetailActivity_paid1) : getString(R.string.activities_OrderDetailActivity_no_paid1));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case ReviewActivity.REQUEST_CODE_ORDER_INFO:

                    (findViewById(R.id.btn_review)).setVisibility(View.GONE);
                    (findViewById(R.id.ll_detailorder_rating)).setVisibility(View.VISIBLE);

                    //((RatingBar) findViewById(R.id.rb_detailorder_rating)).setRating((float) data.getIntExtra("rating", 0));

                    setRating(data.getIntExtra("rating", 0));

                    break;
            }
        }
    }

    private void setRating(int stars) {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_detailorder_stars);
        ImageView star1 = (ImageView) linearLayout.getChildAt(0);
        ImageView star2 = (ImageView) linearLayout.getChildAt(1);
        ImageView star3 = (ImageView) linearLayout.getChildAt(2);
        ImageView star4 = (ImageView) linearLayout.getChildAt(3);
        ImageView star5 = (ImageView) linearLayout.getChildAt(4);

        int[] drawablesArray = new int[5];
        for (int i = 0; i < 5; i++) {
            if (i <= (stars - 1)) {
                drawablesArray[i] = R.drawable.star_mini;
            } else {
                drawablesArray[i] = R.drawable.star_mini_grey;
            }
        }
        star1.setImageResource(drawablesArray[0]);
        star2.setImageResource(drawablesArray[1]);
        star3.setImageResource(drawablesArray[2]);
        star4.setImageResource(drawablesArray[3]);
        star5.setImageResource(drawablesArray[4]);
    }

    public void onClickShare(View v) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String packageName = getApplicationContext().getPackageName();
        String send_text = getString(R.string.activities_OrderCompletedActivity_share1)
                + getString(R.string.app_name)
                + getString(R.string.activities_OrderCompletedActivity_share2)
                + packageName;
        sendIntent.putExtra(Intent.EXTRA_TEXT, send_text);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.activities_OrderDetailActivity_share_title)));
    }

    public void onClickRepeat(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(MainActivity.BACK_KEY, MainActivity.BACK_VALUE_OLD_ORDER);
        intent.putExtra("order_id", order.getOrderId());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, LastOrdersActivity.class));
        finish();
    }


    private String formatDate(long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return dateFormat.format(new Date(date));
    }

    private String formatTime(double time) {

        long mSec = Math.round(time * 60 * 1000);

        if (mSec > 0) {
            return String.format("%d " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long) + ", %d " + getString(R.string.fragments_FragmentAbstractMap_time_second_long), TimeUnit.MILLISECONDS.toMinutes(mSec), TimeUnit.MILLISECONDS.toSeconds(mSec) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(mSec)));
        } else {
            return String.format("%d " + getString(R.string.fragments_FragmentAbstractMap_time_minute_long) + ", %d " + getString(R.string.fragments_FragmentAbstractMap_time_second_long), TimeUnit.MILLISECONDS.toMinutes(0), TimeUnit.MILLISECONDS.toSeconds(0) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(0)));
        }
    }

}
