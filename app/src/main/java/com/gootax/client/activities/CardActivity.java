package com.gootax.client.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gootax.client.R;
import com.gootax.client.events.CheckCardEvent;
import com.gootax.client.events.CreateCardEvent;
import com.gootax.client.models.Profile;
import com.gootax.client.network.listeners.CheckCardListener;
import com.gootax.client.network.listeners.CreateCardListener;
import com.gootax.client.network.requests.PostCheckClientCard;
import com.gootax.client.network.requests.PostCreateClientCard;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Date;


public class CardActivity extends BaseSpiceActivity {

    public static final String RESULT_SUCCESS_KEY = "card_activity.result_success_type";
    public static final String RESULT_SUCCESS_CARD = "card_activity.result_success_card";
    public static final String RESULT_SUCCESS_NULL = "card_activity.result_success_null";

    private WebView webView;
    private ProgressDialog progressDialog;
    private ProgressDialog checkProgressDialog;
    private String orderId = "";
    private String clientId;
    private String phone;
    private boolean isRedirected;
    private Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(Profile.getProfile().getThemeId());
        setContentView(R.layout.activity_card);
        setTitle(R.string.activities_CardActivity_title);

        initToolbar();
        initViews();
        initVars();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) getSupportActionBar()
                .setDisplayHomeAsUpEnabled(true);
    }

    private void initViews() {
        WebViewClient client = new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                if (progressDialog != null) progressDialog.cancel();
            }
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.contains("payment_success") || url.contains("errors")) {
                    isRedirected = true;
                }
            }
        };
        webView = (WebView) findViewById(R.id.webview_card);
        if (webView != null) {
            webView.setWebViewClient(client);
            webView.getSettings().setJavaScriptEnabled(true);
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.activities_CardActivity_text_load));
        progressDialog.show();
        checkProgressDialog = new ProgressDialog(this);
        checkProgressDialog.setMessage(getString(R.string.activities_CardActivity_check_text_load));
    }

    private void initVars() {
        EventBus.getDefault().register(this);
        profile = Profile.getProfile();
        clientId = profile.getClientId();
        phone = profile.getPhone();
        getSpiceManager().execute(new PostCreateClientCard(clientId,
                        String.valueOf((new Date()).getTime()),
                phone, profile.getTenantId()),
                new CreateCardListener());
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onMessage(CreateCardEvent pageEvent) {
        progressDialog.dismiss();
        if (!pageEvent.hasError()) {
            orderId = pageEvent.getOrderId();
            String webPageString = pageEvent.getWebPageString();
            if (!webPageString.isEmpty()) {
                webView.loadUrl(webPageString);
            }
        } else {
            setResult(RESULT_CANCELED, new Intent());
            finish();
        }
    }

    @Subscribe
    public void onMessage(CheckCardEvent event) {
        int resultCode = event.getResultCode();
        Intent returnIntent = new Intent();
        if (resultCode == 0) {
            returnIntent.putExtra(RESULT_SUCCESS_KEY, RESULT_SUCCESS_CARD);
            setResult(RESULT_OK, returnIntent);
        } else {
            setResult(RESULT_CANCELED, returnIntent);
        }
        checkProgressDialog.cancel();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isRedirected) {
            checkProgressDialog.show();
            getSpiceManager().execute(new PostCheckClientCard(clientId,
                            String.valueOf((new Date()).getTime()),
                    orderId, phone, profile.getTenantId()),
                    new CheckCardListener());
        } else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(RESULT_SUCCESS_KEY, RESULT_SUCCESS_NULL);
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }

}
